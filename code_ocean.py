"""This file runs a small subset of cases on Code Ocean platform. To run the
full test suite see `README.md` and the file `automate.py`.
"""
import os
from automan.api import Automator, Simulation
from automate import (PerfAnalysisBreakdown, SShape, scheme_opts,
                      AirfoilHuang)

n_core = -1
n_thread = -2
backend = ' --openmp'


class SShapeHalfRotation(SShape):
    """This problem runs for 5 s. The full problem is on automate.py which runs for
    10 s or one full rotation.
    """
    def setup(self):
        get_path = self.input_path
        cmd = 'python code/s_shape.py' + backend

        # tf=5 corresponds to half rotation of the S-shape.
        self.case_info = {'s_shape_re_2k': dict(tf=5, pf=250)}
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]


if __name__ == '__main__':
    PROBLEMS = [AirfoilHuang, SShapeHalfRotation, PerfAnalysisBreakdown]
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()
