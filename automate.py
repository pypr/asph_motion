#!/usr/bin/env python
import os

from automan.api import PySPHProblem as Problem
from automan.api import Automator, Simulation, filter_cases
import numpy as np
from cycler import cycler
import matplotlib as mpl
from matplotlib import rc
import matplotlib.pyplot as plt
import pandas as pd

from pysph.solver.utils import load, get_files


rc('font', **{'size': 14})
rc('legend', fontsize='xx-small')
rc('axes', grid=True, linewidth=1.2, labelsize='x-small', titlesize='small')
rc('axes.grid', which='both', axis='both')
# rc('axes.formatter', limits=(1, 2), use_mathtext=True, min_exponent=1)
rc('grid', linewidth=0.5, linestyle='--')
rc('xtick', direction='in', top=True, labelsize='x-small')
rc('ytick', direction='in', right=True, labelsize='x-small')
rc('savefig', format='pdf', bbox='tight', pad_inches=0.05,
   transparent=False, dpi=300)
rc('lines', linewidth=1.5)


n_core = -1
n_thread = -2
backend = ' --openmp'

def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    return params


def get_files_at_given_times_from_log(files, times, logfile):
    import re
    result = []
    time_pattern = r"output at time\ (\d+(?:\.\d+)?)"
    d = {}
    for f in files:
        fname = os.path.basename(f)
        noext = os.path.splitext(fname)[0]
        iters = noext.split('_')[-1]
        d.update({iters: f})
    with open(logfile, 'r') as f:
        for line in f:
            t = re.findall(time_pattern, line)
            if t and float(t[0]) in times:
                iters = re.findall(r"iteration\ (\d+)", line)
                result.append(iters[0].zfill(5))
    out_files = []
    for i in result:
        out_files.append(d[i])
    return out_files


class Bao1Moving(Problem):
    '''
    Cases:
    1. amp 0.25 omega 0.7775
    2. amp 0.25 omega 0.9331  #1.0367
    3. amp 0.25 omega 1.5551
    4. amp 1.25 omega 1.5551
    '''
    def get_name(self):
        return 'bao_1moving'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_bao_oscillation_1cylinder.py' + backend

        self.tf = tf = 200.0
        pf = 400
        re = 100
        cr = 1.12
        dx_max = 0.5
        dx_min = 0.01

        self.case_info = {
            'bao_1_moving_amp_0.25_omega_0.9331': dict(
                amp=0.25, omega=0.9331
            )
        }
        # self.case_info.update({
        #     'bao_1_moving_amp_0.25_omega_1.1404': dict(
        #         amp=0.25, omega=1.1404
        #     )
        # })
        self.case_info.update({
            'bao_1_moving_amp_0.25_omega_0.5184': dict(
                amp=0.25, omega=0.5184
            )
        })
        self.case_info.update({
            'bao_1_moving_amp_0.25_omega_1.5551': dict(
                amp=0.25, omega=1.5551
            )
        })
        self.case_info.update({
            'bao_1_moving_amp_1.25_omega_1.5551': dict(
                amp=1.25, omega=1.5551
            )
        })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=re, pf=pf, cr=cr, tf=tf, dx_max=dx_max, dx_min=dx_min,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_cl()
        # self.plot_prop()  # Not used in paper.

    def plot_cl(self):
        rc('axes', prop_cycle=(
            cycler('color', ['red', 'black']) +
            cycler('linestyle', ['-', '-'])
        ))
        for case in self.cases:
            omega = str(case.params['omega'])
            amp = str(case.params['amp'])
            # My data
            cl_asph = np.load(case.input_path('results.npz'))

            # Bao's data
            filename = f"code/data/fpc_bao/bao_1moving/omega_{omega}_amp_{amp}"
            cl_bao = np.genfromtxt(
                os.path.join(filename, "cl.csv"), delimiter=",",
                names=["t", "cl"]
            )
            fig, axs = plt.subplots(1, 1, figsize=(4, 3.8))

            # lockon fig2
            cl = cl_asph['cl']+cl_asph['cl_sf']
            axs.plot(cl_asph['t'], cl, label='present work')
            axs.plot(cl_bao['t'], cl_bao['cl'], label='Bao et al. (2013)',
                     zorder=0)
            axs.set_ylabel(r'$C_l$, total lift')
            axs.set_xlim(5, 100)

            tmp = np.max(np.abs(cl[40:]))
            ymin = -1.8 * tmp
            ymax = 1.8 *tmp
            axs.set_ylim(ymin, ymax)

            axs.set_xlabel(r'$T$')
            axs.legend()
            filename = self.output_path(case.name) + '.pdf'
            fig.savefig(filename, dpi=300)
            plt.clf()
            plt.close()

    def plot_prop(self, conditions=None, prop='vor',
                  cmap=plt.cm.get_cmap('rainbow', 21), ymin=-5, ymax=5,
                  figsize=(7, 4), size=6, dpi=300):
        if conditions is None:
            conditions = {}
        for case in filter_cases(self.cases, **conditions):
            fig, ax = plt.subplots(1, 1, figsize=figsize)
            files = get_files(case.input_path(), 'fpc_bao_oscillation_1cylinder')
            data = load(files[-1])
            t = data['solver_data']['t']
            f = data['arrays']['fluid']
            s = data['arrays']['solid']
            val = f.get(prop)
            xmin = s.x.min()*0.95
            xmax = xmin + 20
            cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

            amp = case.params['amp']
            omega = case.params['omega']
            vmin, vmax = -0.3, 0.3
            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=2*size*f.m[cond]/np.max(f.m[cond]), rasterized=True, cmap=cmap,
                edgecolor='none', vmin=vmin, vmax=vmax
            )
            msg = r"$T = $" + f'{t:.1f}'
            ax.annotate(
                msg, (xmin*1.2, ymax*0.8), fontsize='xx-small',
                bbox=dict(boxstyle="square,pad=0.3", fc='white')
            )
            ax.scatter(s.x, s.y, c=s.m, cmap='viridis', s=size, rasterized=True)
            cbar = fig.colorbar(tmp, ax=ax, shrink=0.4, pad=0.01)
            cbar.ax.tick_params(labelsize='xx-small')
            cbar.set_label(rf'${prop}$', size='xx-small')
            ax.grid()
            ax.set_ylabel('y/D', fontsize='xx-small')
            ax.set_xlabel('x/D', fontsize='xx-small')
            ax.set_aspect('equal', 'box')
            ax.set_xlim(xmin, xmax)
            ax.set_ylim(ymin, ymax)
            ax.tick_params(axis='both', which='major', labelsize='xx-small')
            fig.savefig(
                self.output_path(f"amp_{amp}_omega_{omega}_vor.pdf"), dpi=dpi
            )
            plt.clf()
            plt.close()


class Bao2Moving(Problem):
    '''
    Cases:
    1. gap 1.2
    2. gap 1.5
    3. gap 2.5
    4. gap 4.0
    '''
    def get_name(self):
        return 'bao_2moving'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_bao.py' + backend

        self.tf = tf = 300
        pf = 1000
        dx_max = 0.5
        dx_min = 0.01
        re = 100
        cr = 1.12

        self.case_info = {
            'g1.2': dict(
                gap=1.2, amp=0.25, omega=0.82938
            )
        }
        # self.case_info.update({
        #     'g1.5': dict(
        #         gap=1.5, amp=0.25, omega=0.82938
        #     )
        # })
        self.case_info.update({
            'g2.5': dict(
                gap=2.5, amp=0.25, omega=2.07354
            )
        })
        self.case_info.update({
            'g4.0': dict(
                gap=4.0, amp=0.25, omega=2.07354
            )
        })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=re, pf=pf, dx_max=dx_max, dx_min=dx_min, cr=cr, tf=tf,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_power_spectral_density()
        self.plot_prop()
        self.compare_clcd_of_cylinders()

    def plot_power_spectral_density(self):
        rc('axes', prop_cycle=(
            cycler('color', ['red', 'black']) +
            cycler('linestyle', ['-', '-'])
        ))

        import scipy.signal as signal

        for case in self.cases:
            upper = np.load(case.input_path('results_solid.npz'))
            lower = np.load(case.input_path('results_solid2.npz'))

            fig, axs = plt.subplots(1, 1, figsize=(8, 4))

            time = upper['t']
            dt = time[1]-time[0]
            fs = 1/dt
            Cl = upper['cl']
            f_a1, Cl_a1 = signal.welch((upper['cl']+upper['cl_sf']), fs, nperseg=len(Cl))
            f_a2, Cl_a2 = signal.welch((lower['cl']+lower['cl_sf']), fs, nperseg=len(Cl))
            plt.loglog(f_a1, Cl_a1, label='Upper Cyl', linewidth=1)
            plt.loglog(f_a2, Cl_a2, label='Lower Cyl', linewidth=1)
            axs.set_ylabel('PSD')

            axs.set(xlabel=r'$fD/U_\infty$')
            axs.legend()

            filename = self.output_path(case.name) + '_PSD.png'
            fig.savefig(filename, dpi=300)
            plt.clf()
            plt.close()

    def plot_prop(self, conditions=None, prop='vor', cmap='rainbow',
                  xmin=18, xmax=36, ymin=-7, ymax=4, figsize=(8, 4),
                  size=4, dpi=300):
        if conditions is None:
            conditions = {}
        for case in filter_cases(self.cases, **conditions):
            fig, ax = plt.subplots(1, 1, figsize=figsize)
            files = get_files(case.input_path(), 'fpc_bao')
            data = load(files[-1])
            t = data['solver_data']['t']
            f = data['arrays']['fluid']
            s1 = data['arrays']['solid']
            s2 = data['arrays']['solid2']
            val = f.get(prop)
            cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

            vmin, vmax = -1, 1
            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=size, rasterized=True, cmap=cmap,
                edgecolor='none', vmin=vmin, vmax=vmax
            )
            msg = r"$T = $" + f'{t:.1f}'
            ax.annotate(
                msg, (xmin*1.2, ymax*0.8), fontsize='small',
                bbox=dict(boxstyle="square,pad=0.3", fc='white')
            )
            ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size, rasterized=True)
            ax.scatter(s2.x, s2.y, c=s2.m, cmap='viridis', s=size, rasterized=True)
            fig.colorbar(tmp, ax=ax, shrink=0.4, label=rf'${prop}$', pad=0.01)
            ax.grid()
            ax.set_ylabel(r'$y/D$')
            ax.set_xlabel(r'$x/D$')
            ax.set_aspect('equal', 'box')
            ax.set_xlim(xmin, xmax)
            ax.set_ylim(ymin, ymax)
            fig.savefig(self.output_path(f'{case.name}.pdf'), dpi=dpi)
            plt.clf()
            plt.close()

    def compare_clcd_of_cylinders(self):
        rc('axes', prop_cycle=(
            cycler('color', ['red', 'black']) +
            cycler('linestyle', ['-', '-'])
        ))

        for case in self.cases:
            upper = np.load(case.input_path('results_solid.npz'))
            lower = np.load(case.input_path('results_solid2.npz'))

            fig, axs = plt.subplots(1, 1, figsize=(8, 4))

            plt.plot(upper['t'], (upper['cl']+upper['cl_sf']),
                     label=r'Upper Cyl $C_l$', linewidth=1)
            plt.plot(lower['t'], (lower['cl']+lower['cl_sf']),
                     label=r'Lower Cyl $C_l$', linewidth=1)
            plt.plot(upper['t'], (upper['cd']+upper['cd_sf']),
                     label=r'Upper Cyl $C_d$', linewidth=1)
            plt.plot(lower['t'], (lower['cd']+lower['cd_sf']),
                     label=r'Lower Cyl $C_d$', linewidth=1)
            axs.set(xlabel='t')
            axs.legend()

            filename = self.output_path(case.name) + '_ClCd.pdf'
            fig.savefig(filename, dpi=300)
            plt.clf()
            plt.close()


class Bao2Static(Problem):
    '''
    Cases:
    1. gap 1.2
    2. gap 1.5
    3. gap 2.5
    4. gap 4.0
    '''
    def get_name(self):
        return 'bao_2static'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_bao_static.py' + backend

        self.tf = tf = 250
        pf = 1000
        dx_max = 0.5
        dx_min = 0.01
        re = 100
        cr = 1.12

        self.case_info = {
            's1.2': dict(
                gap=1.2
            )
        }
        self.case_info.update({
            's1.5': dict(
                gap=1.5
            )
        })
        self.case_info.update({
            's4.0': dict(
                gap=4.0
            )
        })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None, cr=cr,
                re=re, tf=tf, pf=pf, dx_min=dx_min, dx_max=dx_max,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_cd()
        self.plot_prop()

    def plot_cd(self):
        rc('axes', prop_cycle=(
            cycler('color', ['red', 'black']) +
            cycler('linestyle', ['-', '-'])
        ))
        for case in self.cases:
            gap = str(case.params['gap'])
            # My data
            upper = np.load(case.input_path('results_solid.npz'))
            lower = np.load(case.input_path('results_solid2.npz'))

            # Bao's data
            filename = f"code/data/fpc_bao/bao_2static/g{gap}csv"
            cd_upper = np.genfromtxt(
                os.path.join(filename, "cdu.csv"), delimiter=",",
                names=["t", "cd"]
            )
            cd_lower = np.genfromtxt(
                os.path.join(filename, "cdl.csv"), delimiter=",",
                names=["t", "cd"]
            )
            cl_upper = np.genfromtxt(
                os.path.join(filename, "clu.csv"), delimiter=",",
                names=["t", "cl"]
            )
            cl_lower = np.genfromtxt(
                os.path.join(filename, "cll.csv"), delimiter=",",
                names=["t", "cl"]
            )

            fig, axs = plt.subplots(2, 2, figsize=(10, 5))

            # Upper Cd
            ut, u_cd =upper['t'], (upper['cd']+upper['cd_sf'])
            axs[0, 0].plot(ut, u_cd, label='present work')
            axs[0, 0].plot(cd_upper['t'], cd_upper['cd'],
                           label='Bao et al. (2013)', zorder=0)
            axs[0, 0].set_title('(a)')
            axs[0, 0].set_ylabel(r'$C_d$')

            min_y = np.min(u_cd[ut > 5])
            max_y = np.max(u_cd[ut > 5])
            fac_min = 1 - np.sign(min_y) * 0.1
            fac_max = 1 + np.sign(max_y) * 0.1
            axs[0, 0].set_ylim(fac_min * min_y, fac_max * max_y)
            axs[0, 0].set_xlim(0, np.max(ut))
            axs[0, 0].legend()

            # Lower Cd
            lt, l_cd = lower['t'], (lower['cd']+lower['cd_sf'])
            axs[0, 1].plot(lt, l_cd, label='present work')
            axs[0, 1].plot(cd_lower['t'], cd_lower['cd'],
                           label='Bao et al. (2013)', zorder=0)
            axs[0, 1].set_title('(b)')

            min_y = np.min(l_cd[lt > 5])
            max_y = np.max(l_cd[lt > 5])
            fac_min = 1 - np.sign(min_y) * 0.1
            fac_max = 1 + np.sign(max_y) * 0.1
            axs[0, 1].set_ylim(fac_min * min_y, fac_max * max_y)
            axs[0, 1].set_xlim(0, np.max(lt))

            axs[0, 1].set_ylabel(r'$C_d$')

            # Lower Cl
            ut, u_cl = upper['t'], (upper['cl']+upper['cl_sf'])
            axs[1, 0].plot(ut, u_cl, label='present work')
            axs[1, 0].plot(cl_upper['t'], cl_upper['cl'],
                           label='Bao et al. (2013)', zorder=0)
            axs[1, 0].set_title('(c)')
            min_y = np.min(u_cl[ut > 5])
            max_y = np.max(u_cl[ut > 5])
            fac_min = 1 - np.sign(min_y) * 0.1
            fac_max = 1 + np.sign(max_y) * 0.1
            axs[1, 0].set_ylim(fac_min * min_y, fac_max * max_y)
            axs[1, 0].set_xlim(0, np.max(ut))
            axs[1, 0].set_ylabel(r'$C_l$')

            # Upper Cl
            lt, l_cl = lower['t'], (lower['cl']+lower['cl_sf'])
            axs[1, 1].plot(lt, l_cl, label='present work')
            axs[1, 1].plot(cl_lower['t'], cl_lower['cl'],
                           label='Bao et al. (2013)', zorder=0)
            axs[1, 1].set_title('(d)')
            axs[1, 1].set_ylabel(r'$C_l$')
            min_y = np.min(l_cl[lt > 5])
            max_y = np.max(l_cl[lt > 5])
            fac_min = 1 - np.sign(min_y) * 0.1
            fac_max = 1 + np.sign(max_y) * 0.1
            axs[1, 1].set_ylim(fac_min * min_y, fac_max * max_y)
            axs[1, 1].set_xlim(0, np.max(lt))

            axs[1, 0].set_xlabel(r'$T$')
            axs[1, 1].set_xlabel(r'$T$')
            fig.savefig(self.output_path(f'{case.name}.pdf'), dpi=300)
            plt.clf()
            plt.close()

    def plot_prop(self, conditions=None, prop='vor', cmap='rainbow',
                  xmin=22.5, xmax=60, ymin=-6, ymax=6, figsize=(9, 5),
                  size=6, dpi=300):
        if conditions is None:
            conditions = {}
        for case in filter_cases(self.cases, **conditions):
            fig, ax = plt.subplots(1, 1, figsize=figsize)
            files = get_files(case.input_path(), 'fpc_bao')
            data = load(files[-1])
            t = data['solver_data']['t']
            f = data['arrays']['fluid']
            s1 = data['arrays']['solid']
            s2 = data['arrays']['solid2']
            val = f.get(prop)
            cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

            cmap = plt.cm.get_cmap('rainbow', 21)
            vmin, vmax = -0.5, 0.5
            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=2*size*f.m[cond]/np.max(f.m[cond]), rasterized=True, cmap=cmap,
                edgecolor='none', vmin=vmin, vmax=vmax
            )
            msg = r"$T = $" + f'{t:.1f}'
            ax.annotate(
                msg, (xmin*1.2, ymax*0.8), fontsize='small',
                bbox=dict(boxstyle="square,pad=0.3", fc='white')
            )
            ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size, rasterized=True)
            ax.scatter(s2.x, s2.y, c=s2.m, cmap='viridis', s=size, rasterized=True)
            fig.colorbar(tmp, ax=ax, shrink=0.4, label=rf'${prop}$', pad=0.01)
            ax.grid()
            ax.set_ylabel('y/D')
            ax.set_xlabel('x/D')
            ax.set_aspect('equal', 'box')
            ax.set_xlim(xmin, xmax)
            ax.set_ylim(ymin, ymax)
            fig.savefig(self.output_path(f'{case.name}_{prop}.pdf'), dpi=dpi)
            plt.clf()
            plt.close()


class MovingSquare(Problem):
    '''
    Cases:
    1. dx_min 0.04
    2. dx_min 0.02
    3. dx_min 0.0125
    '''
    def get_name(self):
        return 'moving_square'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/moving_square.py' + backend

        self.dxmin = [0.06, 0.04, 0.02, 0.0125]
        self.tf = tf = 8.0
        pf = 200
        re = 150
        cr = 1.15
        dx_max = 0.04

        self.case_info = {
            'dxmin_p04': dict(
                dx_min=0.04
            )
        }
        self.case_info.update({
            'dxmin_p02': dict(
                dx_min=0.02
            )
        })
        self.case_info.update({
            'dxmin_p0125': dict(
                dx_min=0.0125
            )
        })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=re, tf=tf, pf=pf, cr=cr, dx_max=dx_max,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_initial_condition()
        self.plot_prop({'dx_min': 0.0125}, prop='vmag', vmin=0, vmax=1.55,
                       ymin=-1.5, ymax=1.5, dpi=150, figsize=(9, 5), size=1,
                       cmap=plt.cm.get_cmap('seismic', 25))
        self.plot_prop({'dx_min': 0.0125}, prop='vor', vmin=-3, vmax=3,
                       ymin=-1.5, ymax=1.5, dpi=150, figsize=(9, 5), size=1,
                       cmap=plt.cm.get_cmap('rainbow', 25))
        self.plot_prop({'dx_min': 0.0125}, prop='p', vmin=-0.6, vmax=0.6,
                       ymin=-1.5, ymax=1.5, dpi=150, figsize=(9, 5),
                       cmap=plt.cm.get_cmap('seismic', 25))

    def drag_plots(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1, figsize=(4, 3.8))
        for case in self.cases:
            re = case.params['re']
            data = case.data
            dx_min = case.params['dx_min']
            label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {1/dx_min:.0f}'

            ax.plot(data['t'], -data['cd'], label=label)

        re = str(int(re)).zfill(3)
        spheric = pd.read_csv(f"code/data/SPHERIC_Case/Force_Re{re}.csv")
        ax.plot(spheric['t'], spheric['Cd_p'], 'k-', zorder=0,
                label='FDNS')
        ax.set_xlim(0, self.tf)
        ax.set_ylabel(r'$C_{d}$, pressure drag')
        ax.set_xlabel(r'$T$')
        ax.legend()
        fig.savefig(self.output_path('cd_moving_square.pdf'))
        plt.clf()

    def skin_friction_plots(self):
        import matplotlib.pyplot as plt

        rc('lines', linewidth=2.0)
        fig, ax = plt.subplots(1, 1, figsize=(4, 3.8))
        for case in self.cases:
            re = case.params['re']
            data = case.data
            dx_min = case.params['dx_min']
            label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {2/dx_min:.0f}'

            ax.plot(data['t'], -data['cd_sf'], label=label)
            ax.set_ylabel(r'$C_{d}$, skin friction drag')
        re = str(int(re)).zfill(3)
        spheric = pd.read_csv(f"code/data/SPHERIC_Case/Force_Re{re}.csv")
        ax.plot(spheric['t'], spheric['Cd_v'], 'k-',
                linewidth=0.5, zorder=0, label='FDNS')
        ax.set_xlim(0, self.tf)
        ax.set_xlabel(r'$T$')
        # ax2.set_title(rf'$Re$ = {re}')
        ax.legend()
        fig.savefig(self.output_path('cd_sf_moving_square.pdf'))
        plt.clf()

    def plot_prop(self, conditions=None, prop='ds', cmap='rainbow',
                  xmin=1, xmax=10, ymin=-4, ymax=4, figsize=(10, 4),
                  size=1.5, dpi=300, vmin=-6, vmax=6):
        if conditions is None:
            conditions = {}
        times = [5]
        for case in filter_cases(self.cases, **conditions):
            files = get_files(case.input_path(), 'moving_square')
            logfile = case.input_path('moving_square.log')
            times = [5, 8]
            files = get_files_at_given_times_from_log(files, times, logfile)
            for file, t in zip(files, times):
                fig, ax = plt.subplots(1, 1, figsize=figsize)
                data = load(file)
                t = data['solver_data']['t']
                f = data['arrays']['fluid']
                s1 = data['arrays']['solid']
                if prop == 'p':
                    rho = 1000
                    val = f.get(prop) / (rho * 1.0**2)
                    label = r'$p/\rho U^2_{\infty}$'
                else:
                    label = rf"${prop}$"
                    val = f.get(prop)
                cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

                tmp = ax.scatter(
                    f.x[cond], f.y[cond], c=val[cond],
                    s=2*size*f.m[cond]/np.max(f.m[cond]),
                    rasterized=True, cmap=cmap,
                    edgecolor='none', vmin=vmin, vmax=vmax
                )
                msg = r"$T = $" + f'{t:.1f}'
                ax.annotate(
                    msg, (xmin*1.2, ymax*0.8), fontsize='small',
                    bbox=dict(boxstyle="square,pad=0.3", fc='white')
                )
                ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size,
                           rasterized=True)
                cbar = fig.colorbar(tmp, ax=ax, shrink=0.4, label=f'{label}',
                                    pad=0.01)
                ax.grid()
                ax.set_aspect('equal')
                ax.set_xlim(xmin, xmax)
                ax.set_ylim(ymin, ymax)
                cbar.ax.tick_params(labelsize='large')
                fig.savefig(self.output_path(f'{case.name}_{prop}_t_{t}.pdf'),
                            dpi=dpi)
                plt.clf()
                plt.close()

    def square_motion(self, t):
        a = 2.8209512
        b = 0.525652151
        c = 0.14142151
        d = -2.55580905e-08

        au = a * np.exp(-(t - b) * (t - b) / (2.0 * c * c)) + d
        return au

    def plot_initial_condition(self):
        rc('lines', linewidth=2.0)
        fig, ax = plt.subplots(1, 1, figsize=(4.48, 2.53))
        t = np.linspace(0, 2, 1000)
        acc = self.square_motion(t)
        vel = np.zeros_like(t)
        for i in range(len(t)-1):
            vel[i+1] = vel[i] + acc[i] * (t[i+1] - t[i])
        ax.plot(t, acc, label='acceleration')
        ax.plot(t, vel, label='velocity')
        ax.set_xlim(0, 2.0)
        ax.set_ylim(0, 3.0)
        ax.set_xlabel(r'$T$')
        ax.legend()
        fig.savefig(self.output_path('../msqr', 'uiau.pdf'))
        plt.clf()


class MovingSquareRe(Problem):
    '''
    Cases:
    1. Re 50
    2. Re 100
    3. Re 150
    '''
    def get_name(self):
        return 'moving_squareRe'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/moving_square.py' + backend

        self.tf = tf = 8.0
        pf = 200
        cr = 1.1
        dx_max = 0.04
        dx_min = 0.0125

        self.case_info = {
            're_50': dict(
                re=50
            )
        }
        self.case_info.update({
            're_100': dict(
                re=100
            )
        })
        self.case_info.update({
            're_150': dict(
                re=150
            )
        })
        self.case_info.update({
            're_600': dict(
                re=600
            )
        })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                dx_min=dx_min, tf=tf, pf=pf, cr=cr, dx_max=dx_max,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.cd_plot_comparison_re()
        self.cd_plot_comparison_spheric()
        self.cd_comparison_re_600()

    def cd_plot_comparison_re(self):
        import matplotlib.pyplot as plt

        rc('lines', linewidth=2.0)
        fig, ax = plt.subplots(1, 1, figsize=(6, 3.8))
        labels = []
        for case in self.cases:
            data = case.data
            re = case.params['re']
            if int(re) == 600:
                continue
            label = r'$Re$ =' + f' {re}'
            labels.append(label)
            ax.plot(data['t'], -data['cd'] - data['cd_sf'], label=label)
            ax.set_ylabel(r'$C_{d}$, total drag')

            # ax_new = fig.add_axes([.275, .575, .275, .2])
            # ax_new.plot(data['t'], -data['cd'] - data['cd_sf'], label=label)
            # ax_new.set_xlim([0.25, 1.0])
            # ax_new.set_ylim([4, 12])

        for case in self.cases:
            data = case.data
            re = case.params['re']
            re = str(int(re)).zfill(3)
            if int(re) == 600:
                continue
            spheric = pd.read_csv(f"code/data/SPHERIC_Case/Force_Re{re}.csv")
            n = 2
            t, cd = spheric['t'][::n], (spheric['Cd_p'] + spheric['Cd_v'])[::n]
            ax.plot(t, cd, 'k-', linewidth=0.5, zorder=10)
        labels.append("FDNS, Colicchio et al. (2006)")
        ax.set_xlim(0, self.tf)
        ax.set_xlabel(r'$T$')
        ax.legend(labels)

        fig.savefig(self.output_path('cd_varying_re.pdf'))
        plt.clf()

    def cd_plot_comparison_spheric(self):
        import matplotlib.pyplot as plt

        rc('lines', linewidth=2.0)
        fig, ax = plt.subplots(1, 1, figsize=(4, 3.8))
        for case in filter_cases(self.cases, re=100):
            # SPHERIC data
            spheric = np.genfromtxt("code/data/SPHERIC_Case/Force_Re100.csv",
                                    delimiter=",", names=["t", "cd_p", "cd_v"])
            n = 1
            t, cd = spheric['t'][::n], (spheric['cd_p']+spheric['cd_v'])[::n]
            ax.plot(t, cd, 'k-', linewidth=0.5, label='FDNS, Colicchio et al. (2006)')

            data = case.data
            ax.plot(data['t'], -data['cd'] - data['cd_sf'], '-', linewidth=1.0,
                    label='present work')
            ax.set_ylabel(r'$C_{d}$, total drag')

            ax_new = fig.add_axes([.5, .35, .275, .2])
            ax_new.plot(spheric['t'][::5],
                        (spheric['cd_p']+spheric['cd_v'])[::5], 'k-',
                        linewidth=0.5, label='FDNS, Colicchio et al. (2006)')
            ax_new.plot(data['t'], -data['cd']-data['cd_sf'],
                        linewidth=0.5, label='present work')
            ax_new.set_xlim([4,8])
            ax_new.set_ylim([1.75,2.15])

        ax.set_xlim(0, self.tf)
        ax.set_ylim([0.0, 10.0])
        ax.set_xlabel(r'$T$')
        ax.legend()
        fig.savefig(self.output_path('cd_comparison_spheric.pdf'))
        plt.clf()

    def cd_comparison_re_600(self):
        import matplotlib.pyplot as plt

        rc('lines', linewidth=2.0)
        fig, ax = plt.subplots(1, 1, figsize=(4, 3.8))
        for case in filter_cases(self.cases, re=600):
            data = case.data
            # SPHERIC data
            marn = np.genfromtxt("code/data/SPHERIC_Case/re600_marrone.csv",
                                 delimiter=",", names=["t", "cdp"])
            fdns = np.genfromtxt("code/data/SPHERIC_Case/Force_Re600.csv",
                                 delimiter=",", names=["t", "Cd_p"])

            ax.plot(fdns['t'], (fdns['Cd_p']), 'b--', linewidth=1.5,
                    label='FDNS, Colicchio et al. (2006)')
            ax.plot(marn['t'], (marn['cdp']), 'g:', linewidth=1.5,
                    label='Marrone et al. (2013)')
            ax.plot(data['t'], -(data['cd'] + data['cd_sf']), 'r-',
                    linewidth=1.5, label='present work', zorder=0)

            ax.set_xlabel(r'$T$')
            ax.set_ylabel(r'$C_{d}$, total drag')
            ax.set_xlim([0.0,8.0])
            ax.set_ylim([0.0, 10.0])
            ax.legend()

        fig.savefig(self.output_path('cd_comparison_re_600.pdf'))
        plt.clf()


class SunAirfoil12Re10k(Problem):
    '''
    Cases:
    1. S 0.00
    2. S 0.25
    3. S 0.50
    4. S 0.75
    5. S 1.00
    '''
    def get_name(self):
        return 'airfoil_static'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/sun_airfoil12_aoa6.py' + backend

        self.tf = tf = 15
        pf = 250
        cr = 1.12
        dx_max = 0.5
        dx_min=0.004
        re = 10000

        self.case_info = {
            'sun_airfoil12_aoa6_re10k_dxmin_004': dict(
                b=0.0
            )
        }
        # self.case_info.update({
        #     're_40000_pp_S50_dxmin004': dict(
        #         re=40000, b=0.50
        #     )
        # })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None, re=re,
                tf=tf, pf=pf, cr=cr, dx_max=dx_max, dx_min=dx_min,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.drag_plots()
        self.skin_friction_plots()

    def drag_plots(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1)
        for case in self.cases:
            data = case.data
            dx_min = case.params['dx_min']
            label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {2/dx_min:.0f}'

            ax.plot(data['t'], -data['cd'], label=label)
            ax.set_ylabel(r'$C_{d}$, pressure drag')
        ax.set_xlim(0, self.tf)
        ax.set_xlabel(r'$T$')
        # ax2.set_title(rf'$Re$ = {re}')
        ax.legend()
        fig.savefig(self.output_path('cd_airfoil_rotation.png'))
        plt.clf()

    def skin_friction_plots(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 1)
        for case in self.cases:
            data = case.data
            dx_min = case.params['dx_min']
            label = r'D/$\Delta x_{\mathrm{min}}$ =' + f' {2/dx_min:.0f}'

            ax.plot(data['t'], -data['cd_sf'], label=label)
            ax.set_ylabel(r'$C_{d}$, skin friction drag')
        ax.set_xlim(0, self.tf)
        ax.set_xlabel(r'$T$')
        # ax2.set_title(rf'$Re$ = {re}')
        ax.legend()
        fig.savefig(self.output_path('cd_sf_airfoil_rotation.png'))
        plt.clf()


class SunAirfoil108AoA4(Problem):
    '''
    Cases:
    1. Re 2000
    2. Re 6000
    '''
    def get_name(self):
        return 'airfoil_static'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/sun_airfoil08_aoa4.py' + backend

        self.tf = tf = 15
        pf = 250
        cr = 1.12
        dx_max = 0.5

        self.case_info = {
            'sun_airfoil08_aoa4_dxmin_004_re2000': dict(
                b=0.0, re=2000, dx_min=0.004, solution_adapt=0.0
            ),
            'sun_airfoil08_aoa4_dxmin_004_re6000': dict(
                b=0.0, re = 6000, dx_min=0.004, solution_adapt=0.0
            ),
            'sun_airfoil08_aoa4_dxmin_0025_re6000': dict(
                b=0.0, re = 6000, dx_min=0.0025, solution_adapt=0.0
            ),
            'sun_airfoil08_aoa4_dxmin_004_re6000_sa': dict(
                b=0.0, re = 6000, dx_min=0.0025, solution_adapt=0.05
            )
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                tf=tf, pf=pf, cr=cr, dx_max=dx_max,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        rc('axes', prop_cycle=(
            cycler('color', ['red', 'tab:blue', 'tab:green', 'k']) +
            cycler('linestyle', ['-', '--', '-.', '']) +
            cycler('marker', ['None', 'None', 'None', 'x'])
        ))
        self.cd_cl_plot(conditions={'re': 2000, 'solution_adapt': 0.0})
        rc('axes', prop_cycle=(
            cycler('color', ['magenta', 'red', 'tab:blue', 'tab:green', 'k']) +
            cycler('linestyle', ['dotted', '-',  '--', '-.', '']) +
            cycler('marker', ['None', 'None', 'None', 'None', 'x'])
        ))
        self.cd_cl_plot(conditions={'re': 6000, 'solution_adapt': 0.0})
        self.plot_prop({'solution_adapt': 0.05}, prop='vor', size=1.5,
                       vmin=-10, vmax=10, xmin=2, xmax=6.5, figsize=(12, 6),
                       ymin=-0.5, ymax=0.5,
                       cmap='rainbow')
        self.plot_prop({'re': 2000, 'solution_adapt': 0.0, 'dx_min': 0.004},
                       prop='vor', size=2.5, vmin=-5, vmax=5, figsize=(12, 6),
                       xmin=21.5, xmax=26, ymin=-0.5, ymax=0.5,
                       cmap='rainbow')

    def cd_cl_plot(self, conditions):
        rc('lines', linewidth=1.0)
        fig, ax = plt.subplots(1, 1, figsize=(4, 4))
        fig1, ax1 = plt.subplots(1, 1, figsize=(4, 4))
        for case in filter_cases(self.cases, **conditions):
            data = np.load(case.input_path('results_solid.npz'))
            re = case.params['re']

            n = 4
            d_dx = int(1/case.params['dx_min'])
            ax.plot(data['t'][::n], data['cd'][::n] + data['cd_sf'][::n], zorder=1,
                    label=r'present work, $L/\Delta x_{\min}$ = '+f'{d_dx}')

            ax1.plot(data['t'][::n], data['cl'][::n] + data['cl_sf'][::n], zorder=1,
                     label=r'present work, $L/\Delta x_{\min}$ = '+f'{d_dx}')

        re = str(re)[0]
        res = pd.read_csv(f'code/data/naca_0008_aoa_4_re_{re}k.csv')

        # ax.plot(res['sun_cd_t'], res['sun_cd'], zorder=9,
        #         label=r'Sun et al., $L/\Delta x_{\min} = 800$')
        ax.plot(res['sun_cd_dx_400_t'], res['sun_cd_dx_400'], zorder=8,
                label=r'Sun et al., $L/\Delta x_{\min} = 400$')
        ax.plot(res['sun_cd_dx_200_t'], res['sun_cd_dx_200'],
                label=r'Sun et al., $L/\Delta x_{\min} = 200$')
        ax.plot(res['dvh_cd_t'], res['dvh_cd'], label='DVH',
                zorder=0, mfc='None', markersize=2.5)
        ax.set_ylabel('$C_d$, total drag')

        ax.set_xlim(0, self.tf)
        ymax = np.max(res['sun_cd_dx_200'])
        ax.set_ylim(0.02, ymax*1.2)
        ax.set_xlabel(r'$T = U_{\infty}t/C$')
        ax.legend()
        fig.savefig(self.output_path(f'cd_re_{re}.pdf'))


        # ax1.plot(res['sun_cl_t'], res['sun_cl'], zorder=9,
        #         label=r'Sun et al., $L/\Delta x_{\min} = 800$')
        ax1.plot(res['sun_cl_dx_400_t'], res['sun_cl_dx_400'], zorder=8,
                label=r'Sun et al., $L/\Delta x_{\min} = 400$')
        ax1.plot(res['sun_cl_dx_200_t'], res['sun_cl_dx_200'],
                label=r'Sun et al., $L/\Delta x_{\min} = 200$')
        ax1.plot(res['dvh_cl_t'], res['dvh_cl'], label='DVH',
                 zorder=0, mfc='None', markersize=2.5)
        ax1.set_ylabel('$C_l$, total lift')

        ax1.set_xlim(0, self.tf)
        ymax = np.max(data['cl'][data['t'] > 2.0]) * 1.2
        ax1.set_ylim(0, ymax)
        ax1.set_xlabel(r'$T = U_{\infty}t/C$')
        ax1.legend()
        fig1.savefig(self.output_path(f'cl_re_{re}.pdf'))
        plt.clf()

    def plot_prop(self, conditions=None, prop='vor', cmap='rainbow',
                  xmin=1, xmax=10, ymin=-4, ymax=4, figsize=(12, 8),
                  size=2, dpi=300, vmin=-6, vmax=6):
        if conditions is None:
            conditions = {}
        for case in filter_cases(self.cases, **conditions):
            fig, ax = plt.subplots(1, 1, figsize=figsize)
            files = get_files(case.input_path(), 'sun_airfoil08_aoa4')
            data = load(files[-1])
            t = data['solver_data']['t']
            f = data['arrays']['fluid']
            s1 = data['arrays']['solid']
            val = f.get(prop)
            cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=size, rasterized=True, cmap=cmap,
                edgecolor='none', vmin=vmin, vmax=vmax
            )
            msg = r"$T = $" + f'{t:.1f}'
            ax.annotate(
                msg, (xmin+(xmax-xmin)*0.2, ymin+(ymax-ymin)*0.8), fontsize='small',
                bbox=dict(boxstyle="square,pad=0.3", fc='white')
            )
            ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size,
                        rasterized=True)
            cbar = fig.colorbar(tmp, ax=ax, shrink=0.4, label=rf'${prop}$',
                                pad=0.01)
            ax.grid()
            ax.set_aspect('equal', 'box')
            ax.set_xlim(xmin, xmax)
            ax.set_ylim(ymin, ymax)
            cbar.ax.tick_params(labelsize='large')
            fig.savefig(self.output_path(f'{case.name}_{prop}_t_{t}.pdf'),
                        dpi=dpi)
            plt.clf()
            plt.close()


class AirfoilHuang(Problem):
    '''
    Cases:
    1. dx-min 0.025 dx-max 0.100
    2. dx-min 0.025 dx-max 0.075
    3. dx-min 0.0125 dx-max 0.05
    '''
    def get_name(self):
        return 'airfoil_huang'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/airfoil_stationary_naca.py' + backend

        self.tf = tf = 12
        pf = 500
        re = 420
        cr = 1.1

        self.case_info = {
            'fpa_huang_lbdx80': dict(
                dx_min=0.0125, dx_max=0.04
            )
        }
        # self.case_info.update({
        #     'fpa_p025p1': dict(
        #         dx_min=0.025, dx_max=0.075
        #     )
        # })
        # self.case_info.update({
        #     'huang_p0125p05': dict(
        #         dx_min=0.0125, dx_max=0.05
        #     )
        # })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=re, tf=tf, pf=pf, cr=cr,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_cp()

    def plot_cp(self):
        plt.figure(figsize=(6, 3.8))
        for case in self.cases:
            cp2 = np.load(case.input_path('x_vs_cp.npz'))
            cp55 = np.genfromtxt("code/data/cp_naca5515_top.csv",
                                 delimiter=",", names=["t", "cp"])
            cp552 = np.genfromtxt("code/data/cp_naca5515_bot.csv",
                                  delimiter=",", names=["x", "cp"])

            plt.plot(cp55['t'], cp55['cp'], 'b-',
                     label='Huang et al. (2019)', linewidth=2.0)
            plt.plot(cp552['x'], cp552['cp'], 'b-', linewidth=2.0)
            plt.plot(2*(cp2['x'] - np.min(cp2['x'])), (cp2['cp']), 'ro',
                     label='present work', linewidth=1.5, markersize=2.5)

            plt.xlabel(r'$x/c$')
            plt.ylabel(r'$C_{p}$')
            plt.legend()
            plt.savefig(self.output_path('cp_NACA5515.pdf'))
            plt.clf()
        plt.close()


class FPCNoBG(Problem):
    def get_name(self):
        return 'fpc_no_bg'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/fpc_auto.py' + backend

        self.tf = tf = 6.0
        self.re = re = 1000
        cr = 1.08
        dx_max = 0.5
        dx_min = 0.004

        self.case_info = {
            'fpc_bg': dict(pf=200, use_bg=None),
            'fpc_no_bg': dict(pf=200),
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=re, cr=cr, tf=tf, dx_max=dx_max, dx_min=dx_min,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_cd()

    def plot_cd(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'red']) +
            cycler('linestyle', ['', '-']) +
            cycler('marker', ['.', 'None'])
        ))
        plt.figure(figsize=(4, 3.8))
        for case in self.cases:
            data = np.load(case.input_path('results.npz'))

            label = 'present work, '
            if 'use_bg' in case.params:
                label += 'w/ background particles'
            else:
                label += 'w/o background particles'
            plt.plot(data['t'], data['cd'] + data['cd_sf'], label=label)

        # RVM
        re = int(self.re)
        val = pd.read_csv(f'code/data/re_{re}.csv')
        plt.plot(val['pr_pdrag_t'][::4],
                 (val['pr_pdrag'] + val['pr_fdrag'])[::4], 'b+', mfc='none',
                 markersize=4, label='Ramachandran (2004)', zorder=0)
        plt.plot(val['kl_pdrag_t'], (val['kl_pdrag'] + val['kl_fdrag']),
                 'ko', zorder=1, markersize=4, mfc='none',
                 label='Koumotsakos et al. (1995)')
        plt.xlim(0, self.tf)
        plt.ylabel(r'$C_{d}$, total drag')
        plt.xlabel(r'$T$')
        plt.legend()
        plt.savefig(self.output_path('fpc_bg_comp.pdf'))
        plt.clf()
        plt.close()


class MovingSquareNoBG(Problem):
    def get_name(self):
        return 'moving_square_no_bg'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/moving_square.py' + backend


        self.tf = tf = 8.0
        self.re = 150
        cr = 1.12
        dx_max = 0.04

        self.case_info = {
            'ms_no_bg': dict(
                dx_min=0.0125, pf=100,
            )
        }
        self.case_info.update({
            'ms_bg': dict(dx_min=0.0125, pf=100, use_bg=None)
        })
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=self.re, tf=tf, cr=cr, dx_max=dx_max,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_cd()

    def plot_cd(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'red']) +
            cycler('linestyle', ['', '-']) +
            cycler('marker', ['.', 'None'])
        ))
        plt.figure(figsize=(4, 3.8))
        for case in self.cases:
            data = np.load(case.input_path('results.npz'))

            label = 'present work, '
            if 'use_bg' in case.params:
                label += 'w/ background particles'
            else:
                label += 'w/o background particles'
            plt.plot(data['t'], -data['cd'] - data['cd_sf'], label=label,
                     linewidth=1.0)
        re = str(int(self.re)).zfill(3)
        spheric = pd.read_csv(f"code/data/SPHERIC_Case/Force_Re{re}.csv")
        plt.plot(spheric['t'], spheric['Cd_p'] + spheric['Cd_v'], 'k-',
                 linewidth=0.5, zorder=0, label='FDNS, Colicchio et al. (2006)')
        plt.title(f'Re = {re}')
        plt.legend()
        plt.ylabel(r'$C_{d}$, total drag')
        plt.xlabel(r'$t$')
        plt.savefig(self.output_path('ms_bg_comp.pdf'))
        plt.clf()
        plt.close()


class MovingSquareSolAdaptNoBG(Problem):
    def get_name(self):
        return 'moving_square_sol_adapt_no_bg'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/moving_square.py' + backend


        self.tf = tf = 8.0
        self.re = 150
        cr = 1.12
        dx_max = 0.04

        self.case_info = {
            'ms_no_bg': dict(dx_min=0.0125, pf=100, no_use_bg=None, solution_adapt=0.05),
            'ms_bg': dict(dx_min=0.0125, pf=100, use_bg=None, solution_adapt=0.05),
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                re=self.re, tf=tf, cr=cr, dx_max=dx_max,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_prop({'no_use_bg': None}, prop='ds', cmap='jet', size=1,
                       vmin=0.0125, vmax=0.04)
        self.plot_prop({'no_use_bg': None}, prop='vor', cmap='rainbow', size=2,
                       vmin=-3, vmax=3)
        self.plot_cd()

    def plot_cd(self):
        rc('axes', prop_cycle=(
            cycler('color', ['tab:blue', 'red']) +
            cycler('linestyle', ['', '-']) +
            cycler('marker', ['.', 'None'])
        ))
        plt.figure(figsize=(4, 3.8))
        for case in self.cases:
            data = np.load(case.input_path('results.npz'))

            label = 'present work, '
            if 'use_bg' in case.params:
                label += 'SA, w/ background particles'
            else:
                label += 'SA, w/o background particles'
            plt.plot(data['t'], -data['cd'] - data['cd_sf'], label=label,
                     linewidth=1.0)
        re = str(int(self.re)).zfill(3)
        spheric = pd.read_csv(f"code/data/SPHERIC_Case/Force_Re{re}.csv")
        plt.plot(spheric['t'], spheric['Cd_p'] + spheric['Cd_v'], 'k-',
                 linewidth=0.5, zorder=0, label='FDNS, Colicchio et al. (2006)')
        plt.legend()
        plt.ylabel(r'$C_{d}$, total drag')
        plt.xlabel(r'$t$')
        plt.savefig(self.output_path('ms_bg_comp.png'))
        plt.clf()
        plt.close()

    def plot_prop(self, conditions=None, prop='ds', cmap='rainbow',
                  xmin=1, xmax=10, ymin=-2, ymax=2, figsize=(8, 5),
                  size=2, dpi=200, vmin=-6, vmax=6):
        if conditions is None:
            conditions = {}
        times = [5]
        for case in filter_cases(self.cases, **conditions):
            fig, ax = plt.subplots(1, 1, figsize=figsize)
            files = get_files(case.input_path(), 'moving_square')
            logfile = case.input_path('moving_square.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            data = load(files[0])
            t = data['solver_data']['t']
            f = data['arrays']['fluid']
            s1 = data['arrays']['solid']
            val = f.get(prop)
            cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

            tmp = ax.scatter(
                f.x[cond], f.y[cond], c=val[cond],
                s=size, rasterized=True, cmap=cmap,
                edgecolor='none', vmin=vmin, vmax=vmax
            )
            msg = r"$T = $" + f'{t:.1f}'
            ax.annotate(
                msg, (xmin*1.2, ymax*0.8), fontsize='small',
                bbox=dict(boxstyle="square,pad=0.3", fc='white')
            )
            ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size, rasterized=True)
            fig.colorbar(tmp, ax=ax, shrink=0.4, label=rf'${prop}$', pad=0.01)
            ax.grid()
            ax.set_ylabel('y/D')
            ax.set_xlabel('x/D')
            ax.set_aspect('equal', 'box')
            ax.set_xlim(xmin, xmax)
            ax.set_ylim(ymin, ymax)
            fig.savefig(self.output_path(f'{case.name}_{prop}.pdf'), dpi=dpi)
            plt.clf()
            plt.close()


class EllipseTranslatePitch(Problem):
    def get_name(self):
        return 'ellipse_trans_pitch'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/ellipse_translation.py' + backend

        self.case_info = {
            'ellipse_motion1_translation': dict(
                tf=10, pf=250,
            ),
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_prop({}, prop='vor', size=10, vmin=-3, vmax=3, figsize=(8, 5),
                       ymin=-2, ymax=2, cmap='rainbow', xmin=3.8, dpi=200,
                       times=[1.25818, 4.49911, 8.00049, 10])

    def plot_prop(self, conditions=None, prop='ds', cmap='rainbow',
                  xmin=3, xmax=14.2, ymin=-4, ymax=4, figsize=(10, 4),
                  size=20, dpi=150, vmin=-6, vmax=6, times=None):
        if conditions is None:
            conditions = {}
        if times is None:
            times = [1, 2, 3]
        for case in filter_cases(self.cases, **conditions):
            filename_w_ext = os.path.basename(case.base_command.split(' ')[1])
            filename = os.path.splitext(filename_w_ext)[0]
            files = get_files(case.input_path(), filename)
            logfile = case.input_path(f'{filename}.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            for file, t in zip(files, times):
                fig, ax = plt.subplots(1, 1, figsize=figsize)
                data = load(file)
                t = data['solver_data']['t']
                f = data['arrays']['fluid']
                s1 = data['arrays']['solid']
                label = rf"${prop}$"
                val = f.get(prop)
                cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

                tmp = ax.scatter(
                    f.x[cond], f.y[cond], c=val[cond],
                    s=size*f.m[cond]/np.max(f.m[cond]),
                    rasterized=True, cmap=cmap,
                    edgecolor='none', vmin=vmin, vmax=vmax
                )
                msg = r"$T = $" + f'{t:.1f}'
                ax.annotate(
                    msg, (xmin*1.2, ymax*0.8), fontsize='small',
                    bbox=dict(boxstyle="square,pad=0.3", fc='white')
                )
                ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size,
                           rasterized=True)
                ax.grid()
                ax.set_aspect('equal')
                ax.set_xlim(xmin, xmax)
                ax.set_ylim(ymin, ymax)
                fig.savefig(self.output_path(f'{prop}_t_{t:.1f}.pdf'),
                            dpi=dpi)
                plt.clf()
                plt.close()
            # Generate standalone colorbar.
            figc, axc = plt.subplots(figsize=(0.2, figsize[1]))
            figc.subplots_adjust(bottom=0.5)

            norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
            figc.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), cax=axc,
                            orientation='vertical', label=label)
            figc.savefig(self.output_path(f'cbar.pdf'), dpi=dpi)


class EllipsePlunging(EllipseTranslatePitch):
    def get_name(self):
        return 'ellipse_plunging'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/ellipse_plunging.py' + backend

        self.case_info = {
            'ellipse_motion2_plunging': dict(
                tf=10, pf=100,
            ),
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_prop({}, prop='vor', size=10, vmin=-20, vmax=20, figsize=(5, 5),
                       ymin=-1.5, ymax=5.5, cmap='rainbow', xmin=5, xmax=12, dpi=150,
                       times=[0.5013, 1, 1.49947, 2])


class EllipseRotation(EllipseTranslatePitch):
    def get_name(self):
        return 'ellipse_rotation'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/xoler_vawt_motion.py' + backend

        self.tf = tf = 10.0
        pf = 500
        cr = 1.15

        self.case_info = {
            'ellipse_67k': dict(
                re=67000, dx_max=0.1, dx_min=0.005
            )
        }
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                pf=pf, cr=cr, tf=tf,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_prop({}, prop='vor', size=15, vmin=-20, vmax=20, figsize=(6, 6),
                       ymin=-5, ymax=5, cmap='seismic', xmin=5, xmax=20,
                       times=[2.49544, 5, 7.50506])

    def plot_prop(self, conditions=None, prop='ds', cmap='rainbow',
                  xmin=3, xmax=14.2, ymin=-4, ymax=4, figsize=(10, 4),
                  size=20, dpi=300, vmin=-6, vmax=6, times=None):
        if conditions is None:
            conditions = {}
        if times is None:
            times = [1, 2, 3]
        for case in filter_cases(self.cases, **conditions):
            filename_w_ext = os.path.basename(case.base_command.split(' ')[1])
            filename = os.path.splitext(filename_w_ext)[0]
            files = get_files(case.input_path(), filename)
            logfile = case.input_path(f'{filename}.log')
            files = get_files_at_given_times_from_log(files, times, logfile)
            for file, t in zip(files, times):
                fig, ax = plt.subplots(1, 1, figsize=figsize)
                data = load(file)
                t = data['solver_data']['t']
                f = data['arrays']['fluid']
                s1 = data['arrays']['solid_0']
                s2 = data['arrays']['solid_1']
                label = rf"${prop}$"
                val = f.get(prop)
                cond = (f.x < xmax) & (f.x > xmin) & (f.y < ymax) & (f.y > ymin)

                tmp = ax.scatter(
                    f.x[cond], f.y[cond], c=val[cond],
                    s=size*f.m[cond]/np.max(f.m[cond]),
                    rasterized=True, cmap=cmap,
                    edgecolor='none', vmin=vmin, vmax=vmax
                )
                msg = r"$T = $" + f'{t:.1f}'
                ax.annotate(
                    msg, (xmin*1.2, ymax*0.8), fontsize='small',
                    bbox=dict(boxstyle="square,pad=0.3", fc='white')
                )
                ax.scatter(s1.x, s1.y, c=s1.m, cmap='viridis', s=size,
                           rasterized=True)
                ax.scatter(s2.x, s2.y, c=s2.m, cmap='viridis', s=size,
                           rasterized=True)
                cbar = fig.colorbar(tmp, ax=ax, shrink=0.4, label=f'{label}',
                                    pad=0.01)
                ax.grid()
                ax.set_aspect('equal')
                ax.set_xlim(xmin, xmax)
                ax.set_ylim(ymin, ymax)
                cbar.ax.tick_params(labelsize='large')
                fig.savefig(self.output_path(f'{prop}_t_{t:.1f}.png'),
                            dpi=dpi)
                plt.clf()
                plt.close()


class SShape(EllipseTranslatePitch):
    def get_name(self):
        return 's_shape'

    def setup(self):
        get_path = self.input_path
        cmd = 'python code/s_shape.py' + backend

        self.case_info = {'s_shape_re_2k': dict(tf=10, pf=250)}
        self.cases = [
            Simulation(
                get_path(*name.split('/')), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, compress_output=None,
                **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self.plot_prop({}, prop='vor', size=35, vmin=-6, vmax=6, figsize=(6, 6),
                       ymin=-1.5, ymax=1.5, cmap='rainbow', dpi=100,
                       xmin=13.5, xmax=16.5, times=[2, 5, 8, 10])


class PerfAnalysisBreakdown(Problem):
    def get_name(self):
        return 'perf_analysis_break_down'

    def setup(self):
        self.cases = []
        self.cases.append(
            Simulation(
                'ms_no_omp', 'python code/performance.py',
                job_info=dict(n_core=1, n_thread=1),
                dx_min=0.0075, pf=50, max_steps=1000,
                cache_nnps=None, compress_output=None,
                lt=30, wt=10, dx_max=0.2, cr=1.15,
                disable_output=None,
            )
        )
        self.cases.append(
            Simulation(
                'ms_omp', 'python code/performance.py --openmp',
                job_info=dict(n_core=6, n_thread=6),
                dx_min=0.0075, pf=50, max_steps=1000,
                cache_nnps=None, compress_output=None,
                lt=30, wt=10, dx_max=0.2, cr=1.15,
                disable_output=None,
            )
        )
        self.cases.append(
            Simulation(
                'ms_omp_4_cores', 'python code/performance.py --openmp',
                job_info=dict(n_core=4, n_thread=4),
                dx_min=0.0075, pf=50, max_steps=1000,
                cache_nnps=None, compress_output=None,
                lt=30, wt=10, dx_max=0.2, cr=1.15,
                disable_output=None,
            )
        )

    def run(self):
        self.make_output_dir()


if __name__ == '__main__':
    PROBLEMS = [
        Bao1Moving,
        Bao2Static,
        MovingSquare,
        MovingSquareRe,
        SunAirfoil108AoA4,
        AirfoilHuang,
        FPCNoBG,
        MovingSquareNoBG,
        MovingSquareSolAdaptNoBG,
        EllipseTranslatePitch,
        EllipsePlunging,
        SShape,
        PerfAnalysisBreakdown,
    ]
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()
