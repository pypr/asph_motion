# Applications of adaptive SPH for complex moving bodies

This repository contains the code and manuscript for research done on
Applications of adaptive SPH for complex moving bodies in 2D.


## File tree

The following are the contents of the of this repository. The `code/` directory
contains algorithms and examples that are used in the manuscript, the
`manuscript/` directory contains the LaTeX code for the generation of the
manuscript, the `automate.py` file runs all the test cases that are used in the
present study, the file `requirements.txt` specifies the dependencies that are
required to run the current code and the license is at `LICENSE.txt`. The
`code/data/` and `plot_cdcyls/` directories contain validation data taken from
established results in the literature.


```bash
.
|-- CREDITS.md
|-- LICENSE.txt
|-- README.md
|-- automate.py
|-- code
|   |-- adapt.py
|   |-- adaptv.py
|   |-- airfoil_3_motion.py
|   |-- airfoil_pitchplunge_motion.py
|   |-- airfoil_purepitch_motion.py
|   |-- airfoil_pureplunge_motion.py
|   |-- airfoil_stationary_huang.py
|   |-- airfoil_stationary_naca.py
|   |-- airfoils2.py
|   |-- data
|   |   |-- SPHERIC_Case
|   |   |   |-- Force_Re050.csv
|   |   |   |-- Force_Re100.csv
|   |   |   |-- Force_Re150.csv
|   |   |   |-- Force_Re600.csv
|   |   |   `-- re600_marrone.csv
|   |   |-- cd_spheric_Re100.csv
|   |   |-- cp_naca5515_bot.csv
|   |   |-- cp_naca5515_top.csv
|   |   |-- fpc_bao
|   |   |   |-- bao_1moving
|   |   |   |   |-- omega_0.5184_amp_0.25
|   |   |   |   |   `-- cl.csv
|   |   |   |   |-- omega_0.9331_amp_0.25
|   |   |   |   |   `-- cl.csv
|   |   |   |   |-- omega_1.1404_amp_0.25
|   |   |   |   |   `-- cl.csv
|   |   |   |   |-- omega_1.5551_amp_0.25
|   |   |   |   |   `-- cl.csv
|   |   |   |   `-- omega_1.5551_amp_1.25
|   |   |   |       `-- cl.csv
|   |   |   `-- bao_2static
|   |   |       |-- g1.2csv
|   |   |       |   |-- cdl.csv
|   |   |       |   |-- cdu.csv
|   |   |       |   |-- cll.csv
|   |   |       |   `-- clu.csv
|   |   |       |-- g1.5csv
|   |   |       |   |-- cdl.csv
|   |   |       |   |-- cdu.csv
|   |   |       |   |-- cll.csv
|   |   |       |   `-- clu.csv
|   |   |       |-- g2.5csv
|   |   |       |   |-- cdl.csv
|   |   |       |   |-- cdu.csv
|   |   |       |   |-- cll.csv
|   |   |       |   `-- clu.csv
|   |   |       `-- g4.0csv
|   |   |           |-- cdl.csv
|   |   |           |-- cdu.csv
|   |   |           |-- cll.csv
|   |   |           `-- clu.csv
|   |   |-- msq_re_600
|   |   |   |-- re600_FDNS.csv
|   |   |   `-- re600_marrone.csv
|   |   |-- naca_0008_aoa_4_re_2k.csv
|   |   |-- naca_0008_aoa_4_re_6k.csv
|   |   |-- naca_0012_aoa_6_re_10k.csv
|   |   `-- re_1000.csv
|   |-- edac.py
|   |-- ellipse.py
|   |-- ellipse_plunging.py
|   |-- ellipse_translation.py
|   |-- fpc.py
|   |-- fpc2.py
|   |-- fpc_auto.py
|   |-- fpc_bao.py
|   |-- fpc_bao_oscillation_1cylinder.py
|   |-- fpc_bao_oscillation_2cylinders.py
|   |-- fpc_bao_static.py
|   |-- fpc_huang.py
|   |-- hybrid_simple_inlet_outlet.py
|   |-- moving_airfoil.py
|   |-- moving_cylinder.py
|   |-- moving_square.py
|   |-- no_bg.py
|   |-- oscillating_square.py
|   |-- performance.py
|   |-- rb_rotation.py
|   |-- s_shape.py
|   |-- sun_airfoil08_aoa4.py
|   |-- sun_airfoil12_aoa6.py
|   `-- wall_normal.py
|-- manuscript
|   |-- Makefile
|   |-- macros.tex
|   |-- model6-num-names.bst
|   |-- paper.tex
|   |-- references.bib
`-- requirements.txt
```

The algorithms mentioned in the manuscript are primarily contained in `edac.py`,
`adapt.py`, `adaptv.py`, `no_bg.py`. The `wall_normal.py` contains the algorithm
to compute the normals and `hybrid_inlet_outlet.py` contain the code which
implements the no-reflection boundary conditions.

The following files in `code/` directory contain the test problems used in the
manuscript:
 - `fpc_auto.py` - Flow past a stationary cylinder.
 - `moving_square.py` - Flow past a moving square (SPHERIC test case).
 - `ellipse_plunging.py` - Flow past a plunging ellipse at Re = 500.
 - `ellipse_tranlation.py` - Flow past a translating ellipse at Re = 500.
 - `fpc_bao_static.py` - Flow past two stationary cylinders at Re = 100.
 - `fpc_bao_oscillation_1cylinder.py` - Flow past single oscillating cylinder
   (Re = 100).
 - `airfoil_stationary_naca.py` - Flow past a stationary NACA5515 airfoil.
 - `s_shape.py` - Flow past a rotating S-shape are Re = 2000.
 - `sun_airfoil08_aoa4.py` - Flow past a NACA0008 airfoil at 4 deg AoA and Re = 2000.
 - `sun_airfoil12_aoa6.py` - Flow past a NACA0012 airfoil at 6 deg AoA and Re = 6000.
 - `performance.py` - Code to measure the parallel performance of the algorithms.

The helper file `hybrid_inlet_outlet.py` is used to implement the inlet/outlet
boundary conditions for flow past cylinder and `rb_rotation.py` is a simple
example to visualize the motion of the body.

Details and run time of the problems in `automate.py`. Cases in increasing order
of run time measured on a 16 core machine.

1. `PerfAnalysisBreakdown` - Cases analyzing performance (15 mins, 5
   mins/case) - 3 cases.

2. `MovingSquareNoBG` - Flow past a moving square (3 hrs, 1.5 hrs/case) - 2
   cases demonstrating without background particle approach.

3. `AirfoilHuang` - Flow past a NACA5515 airfoil (1.5 hr, 1.5 hrs/case) - 1
   case.

4. `MovingSquare` - Flow past a moving square (6 hrs, 2 hrs/case) - 3 cases with
   varying resolution.

5. `MovingSquareRe` - Flow past a moving square at different Reynolds numbers
   (10 hrs, 2.5 hrs/case) - 4 cases with varying Reynolds numbers.

6. `FPCNoBG` - Flow past a cylinder (6 hrs, 3 hrs/case) - 2 cases demonstrating
   without background particle approach.

7. `MovingSquareSolAdaptNoBG` - Flow past a moving square (7 hrs, 3.5
   hrs/case) - 2 cases with solution adaptivity demonstrating without background
   particle approach.

8. `SShape` - Flow past a rotating S-shape (6 hrs, 6 hrs/case) - 1 case

9. `EllipsePlunging` - Flow past a plunging ellipse (10 hrs, 10 hrs/case) - 1
   case

10. `EllipseTranslatePitch` - Flow past a translating and pitching ellipse (10
    hrs, 10 hrs/case) - 1 case

11. `Bao1Moving` - Flow past a moving cylinder (40 hrs, 8 hrs/case) - 5 cases
    with varying amplitude and frequency of oscillation.

12. `SunAirfoil108AoA4` - Flow past a static airfoil (100 hrs, 25 hrs/case) - 4
    cases with varying angles of attack and Reynolds numbers.

13. `Bao2Static` - Flow past two static cylinders (119 hrs, 40 hrs/case
	average) - 3 cases with varying gap spacing between cylinders.


## Installation

This requires pysph to be setup along with automan. See the
`requirements.txt`. To setup perform the following:

0. Setup a suitable Python distribution, using
   [edm](https://docs.enthought.com/edm/) or [conda](https://conda.io) or a
   [virtualenv](https://virtualenv.pypa.io/).

1. Clone this repository:
```
    $ git clone https://gitlab.com/pypr/asph_motion.git
```

2. Run the following from your Python environment:
```
    $ cd asph_motion
    $ pip install -r requirements.txt
```

## Running an example

A simple example to check if your installation proceeded as expected, run the
following command from the top level directory:

    $ python code/moving_square.py --max-steps 5000

it will take about 30 mins on a single core CPU to run the above simulation and
the output will be generated in the current directory with a folder named
`moving_square_output`. To view the results run,

    $ pysph view moving_square_output


## Generating the results

The paper and the results are all automated using the
[automan](https://automan.readthedocs.io) package which should already be
installed as part of the above installation. This will perform all the
required simulations (this can take a while) and also generate all the plots
for the manuscript.

To use the automation code, do the following::

    $ python automate.py
    # or
    $ ./automate.py

By default the simulation outputs are in the ``outputs`` directory and the
final plots for the paper are in ``manuscript/figures``.


## Building the paper

The manuscript is written with LaTeX and if you have that installed you may do
the following:

```
$ cd manuscript
$ pdflatex paper.tex
$ bibtex paper
$ pdflatex paper.tex
$ pdflatex paper.tex
```
