"""
Flow past cylinder
"""
import os
import logging
from time import time
import numpy
import numpy as np
from pysph.tools import geometry as G
from pysph.base.utils import get_particle_array
from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation, Group

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme
from fpc_auto import (
    WindTunnel, EvaluateCharacterisctics, ShepardInterpolateCharacteristics,
    EvaluatePropertyfromCharacteristics
)

logger = logging.getLogger()


# Motion parameters
T = 30
A = 0.5
omega = 2*np.pi/T

# Fluid mechanical/numerical parameters
rho = 1000
u_square = 1.0
umax = 2 * u_square
c0 = 10 * umax
p0 = rho * c0 * c0


def check_time_outlet_bc(t, dt):
    if t < 9:
        return True
    return False


def check_time_solid_bc(t, dt):
    if t >= 9:
        return True
    return False


class IdentifyBoundary(Equation):
    def __init__(self, dest, sources, dim):
        self.dim = dim
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_div_r, d_surface):
        d_div_r[d_idx] = 0.0
        d_surface[d_idx] = 0

    def loop(self, d_idx, d_div_r, XIJ, DWIJ, s_m, s_rho, s_idx):
        Vj = s_m[s_idx]/s_rho[s_idx]
        xijdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]
        d_div_r[d_idx] -= Vj * xijdotdwij

    def post_loop(self, d_idx, d_div_r, d_surface):
        if d_div_r[d_idx] < self.dim - 0.5:
            d_surface[d_idx] = 1


class MovingSquare(WindTunnel):
    def initialize(self):
        super().initialize()
        self.side = 1 * self.dc

    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(
            potential=False, sol_adapt=False, Lt=10, Wt=2.5, tf=5, dc=1, re=150,
            dx_min=0.0125, dx_max=0.04, cr=1.15
        )

    def consume_user_options(self):
        if self.options.n_damp is None:
            self.options.n_damp = 20
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        self.sol_adapt = self.options.sol_adapt
        dx_min = self.options.dx_min
        dx_max = self.options.dx_max
        cr = self.options.cr
        self.dx_min = dx_min
        self.dx_max = dx_max
        self.cr = cr
        re = self.options.re

        self.nu = nu = u_square * self.dc / re

        self.hdx = hdx = self.options.hdx
        self.nl = (int)(10.0*hdx)
        self.c0 = c0

        self.h = h = hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        # This division by a factor of 2 is needed. The umax in the domain is
        # upto 2.8 and not 1.0. U free stream is mislabelled  as umax.
        dt_cfl = cfl * h / ((self.c0 + umax))
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        self.tf = 8.2
        self.vacondio = self.options.vacondio
        self.hybrid = self.options.hybrid
        self.wake = self.options.wake

        self.side = 1.0*self.dc

    def _create_solid(self):
        dx = self.dx_min
        self.cxy = self.Lt/6.66666, 0.0
        x0, y0 = self.cxy
        side = self.side
        y0 -= side*0.5
        h0 = self.hdx*dx
        x, y = np.mgrid[x0-side/2:x0+side/2:dx, y0:y0+side:dx]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, x0=x, u=0.0, m=volume*rho, rho=rho, h=h0,
            ds=dx
        )
        solid.add_constant('ds_min', dx)
        return solid

    def _create_box(self):
        dx = self.dx_max
        m = rho * dx * dx
        h0 = self.hdx*dx
        layers = self.nl * dx
        w = self.Wt
        l = self.Lt
        x, y = np.mgrid[-layers:l+layers:dx, -w-layers:w+layers:dx]

        fluid = (x < l) & (x > 0) & (y < w) & (y > -w)
        xf, yf = x[fluid], y[fluid]
        xw, yw = x[~fluid], y[~fluid]
        fluid = get_particle_array(
            name='fluid', x=xf, y=yf, m=m, h=h0, rho=rho, p=0.0, vmag=0.0
        )

        wall = get_particle_array(name='wall', x=xw, y=yw, m=m, h=h0, rho=rho)
        return fluid, wall

    def create_equations(self):
        from pysph.sph.equation import Group
        from edac import EvaluateNumberDensity

        equations = self.scheme.get_equations()
        eq = []
        eq.append(
            Group(equations=[
                EvaluateCharacterisctics(
                    dest='fluid', sources=None, c_ref=self.c0, rho_ref=rho,
                    u_ref=0.0, v_ref=0.0, p_ref=0.0
                )
            ], condition=check_time_outlet_bc)
        )
        eq.append(
            Group(equations=[
                EvaluateNumberDensity(dest='wall', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='wall', sources=['fluid']),
            ], condition=check_time_outlet_bc)
        )
        eq.append(Group(equations=[
            EvaluatePropertyfromCharacteristics(
                dest='wall', sources=None, c_ref=self.c0, rho_ref=rho,
                u_ref=0.0, v_ref=0.0, p_ref=0.0
            )], condition=check_time_outlet_bc)
        )
        wall_solid_bc = equations[2].equations.pop()
        condition_outlet_bc = Group(
            equations=[wall_solid_bc],
            condition=check_time_solid_bc
        )
        equations.insert(3, condition_outlet_bc)
        equations = eq + equations
        return equations

    def _get_normals(self, pa):
        from pysph.tools.sph_evaluator import SPHEvaluator
        from wall_normal import ComputeNormals, SmoothNormals

        pa.add_property('xn')
        pa.add_property('yn')
        pa.add_property('normal', stride=3)
        pa.add_property('normal_tmp', stride=3)

        name = pa.name

        props = ['m', 'rho', 'h']
        for p in props:
            x = pa.get(p)
            if numpy.all(x < 1e-12):
                msg = f'WARNING: cannot compute normals "{p}" is zero'
                print(msg)

        seval = SPHEvaluator(
            arrays=[pa], equations=[
                Group(equations=[
                    ComputeNormals(dest=name, sources=[name])
                ]),
                Group(equations=[
                    SmoothNormals(dest=name, sources=[name])
                ]),
            ],
            dim=2, domain_manager=self.domain, kernel=QuinticSpline(dim=2)
        )
        seval.evaluate()
        pa.xn[:] = -pa.normal[::3]
        pa.yn[:] = -pa.normal[1::3]

    def create_particles(self):
        fluid, wall = self._create_box()
        solid = self._create_solid()

        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
        self.wake = wake

        particles = [fluid, solid, wall]
        boundary = [solid]
        self.shift_sources = list(particles)

        if wake is not None:
            particles.append(wake)
            boundary.append(wake)
        self.scheme.setup_properties(particles, clean=False)

        setup_properties([fluid])
        self._get_normals(wall)
        fluid.add_property('vmag')

        bg_freq = 2
        adapt_freq = 5

        self._update_bg = UpdateBackground(
            fluid, dim=2, boundary=boundary, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=adapt_freq,
            bg_freq=bg_freq, sol_adapt=self.sol_adapt,
            use_bg=self.options.use_bg
        )
        sep = '-'*70
        msg = 'Generating background (BG) particles:'
        print(sep, '\n', msg)
        start = time()
        self._update_bg.set_initial_bg()
        msg = f'time took to generate the BG particles, {time()-start:.4f}s'
        print(msg, '\n', sep)
        logger.info('BG initialize:\n%s\n  %s\n%s', sep, msg, sep)
        self._update_bg.initialize_fluid(
            solids=[solid], vacondio=self.vacondio, hybrid=self.hybrid,
            output_dir=self.output_dir
        )
        if self.options.use_bg:
            bg_pa = self._update_bg.bg_pa
            G.remove_overlap_particles(bg_pa, solid, self.dx_min, dim=2)
            particles.append(bg_pa)

        props = ['u0', 'v0', 'w0', 'x0', 'y0', 'z0']
        for prop in props:
            if prop not in solid.properties:
                solid.add_property(prop)

        props = ['J1', 'J2u', 'J3u', 'J2v', 'J3v']
        for prop in props:
            fluid.add_property(prop)
        # fluid.add_output_arrays(props)
        props = [ 'J1', 'avg_j1', 'J2u', 'avg_j2u', 'J3u', 'avg_j3u', 'J2v',
                 'avg_j2v', 'J3v', 'avg_j3v']
        for p in props:
            if p not in wall.properties:
                wall.add_property(p)
        solid.add_output_arrays(['au'])
        return particles

    def create_inlet_outlet(self, particle_arrays):
        return []

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=None,
            nu=nu, h=None, inlet_outlet_manager=None, cfl=None,
            inviscid_solids=['wall']
        )
        return s

    def configure_scheme(self):
        from pysph.sph.integrator_step import TwoStageRigidBodyStep
        scheme = self.scheme
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl, c0=self.c0)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0,
            output_at_times=list(range(1, 7)),
            extra_steppers={'solid': TwoStageRigidBodyStep()}
        )

    def square_motion(self, t):
        a = 2.8209512
        b = 0.525652151
        c = 0.14142151
        d = -2.55580905e-08

        au = a * np.exp(-(t - b) * (t - b) / (2.0 * c * c)) + d
        return au

    def pre_step(self, solver):
        t = solver.t
        for particle in self.particles:
            if particle.name == 'solid':
                au = self.square_motion(t)
                particle.au[:] = au

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()

    def post_process(self, info_filename):
        self.read_info(info_filename)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')

        t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t('fluid', 'solid')
        self._plot_coeffs(t, cd, cl, cd_sf, cl_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _plot_coeffs(self, t, cd, cl, cd_sf, cl_sf):
        import pandas as pd
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        dir_path = os.path.abspath(os.path.dirname(__file__))
        re = str(int(self.options.re)).zfill(3)
        path = os.path.join(dir_path, f"data/SPHERIC_Case/Force_Re{re}.csv")
        data = pd.read_csv(path)
        plt.figure()
        plt.plot(t, -(cd + cd_sf), 'r-', linewidth=1, label='Adaptive-SPH')
        plt.plot(data['t'], data['Cd_p'] + data['Cd_v'], 'k-', linewidth=0.5, zorder=0,
                 label='SPHERIC result')
        plt.title(f'Re = {re}')
        plt.legend()
        plt.xlabel(r'$t$')
        plt.ylabel('cd')
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def customize_output(self):
        self._mayavi_config('''
        if 'bg' in particle_arrays:
            particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        for name in ['fluid']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        b = particle_arrays['solid'].scalar = 'm'
        for name in ['fluid', 'solid', 'wall']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')


if __name__ == '__main__':
    app = MovingSquare()
    app.run()
    app.post_process(app.info_filename)
