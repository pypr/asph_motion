"""
Flow past cylinder
"""
import logging
from time import time
import os
import numpy as np
from numpy import pi, cos, sin, exp

from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme
from flow_past_cylinder import SolidWallNoSlipBCReverse, ResetInletVelocity


# Motion parameters
T = 30
A = 0.5 # A = [0.3, 1.5] A/D = ?? 
omega = 2*np.pi/T  # omega=6.283 / T = 0.628

logger = logging.getLogger()

# Fluid mechanical/numerical parameters
rho = 1
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0

u0 = -0.25

class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 50.0  # length of tunnel
        self.Wt = 15.0  # half width of tunnel
        self.dc = 1.2  # diameter of cylinder
        self.cxy = (self.Lt - 20.0), 0.0  # center of cylinder
        self.nl = 10  # Number of layers for wall/inlet/outlet
        self.s = 2.5 * self.dc
        # self.uo = -0.25

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=100,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--dx-min", action="store", type=float, dest="dx_min",
            default=0.0625,
            help="Minimum resolution."
        )
        group.add_argument(
            "--dx-max", action="store", type=float, dest="dx_max",
            default=0.5,
            help="Maximum resolution."
        )
        cr = pow(2.0, 1.0/4)
        group.add_argument(
            "--cr", action="store", type=float, dest="cr",
            default=cr,
            help="Resolution step ratio."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=40,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=20,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--dc", action="store", type=float, dest="dc", default=2.0,
            help="Diameter of the cylinder."
        )
        add_bool_argument(
            group, 'potential', dest='potential', default=False,
            help='Initialize with potential flow.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=False,
            help="Use Vacondio et al. approach for splitting"
        )
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        group.add_argument(
            "--gsf", action="store", type=float, dest="gsf_factor",
            default=1.5,
            help="gap specing factor, spacing bw two cyls / cyl dia."
        )

    def consume_user_options(self):
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        dx_min = self.options.dx_min
        dx_max = self.options.dx_max
        cr = self.options.cr
        self.dx_min = dx_min
        self.dx_max = dx_max
        self.cr = cr
        re = self.options.re

        self.nu = nu = umax * self.dc / re
        # self.cxy = self.Lt / 5., 0.0

        self.hdx = hdx = self.options.hdx
        self.nl = (int)(6.0*hdx)

        self.h = h = hdx * self.dx_min
        cfl = self.options.cfl_factor
        dt_cfl = cfl * h / (c0 + umax)
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        self.tf = 20.0
        self.vacondio = self.options.vacondio

        self.gsf = self.options.gsf_factor
        self.s = self.gsf * self.dc

    def _create_fluid(self):
        dx = self.dx_max
        h0 = self.hdx*dx
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0, V=1.0 / volume, u=umax,
            p=0.0, rho=rho, vmag=0.0)
        xc, yc = self.cxy
        xnew = x - xc
        ynew = y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = (umax - a2/(znew*znew)).conjugate()
        if self.options.potential:
            fluid.u[:] = vel.real
            fluid.v[:] = vel.imag
            fluid.vmag[:] = np.abs(vel)
            fluid.p[:] = 500. - 0.5 * rho * np.abs(vel)**2
        return fluid

    # def _create_solid(self):
    #     dx = self.dx_min
    #     h0 = self.hdx*dx

    #     x = []
    #     y = []
    #     fluid = self._create_fluid()
    #     xf, yf  = fluid.x, fluid.y
    #     cond = ((30 < xf) & (xf < 32.0) & (-1 < yf) & (yf < 1.0))
    #     x = np.array(xf[cond])
    #     y = np.array(yf[cond])
    #     x, y = (t.ravel() for t in (x, y))
    #     # x += self.cxy[0]
    #     volume = dx*dx
    #     solid = get_particle_array(
    #         name='solid', x=x, y=y, x_0=x,
    #         m=volume*rho, rho=rho, h=h0, V=1.0/volume
    #     )   
    #     # solid.y += self.s/2.0     
    #     solid.add_constant('ds_min', dx)
    #     return solid

    def _create_solid(self): 
        dx = self.dx_min 
        # define self.cxy = self.Lt - 20
        # self.uo = -0.25
        x0, y0 = self.cxy
        l = 2.0
        w = 2.0        
        y0 -= w*0.5
        h0 = self.hdx*dx
        x, y = np.mgrid[x0-l/2:x0+l/2:dx, y0:y0+w:dx]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, y_0=y, # u=u0,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume, ds=dx
        )
        # solid.y += self.s/2.0     
        # solid.x += center[0]
        # solid.y += center[1]
        solid.add_constant('ds_min', dx)
        return solid

        # x = []
        # y = []
        # fluid = self._create_fluid()
        # xf, yf  = fluid.x, fluid.y
        # cond = ((30 < xf) & (xf < 32.0) & (-1 < yf) & (yf < 1.0))
        # x = np.array(xf[cond])
        # y = np.array(yf[cond])
        # x, y = (t.ravel() for t in (x, y))


    def _create_pseudo_wake(self):
        dx = self.dx_min*self.cr**2
        dc = self.dc
        x0, y0 = self.cxy
        l = 2*self.dc
        w = 1.25*self.dc
        x0 += dc*1.25
        y0 -= w*0.5
        h0 = self.hdx*dx
        x, y = np.mgrid[x0:x0+l:dx, y0:y0+w:dx]
        volume = dx*dx
        wake = get_particle_array(
            name='wake', x=x, y=y,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume)
        wake.add_constant('ds_min', dx)
        return wake

    def _create_wall(self):
        dx = self.dx_max
        h0 = self.hdx*dx
        x0, y0 = np.mgrid[
            dx/2: self.Lt+self.nl*dx+self.nl*dx: dx, dx/2: self.nl*dx: dx]
        x0 -= self.nl*dx
        y0 -= self.nl*dx+self.Wt
        x0 = np.ravel(x0)
        y0 = np.ravel(y0)

        x1 = np.copy(x0)
        y1 = np.copy(y0)
        y1 += self.nl*dx+2*self.Wt
        x1 = np.ravel(x1)
        y1 = np.ravel(y1)

        x0 = np.concatenate((x0, x1))
        y0 = np.concatenate((y0, y1))
        volume = dx*dx
        wall = get_particle_array(
            name='wall', x=x0, y=y0, m=volume*rho, rho=rho, h=h0,
            V=1.0/volume)
        return wall

    def _set_wall_normal(self, pa):
        props = ['xn', 'yn', 'zn']
        for p in props:
            pa.add_property(p)

        y = pa.y
        cond = y > 0.0
        pa.yn[cond] = 1.0
        cond = y < 0.0
        pa.yn[cond] = -1.0

    def _create_outlet(self):
        dx = self.dx_max
        h0 = self.hdx*dx
        nl = self.nl
        x, y = np.mgrid[dx/2:nl * dx:dx,  -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x += self.Lt
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        # Use uhat=umax. So that the particles are moving out, if 0.0 is used
        # instead, the outlet particles will not move.
        outlet = get_particle_array(
            name='outlet', x=x, y=y, m=m, h=h0, V=1.0/volume, u=umax,
            p=0.0, rho=one * rho, uhat=umax, vhat=0.0)
        return outlet

    def _create_inlet(self):
        dx = self.dx_max
        h0 = self.hdx*dx
        nl = self.nl
        x, y = np.mgrid[dx / 2:nl*dx:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        x = x - nl * dx
        one = np.ones_like(x)
        volume = one * dx * dx

        inlet = get_particle_array(
            name='inlet', x=x, y=y, m=volume * rho, h=h0, u=umax, rho=rho,
            V=1.0 / volume, p=0.0)
        xc, yc = self.cxy
        xnew = x - xc
        ynew = y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = (umax - a2/(znew*znew)).conjugate()
        if self.options.potential:
            inlet.u[:] = vel.real
            inlet.v[:] = vel.imag
            inlet.p[:] = 500. - 0.5 * rho * np.abs(vel)**2
        return inlet

    def create_particles(self):
        dx = self.dx_min
        fluid = self._create_fluid()
        solid = self._create_solid()
        # solid2 = self._create_solid2()
        outlet = self._create_outlet()
        inlet = self._create_inlet()
        wall = self._create_wall()
        
        # G.remove_overlap_particles(fluid, solid, self.dx_max, dim=2)

        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        # wake = self._create_pseudo_wake()
        wake = None
        self.wake = wake

        particles = [fluid, inlet, outlet, solid, wall]
        boundary = [solid]

        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        if wake is not None:
            particles.append(wake)
            boundary.append(wake)
        self.scheme.setup_properties(particles)

        setup_properties([fluid, inlet, outlet])
        # XXX See if this rtmp can also be done away with.
        fluid.add_property('rtmp')
        fluid.add_property('mtmp')
        fluid.add_property('vmag')

        self._set_wall_normal(wall)

        self._update_bg = UpdateBackground(
            fluid, dim=2, boundary=boundary, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=5, bg_freq=500
        )
        # sep = '-'*70
        # msg = 'Generating background (BG) particles:'
        # print(sep, '\n', msg)
        # start = time()
        # self._update_bg.set_initial_bg()
        # msg = f'time took to generate the BG particles, {time()-start:.4f}s'
        # print(msg, '\n', sep)
        # logger.info('BG initialize:\n%s\n  %s\n%s', sep, msg, sep)
        # self._update_bg.initialize_fluid(solids=[solid])
        # bg_pa = self._update_bg.bg_pa
        # retain_layers = 10
        # cond = ((solid.x - self.cxy[0])**2 + (solid.y - self.cxy[1])**2
        #         < (self.dc/2 - retain_layers*self.dx_min)**2)

        # indices = np.where(cond)[0]
        # bg_pa.remove_particles(indices)
        # solid.remove_particles(indices)
        # particles.append(bg_pa)

        self._update_bg.set_initial_bg()
        self._update_bg.initialize_fluid(solids=[solid])
        bg_pa = self._update_bg.bg_pa
        particles.append(bg_pa)
        
        xc, yc = self.cxy
        xnew = fluid.x - xc
        ynew = fluid.y - yc
        znew = xnew + 1j*ynew
        a2 = (self.dc * 0.5)**2
        vel = (umax - a2/(znew*znew)).conjugate()
        if self.options.potential:
            fluid.u[:] = vel.real
            fluid.v[:] = vel.imag
            fluid.uhat[:] = vel.real
            fluid.vhat[:] = vel.imag
            fluid.vmag[:] = np.abs(vel)
            fluid.p[:] = 500 - 0.5 * rho * np.abs(vel)**2

        solid.add_property('y_0')
        solid.y_0[:] = solid.y  
        fluid.uag[:] = 1.0
        fluid.uta[:] = 1.0
        outlet.uta[:] = 1.0
        return particles

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], shift_factor=0.1, dx_min=0.0
        )
        return s

    def create_tools(self):
        af = self._update_bg.adapt_fluid
        arrays = list(self.particles)
        if self.wake is not None:
            arrays.remove(self.wake)
        return [self._update_bg, self._update_bg.adapt_fluid]

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx_max)
        scheme.configure(h=self.h, nu=self.nu, dx_min=self.dx_min)
        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0
        )

    def _get_io_info(self):
        from pysph.sph.bc.hybrid.inlet import Inlet
        from pysph.sph.bc.hybrid.outlet import Outlet
        from hybrid_simple_inlet_outlet import SimpleInletOutlet

        inleteqns = [
            ResetInletVelocity('ghost_inlet', None, U=-umax, V=0.0, W=0.0),
            ResetInletVelocity('inlet', None, U=umax, V=0.0, W=0.0),
        ]

        i_has_ghost = True
        o_has_ghost = False
        i_update_cls = Inlet
        o_update_cls = Outlet
        manager = SimpleInletOutlet

        props_to_copy = [
            'x0', 'y0', 'z0', 'uhat', 'vhat', 'what', 'x', 'y', 'z',
            'u', 'v', 'w', 'm', 'h', 'rho', 'p', 'ioid'
        ]
        props_to_copy += ['uta', 'pta', 'u0', 'v0', 'w0', 'p0']

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[0.0, 0.0, 0.0], equations=inleteqns,
            has_ghost=i_has_ghost, update_cls=i_update_cls, umax=umax
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt, 0.0, 0.0], equations=None,
            has_ghost=o_has_ghost, update_cls=o_update_cls,
            props_to_copy=props_to_copy
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')
        t, cd, cl, cd_sf, cl_sf = self._plot_force_vs_t()
        return t, cd, cl, cd_sf, cl_sf


    def _plot_force_vs_t(self):
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.sph.equation import Group
        from edac import (MomentumEquationPressureGradient,
                          SummationDensityGather, SetWallVelocity)

        data = load(self.output_files[0])
        solid = data['arrays']['solid']
        fluid = data['arrays']['fluid']

        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf']
        for p in prop:
            solid.add_property(p)
            fluid.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.
        equations = [
            Group(
                equations=[
                    SummationDensityGather(dest='solid',
                                           sources=['fluid', 'solid']),
                    SetWallVelocity(dest='solid', sources=['fluid']),
                    ], real=False),
            Group(
                equations=[
                    # Pressure gradient terms
                    MomentumEquationPressureGradient(
                        dest='solid', sources=['fluid']),
                    SolidWallNoSlipBCReverse(
                        dest='solid', sources=['fluid'], nu=self.nu),
                    ], real=True),
        ]
        sph_eval = SPHEvaluator(
            arrays=[solid, fluid], equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cl_sf, cd_sf = [], [], [], [], []
        import gc
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx_max}, Cr: {self.cr:.4f}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        if os.path.exists(self.res):
            results = np.load(self.res)
            t, cl, cd = map(list, [results['t'], results['cl'], results['cd']])
            cl_sf, cd_sf = map(list, [results['cl_sf'], results['cd_sf']])
            latest_time = t[-1]
            at_file = tmp_at_file = len(t)
            guess_file = self.output_files[tmp_at_file]
            guess_time = load(guess_file)['solver_data']['t']
            tdiff = guess_time - latest_time
            if tdiff > 0:
                step = -1
            else:
                step = 1
            for sd, _ in iter_output(self.output_files[tmp_at_file::step]):
                at_file += step
                if abs(t[-1] - sd['t']) < 1e-6:
                    at_file -= 1
                    break

        i = 0
        files_total = len(self.output_files[at_file:])
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays['fluid']
            solid = arrays['solid']
            cx, cy = self.cxy
            radius = (self.dc/2 + np.max(solid.h)*10)
            cond = ((fluid.x - cx)**2 + (fluid.y - cy)**2 < radius**2)
            indices = np.where(~cond)[0]
            fluid.remove_particles(indices)
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            sph_eval.update_particle_arrays([solid, fluid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))
            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd_sf, cl_sf = list(map(np.asarray, (t, cd, cl, cd_sf, cl_sf)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cl_sf=cl_sf, cd_sf=cd_sf)
        # Now plot the results.
        import matplotlib
        # matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_df$')
        plt.plot(t, cl_sf, label=r'$C_lf$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        plt.grid()
        plt.show()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()
        return t, cd, cl, cd_sf, cl_sf

    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')


    def pre_step(self, solver):               
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                if t > 1.0:
                    particle.y[:] = particle.y_0[:] + A * np.sin(omega * (t-1.0))
                    particle.v[:] = A * np.cos(omega*(t-1.0)) * omega 
                    particle.av[:] = -A * np.sin(omega *(t-1.0)) * (omega)**2

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()


if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
