"""
Flow past cylinder
"""
import logging
from time import time
import os
import numpy
import numpy as np
from numpy import pi, cos, sin, exp

from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme, SolidWallNoSlipBCReverse


logger = logging.getLogger()

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0

# Motion parameters
T = 10
A = 1.0 # A = [0.3, 1.5] A/D = ?? 
omega = 2*np.pi/T  # omega=6.283/T = 0.628/3

##-------------------------------------------------------------
alpha = 5.0  # degrees # NACA5515 Huang19 model

dx = 0.1
hdx = 1.2
h0 = dx * hdx

# from pysph.sph.wc.edac import EDACScheme
from pysph.tools.geometry import get_4digit_naca_airfoil, rotate

def solid_airfoil(dx, airfoil, chord, center, angle, Lt):
    """ Generates an airfoil geometry for fluid flow simulation.
    """
    if len(airfoil) == 4:
        # data = np.load('airfoil.npz')
        data = np.load('airfoil_NACA5515_p025.npz')
        x, y = data['xf'], data['yf']
        # x, y = get_4digit_naca_airfoil(dx, airfoil, chord)
        z = np.zeros_like(x)
        x, y, z = G.rotate(x, y, z, angle=-alpha)

        h = dx
        one = np.ones_like(x)
        volume = dx * dx * one
        solid = get_particle_array(name='solid', x=x, y=y, y0=y, ds=dx,
            m=volume * rho, rho=one * rho, h=h * one, V=1.0 / volume)
        solid.add_constant('ds_min', dx)

        solid.x += center[0]
        solid.y += center[1]
        return solid 
##-------------------------------------------------------------


def potential_flow(x, y, u_infty=1.0, center=(0, 0), diameter=1.0):
    x = x - center[0]
    y = y - center[1]
    z = x + 1j*y
    a2 = (diameter * 0.5)**2
    vel = (u_infty - a2/(z*z)).conjugate()
    u = vel.real
    v = vel.imag
    p = 500 - 0.5 * rho * np.abs(vel)**2
    return u, v, p


class ShepardInterpolateCharacteristics(Equation):
    def initialize(self, d_idx, d_J1, d_J2u, d_J3u, d_J2v, d_J3v):
        d_J1[d_idx] = 0.0
        d_J2u[d_idx] = 0.0
        d_J3u[d_idx] = 0.0

        d_J2v[d_idx] = 0.0
        d_J3v[d_idx] = 0.0

    def loop(self, d_idx, d_J1, d_J2u, s_J1, s_J2u, d_J3u, s_J3u, s_idx, s_J2v,
             s_J3v, d_J2v, d_J3v, WIJ):
        d_J1[d_idx] += s_J1[s_idx] * WIJ
        d_J2u[d_idx] += s_J2u[s_idx] * WIJ
        d_J3u[d_idx] += s_J3u[s_idx] * WIJ

        d_J2v[d_idx] += s_J2v[s_idx] * WIJ
        d_J3v[d_idx] += s_J3v[s_idx] * WIJ

    def post_loop(self, d_idx, d_J1, d_J2u, d_wij, d_avg_j2u, d_avg_j1, d_J3u,
                  d_avg_j3u, d_J2v, d_J3v, d_avg_j2v, d_avg_j3v):
        if d_wij[d_idx] > 1e-14:
            d_J1[d_idx] /= d_wij[d_idx]
            d_J2u[d_idx] /= d_wij[d_idx]
            d_J3u[d_idx] /= d_wij[d_idx]

            d_J2v[d_idx] /= d_wij[d_idx]
            d_J3v[d_idx] /= d_wij[d_idx]

        else:
            d_J1[d_idx] = d_avg_j1[0]
            d_J2u[d_idx] = d_avg_j2u[0]
            d_J3u[d_idx] = d_avg_j3u[0]

            d_J2v[d_idx] = d_avg_j2v[0]
            d_J3v[d_idx] = d_avg_j3v[0]

    def reduce(self, dst, t, dt):
        dst.avg_j1[0] = numpy.average(dst.J1[dst.wij > 0.0001])
        dst.avg_j2u[0] = numpy.average(dst.J2u[dst.wij > 0.0001])
        dst.avg_j3u[0] = numpy.average(dst.J3u[dst.wij > 0.0001])

        dst.avg_j2v[0] = numpy.average(dst.J2v[dst.wij > 0.0001])
        dst.avg_j3v[0] = numpy.average(dst.J3v[dst.wij > 0.0001])


class EvaluateCharacterisctics(Equation):
    def __init__(self, dest, sources, c_ref, rho_ref, u_ref, p_ref, v_ref):
        self.c_ref = c_ref
        self.rho_ref = rho_ref
        self.p_ref = p_ref
        self.u_ref = u_ref
        self.v_ref = v_ref
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_u, d_v, d_p, d_rho, d_J1, d_J2u, d_J3u, d_J2v,
                   d_J3v):
        a = self.c_ref
        rho_ref = self.rho_ref

        rho = d_rho[d_idx]
        pdiff = d_p[d_idx] - self.p_ref
        udiff = d_u[d_idx] - self.u_ref
        vdiff = d_v[d_idx] - self.v_ref

        d_J1[d_idx] = -a * a * (rho - rho_ref) + pdiff
        d_J2u[d_idx] =  rho * a * udiff + pdiff
        d_J3u[d_idx] = -rho * a * udiff + pdiff

        d_J2v[d_idx] =  rho * a * vdiff + pdiff
        d_J3v[d_idx] = -rho * a * vdiff + pdiff


class EvaluatePropertyfromCharacteristics(Equation):
    def __init__(self, dest, sources, c_ref, rho_ref, u_ref, v_ref, p_ref):
        self.c_ref = c_ref
        self.rho_ref = rho_ref
        self.p_ref = p_ref
        self.u_ref = u_ref
        self.v_ref = v_ref             
        super().__init__(dest, sources)


    def initialize(self, d_idx, d_p, d_J1, d_J2u, d_J2v, d_J3u, d_J3v, d_rho,
                   d_u, d_v, d_xn, d_yn):
        a = self.c_ref
        a2_1 = 1.0/(a*a)
        rho = d_rho[d_idx]
        xn = d_xn[d_idx]
        yn = d_yn[d_idx]

        # Characteristic in the downstream direction.
        if xn > 0.5 or yn > 0.5:
            J1 = d_J1[d_idx]
            J3 = 0.0
            if xn > 0.5:
                J2 = d_J2u[d_idx]
                d_u[d_idx] = self.u_ref + (J2 - J3) / (2 * rho * a)
            else:
                J2 = d_J2v[d_idx]
                d_v[d_idx] = self.v_ref + (J2 - J3) / (2 * rho * a)
        # Characteristic in the upstream direction.
        else:
            J1 = 0.0
            J2 = 0.0
            if xn < -0.5:
                J3 = d_J3u[d_idx]
                d_u[d_idx] = self.u_ref + (J2 - J3) / (2 * rho * a)
            else:
                J3 = d_J3v[d_idx]
                d_v[d_idx] = self.v_ref + (J2 - J3) / (2 * rho * a)

        d_rho[d_idx] = self.rho_ref + a2_1 * (-J1 + 0.5 * (J2 + J3))
        d_p[d_idx] = self.p_ref + 0.5 * (J2 + J3)


class ResetInletVelocity(Equation):
    def __init__(self, dest, sources, U, V, W):
        self.U = U
        self.V = V
        self.W = W

        super().__init__(dest, sources)

    def loop(self, d_idx, d_u, d_v, d_w, d_uref):
        if d_idx == 0:
            d_uref[0] = self.U
        d_u[d_idx] = self.U
        d_v[d_idx] = self.V
        d_w[d_idx] = self.W


class WindTunnel(Application):
    def initialize(self):
        # Geometric parameters
        self.Lt = 12.0  # length of tunnel
        self.Wt = 6.0  # half width of tunnel
        self.dc = 1.2  # diameter of cylinder
        self.nl = 10  # Number of layers for wall/inlet/outlet
        self.sol_adapt = 0.0
        self._nnps = None

        # Remove inner layers of the cylinder. This will save a lot of space if
        # dx_min is very low.
        self.remove_inner_layers = True

        # # self.chord = 2.0
        # self.chord = self.dc * 2   
        # self.center = (self.Lt/3.0, 0.0)
        

    def add_user_options(self, group):
        group.add_argument(
            "--re", action="store", type=float, dest="re", default=100,
            help="Reynolds number."
        )
        group.add_argument(
            "--hdx", action="store", type=float, dest="hdx", default=1.2,
            help="Ratio h/dx."
        )
        group.add_argument(
            "--dx-min", action="store", type=float, dest="dx_min",
            default=0.025,
            help="Minimum resolution."
        )
        group.add_argument(
            "--dx-max", action="store", type=float, dest="dx_max",
            default=0.5,
            help="Maximum resolution."
        )
        cr = pow(2.0, 1.0/4)
        group.add_argument(
            "--cr", action="store", type=float, dest="cr",
            default=cr,
            help="Resolution step ratio."
        )
        group.add_argument(
            "--lt", action="store", type=float, dest="Lt", default=12,
            help="Length of the WindTunnel."
        )
        group.add_argument(
            "--wt", action="store", type=float, dest="Wt", default=6,
            help="Half width of the WindTunnel."
        )
        group.add_argument(
            "--dc", action="store", type=float, dest="dc", default=1.0,
            help="Diameter of the cylinder."
        )
        add_bool_argument(
            group, 'potential', dest='potential', default=False,
            help='Initialize with potential flow.'
        )
        add_bool_argument(
            group, 'vacondio', dest='vacondio', default=True,
            help="Use Vacondio et al. approach for splitting."
        )
        add_bool_argument(
            group, 'hybrid', dest='hybrid', default=True,
            help="Use iterative merging."
        )
        group.add_argument(
            '--solution-adapt', action="store", type=float,
            dest='sol_adapt', default=self.sol_adapt,
            help=("Automatically adapt to the solution. "
                  "Argument is the cutoff fraction.")
        )
        group.add_argument(
            "--cfl-factor", action="store", type=float, dest="cfl_factor",
            default=0.25,
            help="CFL number, useful when using different Integrator."
        )
        add_bool_argument(
            group, 'wake', dest='wake', default=False,
            help="Add wake region."
        )

    def consume_user_options(self):
        if self.options.n_damp is None:
            self.options.n_damp = 20
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        self.sol_adapt = self.options.sol_adapt
        dx_min = self.options.dx_min
        dx_max = self.options.dx_max
        cr = self.options.cr
        self.dx_min = dx_min
        self.dx_max = dx_max
        self.cr = cr
        re = self.options.re

        self.nu = nu = umax * self.dc / re
        self.cxy = self.Lt / 3.0, 0.0

        self.hdx = hdx = self.options.hdx

        self.h = h = hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        dt_cfl = cfl * h / (c0 + umax)
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        self.tf = 6.0
        self.vacondio = self.options.vacondio
        self.hybrid = self.options.hybrid
        self.wake = self.options.wake

        # self.chord = 2.0
        self.chord = self.dc * 2   
        self.center = self.cxy # (self.Lt/3.0, 0.0)
        # self.cxy = self.Lt / 3.0, 0.0

    # def _create_solid(self):
    #     dx = self.dx_min
    #     h0 = self.hdx*dx

    #     r = np.arange(dx/2, self.dc/2, dx)
    #     x, y = np.array([]), np.array([])
    #     for i in r:
    #         spacing = dx
    #         theta = np.linspace(0, 2*pi, int(2*pi*i/spacing), endpoint=False)
    #         x = np.append(x,  i * cos(theta))
    #         y = np.append(y,  i * sin(theta))

    #     x += self.cxy[0]
    #     volume = dx*dx
    #     solid = get_particle_array(
    #         name='solid', x=x, y=y, m=volume*rho, rho=rho, h=h0
    #     )
    #     solid.add_constant('ds_min', dx)
    #     return solid

    def _create_pseudo_wake(self):
        dx = self.dx_min
        dc = self.dc
        x0, y0 = self.cxy
        l = 1.2*dc
        w = 1.5*dc
        y0 -= 0.5*w
        h0 = self.hdx*dx
        x, y = np.mgrid[x0:x0+l:dx, y0:y0+w:dx]
        volume = dx*dx
        wake = get_particle_array(
            name='wake', x=x, y=y, m=volume*rho, rho=rho, h=h0
        )
        wake.add_constant('ds_min', dx)
        return wake

    def _create_box(self):
        dx = self.dx_max
        m = rho * dx * dx
        h0 = self.hdx*dx
        layers = self.nl * dx
        w = self.Wt
        l = self.Lt
        x, y = np.mgrid[-layers:l+layers:dx, -w-layers:w+layers:dx]

        fluid = (x < l) & (x > 0) & (y < w) & (y > -w)
        xf, yf = x[fluid], y[fluid]
        x, y = x[~fluid], y[~fluid]
        fluid = get_particle_array(
            name='fluid', x=xf, y=yf, m=m, h=h0, u=u_freestream, rho=rho,
            p=0.0, vmag=0.0
        )

        wall = (y > w - dx/2) | (y < -w + dx/2)
        xw, yw = x[wall], y[wall]
        x, y = x[~wall], y[~wall]
        wall = get_particle_array(
            name='wall', x=xw, y=yw, m=m, h=h0, rho=rho
        )

        inlet = (x < dx/2)
        xi, yi = x[inlet], y[inlet]
        x, y = x[~inlet], y[~inlet]
        inlet = get_particle_array(
            name='inlet', x=xi, y=yi, m=m, h=h0, u=u_freestream, rho=rho,
            p=0.0, uhat=u_freestream
        )

        outlet = (x > l - dx/2)
        xo, yo = x[outlet], y[outlet]
        # Use uhat=umax. So that the particles are moving out, if 0.0 is used
        # instead, the outlet particles will not move.
        outlet = get_particle_array(
            name='outlet', x=xo, y=yo, m=m, h=h0, u=u_freestream, rho=rho,
            p=0.0, uhat=u_freestream, vhat=0.0
        )
        return fluid, wall, inlet, outlet

    def _set_wall_normal(self, pa):
        props = ['xn', 'yn', 'zn']
        for p in props:
            pa.add_property(p)

        y = pa.y
        cond = y > 0.0
        pa.yn[cond] = 1.0
        cond = y < 0.0
        pa.yn[cond] = -1.0

    def create_nnps(self):
        if self._nnps is None:
            from pysph.base.nnps import OctreeNNPS as NNPS
            self._nnps = NNPS(dim=2.0, particles=self.particles,
                              radius_scale=3.0, cache=True)
        return self._nnps

    def create_particles(self):
        fluid, wall, inlet, outlet = self._create_box()
        # solid = self._create_solid()
        solid = solid_airfoil(dx=dx, airfoil='5515', chord=self.chord, 
                              center=self.cxy, angle=5.0, Lt = self.Lt)

        ghost_inlet = None
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
        self.wake = wake

        particles = [fluid, inlet, outlet, solid, wall]
        boundary = [solid]
        self.shift_sources = list(particles)

        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        if wake is not None:
            particles.append(wake)
            boundary.append(wake)
        self.scheme.setup_properties(particles)

        setup_properties([fluid, inlet, outlet])
        fluid.add_property('vmag')

        self._set_wall_normal(wall)

        bg_freq = 10 if self.sol_adapt > 0 else 500

        self._update_bg = UpdateBackground(
            fluid, dim=2, boundary=boundary, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=5,
            bg_freq=bg_freq, sol_adapt=self.sol_adapt
        )
        sep = '-'*70
        msg = 'Generating background (BG) particles:'
        print(sep, '\n', msg)
        start = time()
        self._update_bg.set_initial_bg()
        msg = f'time took to generate the BG particles, {time()-start:.4f}s'
        print(msg, '\n', sep)
        logger.info('BG initialize:\n%s\n  %s\n%s', sep, msg, sep)
        self._update_bg.initialize_fluid(solids=[solid], vacondio=self.vacondio,
                                         hybrid=self.hybrid)
        bg_pa = self._update_bg.bg_pa
        particles.append(bg_pa)

        if self.options.potential:
            u, v, p = potential_flow(inlet.x, inlet.y, u_freestream, self.cxy, self.dc)
            inlet.u[:] = u
            inlet.v[:] = v
            inlet.p[:] = p
            u, v, p = potential_flow(fluid.x, fluid.y, u_freestream, self.cxy, self.dc)
            fluid.u[:] = u
            fluid.v[:] = v
            fluid.p[:] = p
            fluid.uhat[:] = u
            fluid.vhat[:] = v
            fluid.vmag[:] = np.sqrt(u**2 + v**2)
        outlet.add_property('xn')
        outlet.add_property('yn')
        inlet.add_property('xn')
        inlet.add_property('yn')
        outlet.xn[:] = 1.0
        outlet.yn[:] = 0.0
        inlet.xn[:] = -1.0
        inlet.yn[:] = 0.0
        DEFAULT_PROPS = [
            'xn', 'yn', 'zn', 'J2v', 'J3v', 'J2u', 'J3u', 'J1', 'wij2', 'disp',
            'ioid'
        ]
        for prop in DEFAULT_PROPS:
            if prop not in wall.properties:
                wall.add_property(prop)
        wall.add_constant('uref', 0.0)
        consts = [
            'avg_j2u', 'avg_j3u', 'avg_j2v', 'avg_j3v', 'avg_j1', 'uref'
        ]
        for const in consts:
            if const not in wall.constants:
                wall.add_constant(const, 0.0)
        return particles

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def create_equations(self):
        from pysph.sph.equation import Group
        from edac import EvaluateNumberDensity
        from pysph.sph.bc.inlet_outlet_manager import (
            UpdateNormalsAndDisplacements
        )

        equations = self.scheme.get_equations()
        eq = []
        eq.append(
            Group(equations=[
                EvaluateCharacterisctics(
                    dest='fluid', sources=None, c_ref=c0, rho_ref=rho,
                    u_ref=u_freestream, v_ref=0.0, p_ref=0.0
                )
            ])
        )
        eq.append(
            Group(equations=[
                UpdateNormalsAndDisplacements(
                    'inlet', None, xn=-1, yn=0, zn=0, xo=0, yo=0, zo=0
                ),
                UpdateNormalsAndDisplacements(
                    'outlet', None, xn=1, yn=0, zn=0, xo=0, yo=0, zo=0
                ),
                EvaluateNumberDensity(dest='inlet', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='inlet', sources=['fluid']),
                EvaluateNumberDensity(dest='wall', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='wall', sources=['fluid']),
                EvaluateNumberDensity(dest='outlet', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='outlet', sources=['fluid']),
            ])
        )
        eq.append(Group(equations=[
            EvaluatePropertyfromCharacteristics(
                dest='wall', sources=None, c_ref=c0, rho_ref=rho,
                u_ref=u_freestream, v_ref=0.0, p_ref=0.0
            ),
            EvaluatePropertyfromCharacteristics(
                dest='inlet', sources=None, c_ref=c0, rho_ref=rho,
                u_ref=u_freestream, v_ref=0.0, p_ref=0.0
            ),
            EvaluatePropertyfromCharacteristics(
                dest='outlet', sources=None, c_ref=c0, rho_ref=rho,
                u_ref=u_freestream, v_ref=0.0, p_ref=0.0
            )])
        )
        # Remove solid wall bc in the walls.
        # wall_solid_bc = equations[2].equations.pop()
        equations = eq + equations
        # Remove Compute average pressure on inlet and outlet.
        return equations

    def create_tools(self):
        af = self._update_bg.adapt_fluid
        af._orig_nnps = self.create_nnps()
        # FIXME: Awful Hack!
        af.smoother = af.setup_smoothing(
            af.name, self.shift_sources, af._orig_nnps, af.dim,
            rho0=af.rho0, iters=3
        )
        arrays = list(self.particles)
        if self.wake is not None:
            arrays.remove(self.wake)
        return [self._update_bg, self._update_bg.adapt_fluid]

    def configure_scheme(self):
        scheme = self.scheme
        self.iom = self._create_inlet_outlet_manager()
        scheme.inlet_outlet_manager = self.iom
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        self.iom.update_dx(self.dx_max)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0,
            output_at_times=list(range(1, 7))
        )

    def _get_io_info(self):
        from pysph.sph.bc.hybrid.outlet import Outlet
        from hybrid_simple_inlet_outlet import Inlet, SimpleInletOutlet

        i_has_ghost = False
        o_has_ghost = False
        i_update_cls = Inlet
        o_update_cls = Outlet
        manager = SimpleInletOutlet

        props_to_copy = [
            'x0', 'y0', 'z0', 'uhat', 'vhat', 'what', 'x', 'y', 'z',
            'u', 'v', 'w', 'm', 'h', 'rho', 'p', 'ioid'
        ]
        props_to_copy += ['u0', 'v0', 'w0', 'p0']

        inlet_info = InletInfo(
            pa_name='inlet', normal=[-1.0, 0.0, 0.0],
            refpoint=[self.dx_max/2, 0.0, 0.0], equations=None,
            has_ghost=i_has_ghost, update_cls=i_update_cls,
            umax=u_freestream
        )

        outlet_info = OutletInfo(
            pa_name='outlet', normal=[1.0, 0.0, 0.0],
            refpoint=[self.Lt - self.dx_max/2, 0.0, 0.0], equations=None,
            has_ghost=o_has_ghost, update_cls=o_update_cls,
            props_to_copy=props_to_copy
        )

        return inlet_info, outlet_info, manager

    def _create_inlet_outlet_manager(self):
        inlet_info, outlet_info, manager = self._get_io_info()
        iom = manager(
            fluid_arrays=['fluid'], inletinfo=[inlet_info],
            outletinfo=[outlet_info]
        )
        return iom

    def create_inlet_outlet(self, particle_arrays):
        iom = self.iom
        io = iom.get_inlet_outlet(particle_arrays)
        return io

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')

        t, cd, cl, cd_sf, cl_sf, cp = self._compute_force_vs_t(['fluid'], ['solid'])
        self._plot_coeffs(t, cd, cl, cd_sf, cl_sf, cp)
        return t, cd, cl, cd_sf, cl_sf, cp

    def _compute_force_vs_t(self, fluids, solids):
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.sph.equation import Group
        from edac import (
            MomentumEquationPressureGradient2, SummationDensityGather,
            SetWallVelocity, EvaluateNumberDensity
        )
        from edac import ComputeBeta

        data = load(self.output_files[0])
        parrays = []
        for name in fluids + solids:
            parrays.append(data['arrays'][name])
        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf']
        for p in prop:
            for parray in parrays:
                if p not in parray.properties:
                    parray.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.
        all_sources = fluids + solids
        equations = []
        g1 = []
        for sname in solids:
            g1.extend([
                SummationDensityGather(dest=sname, sources=all_sources),
                ComputeBeta(dest=sname, sources=all_sources, dim=2),
                EvaluateNumberDensity(dest=sname, sources=fluids),
                SetWallVelocity(dest=sname, sources=fluids),
            ])
        equations.append(Group(equations=g1, real=False))

        g2 = []
        nu = self.nu
        for sname in solids:
            g2.extend([
                MomentumEquationPressureGradient2(dest=sname, sources=fluids),
                SolidWallNoSlipBCReverse(dest=sname, sources=fluids, nu=nu),
            ])
        equations.append(Group(equations=g2, real=True))

        sph_eval = SPHEvaluator(
            arrays=parrays, equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cl_sf, cd_sf, cp = [], [], [], [], [], []
        import gc
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx_max}, Cr: {self.cr:.4f}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        if os.path.exists(self.res):
            results = np.load(self.res)
            t, cl, cd = map(list, [results['t'], results['cl'], results['cd']])
            cl_sf, cd_sf, cp = map(list, [results['cl_sf'], results['cd_sf'], results['cp']])
            latest_time = t[-1]
            latest_file = len(t)
            guess_file = self.output_files[latest_file]
            guess_time = load(guess_file)['solver_data']['t']
            tdiff = guess_time - latest_time
            if tdiff > 0:
                step = -1
            else:
                step = 1
            for i, file in enumerate(self.output_files[latest_file::step]):
                data = load(file)
                sd  = data['solver_data']
                total_files = len(self.output_files[latest_file::])
                print(f"Searching for latest file, {i}/{total_files}", end='\r')
                if abs(t[-1] - sd['t']) < 1e-6:
                    at_file = self.output_files.index(file)
                    break

        files_total = len(self.output_files[at_file:])
        if files_total == 0:
            data = np.load(self.res)
            return data['t'], data['cd'], data['cl'], data['cd_sf'], data['cl_sf'], data['cp']

        i = 0
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays['fluid']
            solid = arrays['solid']
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            sph_eval.update_particle_arrays([fluid, solid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            ps = solid.p - self.p_ref
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))
            cp.append(np.sum(fx)/(rho * umax**2 * np.pi))
            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}, Cp: {Cp[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd_sf, cl_sf, cp = list(map(np.asarray, (t, cd, cl, cd_sf, cl_sf, cp)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cl_sf=cl_sf, cd_sf=cd_sf, cp=cp)
        return t, cd, cl, cd_sf, cl_sf, cp

    def _plot_coeffs(self, t, cd, cl, cd_sf, cl_sf, cp):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_{df}$')
        plt.plot(t, cl_sf, label=r'$C_{lf}$')
        plt.plot(t, cp, label=r'$C_p$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()


if __name__ == '__main__':
    app = WindTunnel()
    app.run()
    app.post_process(app.info_filename)
