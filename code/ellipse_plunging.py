"""
Flow past Ellipse.
"""
import os
import numpy as np
from numpy import pi, sin, cos

from pysph.base.utils import get_particle_array
from moving_square import MovingSquare


# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


def rotate(x, y, axis, deg):
    rad = deg * np.pi / 180
    R0 = np.cos(rad)
    R1 = -np.sin(rad)
    R2 = np.sin(rad)
    R3 = np.cos(rad)
    xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
    ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
    return xnew + axis[0], ynew + axis[1]


class EllipseMotion(MovingSquare):
    def initialize(self):
        super().initialize()
        self.diameter = 1 * self.dc
        self.remove_inner_layers = False
        self.chord = 1.0
        self.sol_adapt = 0.075
        self.omega = 2 * np.pi 

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument(
            '--a', action="store", type=float, dest="a",
            default=1.0,
            help="The major length of the ellipse."
        )
        group.add_argument(
            '--b', action="store", type=float, dest="b",
            default=0.5,
            help="The minor length of the ellipse."
        )
        group.set_defaults(
            potential=False, sol_adapt=False, dc=1, cr=1.12, pf=250, re=550, 
            Lt=30, Wt=15, dx_min=0.01, dx_max=0.15, tf=5,  
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.cxy = self.Lt/2, 0.0
        self.a = 2 * self.chord
        self.b = 2.5 * self.a

        # 1.5 is a small factor taking into account the
        # diameter of the ellipse.
        umax = self.omega * self.b * 1.5
        self.c0 = 10 * umax
        dt_cfl = self.cfl * self.h / (self.c0 + umax)
        self.dt = min(dt_cfl, self.dt)

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        data = np.load('ellipse_dxmin_01.npz')
        x, y = data['xf'], data['yf']

        x += self.cxy[0]
        y += self.cxy[1] - self.b
        
        self.xcm = self.cxy[0]
        self.ycm = self.cxy[1] - self.b
        self.ucm = 0.0
        self.vcm = 0.0

        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, m=volume*rho, rho=rho, h=h0, xi=x, yi=y,
        )
        solid.add_constant('ds_min', dx)

        import matplotlib.pyplot as plt
        plt.scatter(solid.x, solid.y, s=2)
        plt.scatter(self.xcm, self.ycm, s=8, c='r')
        plt.scatter(self.cxy[0], self.cxy[1], s=8, marker='x', c='k')
        plt.axis('equal')
        plt.savefig(os.path.join(self.output_dir, 'geom.png'))
        # plt.show()
        return solid

    def rotate(self, x, y, axis, rad):
        R0 = np.cos(rad)
        R1 = -np.sin(rad)
        R2 = np.sin(rad)
        R3 = np.cos(rad)
        xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
        ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
        xr, yr = xnew + axis[0], ynew + axis[1]
        return xr, yr

    def motion_flapping(self, t):
        omega = self.omega / 10
        height = self.a - self.a * np.cos(omega * t) 
        length = self.b * np.sin(omega * t)
        u = omega * self.b * cos(omega * t)
        v = omega * self.a * sin(omega * t)
        au = -omega * omega * self.b * sin(omega * t)
        av = omega * omega * self.a * cos(omega * t)
        return height, length, u, v, au, av


    # Motion Three: Flapping motion, MAV type (elliptic-plunging)
    def pre_step(self, solver):
        t = solver.t
        omega = self.omega / 20
        for particle in self.particles:
            if particle.name == 'solid':
                # Translation - PurePlunging (Vert + Horizontal Motion)
                height, length, u, v, au, av = self.motion_flapping(t)
                particle.x[:] = particle.xi + length
                particle.y[:] = particle.yi + height
                particle.u[:] = u 
                particle.v[:] = v 
                particle.au[:] = au
                particle.av[:] = av


    # Motion Two: Rotating motion - vawt type
    def pre_step3(self, solver):     
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                omega = self.omega / 20
                # Rotation #- pitch
                axis = [self.cxy[0], self.cxy[1]]
                theta = omega * t
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                particle.u[:] = -omega * (
                    sin(omega * t) * (particle.x - axis[0]) +
                    cos(omega * t) * (particle.y - axis[1])
                )
                particle.v[:] =  omega * (
                    cos(omega * t) * (particle.x - axis[0]) -
                    sin(omega * t) * (particle.y - axis[1])
                )
                particle.au[:] = -omega**2 * (
                    cos(omega * t) * (particle.x - axis[0]) -
                    sin(omega * t) * (particle.y - axis[1])
                )
                particle.av[:] = -omega**2 * (
                    sin(omega * t) * (particle.x - axis[0]) +
                    cos(omega * t) * (particle.y - axis[1])
                )




if __name__ == '__main__':
    app = EllipseMotion()
    app.run()
    app.post_process(app.info_filename)
