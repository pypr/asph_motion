"""
Flow past two cylinders.
"""
import os
import numpy as np
from numpy import pi, cos, sin, exp
from adapt import setup_properties
from fpc_auto import WindTunnel
from edac import AdaptiveEDACScheme


from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme, SolidWallNoSlipBCReverse


# Motion parameters
# T = 10
# fe = 0.2475 # fe = [0.12375, 0.165, 0.20625, 0.2475]
# T = 1.0/fe
# omega = 2*np.pi/T  # omega=6.283 / T = 0.628
# A = 1.25 # A = [0.25, 1.25] 
# omega = [0.7775, 1.0367, 1.5551] # = 2*np.pi/T

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0

# Motion parameters
T = 100
A = 1.0 # A = [0.3, 1.5] A/D = ?? 
omega = 2*np.pi/T  # omega=6.283/T = 0.628/3

##-------------------------------------------------------------


def compute_forces(fname, sname, nu):
    from edac import (
        MomentumEquationPressureGradient2, SummationDensityGather,
        SetWallVelocity, EvaluateNumberDensity, ComputeBeta
    )

    all_sources = [fname, sname]
    equations = []
    g1 = []
    g1.extend([
        SummationDensityGather(dest=sname, sources=all_sources),
        ComputeBeta(dest=sname, sources=all_sources, dim=2),
        EvaluateNumberDensity(dest=sname, sources=[fname]),
        SetWallVelocity(dest=sname, sources=[fname]),
    ])
    equations.append(Group(equations=g1, real=False))

    g2 = []
    g2.extend([
        MomentumEquationPressureGradient2(dest=sname, sources=[fname]),
        SolidWallNoSlipBCReverse(dest=sname, sources=[fname], nu=nu),
    ])
    equations.append(Group(equations=g2, real=True))
    return equations

class FPCBao(WindTunnel):
    def initialize(self):
        super().initialize()
        self.remove_inner_layers = False
        self.sol_adapt = 0.05

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument(
            '--gap', action="store", type=float, dest="gap",
            default=1.5,
            help="Spacing gap between cylinders (divided to cylinder dia)."
        )
        group.add_argument(
            '--omega', action="store", type=float, dest="omega",
            default=1.0367,
            help="Angular velocity of oscillation (2*pi*fe)."
        )
        group.add_argument(
            '--amp', action="store", type=float, dest="amp",
            default=0.5,
            help="Amplitude of cylinder oscillation."
        )
        group.set_defaults(
            potential=False, dc=1.0, re=100, dx_max=0.4, dx_min=0.01,
            Lt=60, Wt=25, cr=1.12, tf=30
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.gap = self.options.gap * self.dc
        self.amp = self.options.amp
        self.omega = self.options.omega  
        self.cxy = (self.Lt/3.0, 0)

        umax = np.sqrt((self.amp * self.omega)**2 + u_freestream*2)
        dt_cfl = self.cfl * self.h / (c0 + umax)
        self.dt = min(self.dt, dt_cfl)


    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def create_particles(self):
        fluid, wall, inlet, outlet = self._create_box()
        solid = self._create_solid()

        particles = [fluid, inlet, outlet, solid, wall]
        bg_sources = [solid]

        # Do not add wake, if present, to shift_sources.
        self.shift_sources = list(particles)

        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
            particles.append(wake)
            bg_sources.append(wake)
        self.wake = wake

        # Do not use clean=True here. The properties not used in EDAC equations
        # but used in the create_equations or adaptive will be erased.
        self.scheme.setup_properties(particles, clean=False)

        setup_properties([fluid, inlet, outlet])
        fluid.add_property('vmag')

        bg_freq = 10
        bg_pa = self._create_bg(fluid, bg_sources, bg_freq)
        particles.append(bg_pa)

        solid.add_property('y_0')
        solid.y_0[:] = solid.y
        solid.add_property('x_0')
        solid.x_0[:] = solid.x

        return particles


    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')

        t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t('fluid', 'solid')
        self._plot_coeffs(t, cd, cl, cd_sf, cl_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _compute_force_vs_t(self, fluid_name, solid_name):
        import gc
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator

        data = load(self.output_files[0])
        parrays = []
        for name in [fluid_name, solid_name]:
            parrays.append(data['arrays'][name])
        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf']
        for p in prop:
            for parray in parrays:
                if p not in parray.properties:
                    parray.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.

        equations = compute_forces(fluid_name, solid_name, nu=self.nu)
        sph_eval = SPHEvaluator(
            arrays=parrays, equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cl_sf, cd_sf = [], [], [], [], []
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx_max}, Cr: {self.cr:.4f}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        if os.path.exists(self.res):
            results = np.load(self.res)
            t, cl, cd = map(list, [results['t'], results['cl'], results['cd']])
            cl_sf, cd_sf = map(list, [results['cl_sf'], results['cd_sf']])
            latest_time = t[-1]
            latest_file = len(t)
            guess_file = self.output_files[latest_file]
            guess_time = load(guess_file)['solver_data']['t']
            tdiff = guess_time - latest_time
            if tdiff > 0:
                step = -1
            else:
                step = 1
            for i, file in enumerate(self.output_files[latest_file::step]):
                data = load(file)
                sd  = data['solver_data']
                total_files = len(self.output_files[latest_file::])
                print(f"Searching for latest file, {i}/{total_files}", end='\r')
                if abs(t[-1] - sd['t']) < 1e-6:
                    at_file = self.output_files.index(file)
                    break

        files_total = len(self.output_files[at_file:])
        if files_total == 1:
            data = np.load(self.res)
            return data['t'], data['cd'], data['cl'], data['cd_sf'], data['cl_sf']

        i = 0
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays[fluid_name]
            solid = arrays[solid_name]
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            sph_eval.update_particle_arrays([fluid, solid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))
            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd_sf, cl_sf = list(map(np.asarray, (t, cd, cl, cd_sf, cl_sf)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cl_sf=cl_sf, cd_sf=cd_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _plot_coeffs(self, t, cd, cl, cd_sf, cl_sf):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_{df}$')
        plt.plot(t, cl_sf, label=r'$C_{lf}$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd-cl')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def pre_step(self, solver):               
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                particle.y[:] = particle.y_0[:] + self.amp * np.sin(self.omega * t)
                # particle.x[:] = particle.x_0[:] + self.amp * np.sin(self.omega * t)
                particle.v[:] = self.amp * np.cos(self.omega*t) * self.omega 
                particle.av[:] = -self.amp * np.sin(self.omega *t) * (self.omega)**2


    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 3.0
        ''')


if __name__ == '__main__':
    app = FPCBao()
    app.run()
    app.post_process(app.info_filename)
