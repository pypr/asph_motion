"""
Flow past cylinder
"""
import logging
from time import time
import numpy
import numpy as np
from numpy import pi, cos, sin, exp
from pysph.tools import geometry as G
from pysph.base.utils import get_particle_array
from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme
from fpc_auto import WindTunnel

logger = logging.getLogger()


# Motion parameters
T = 30
A = 0.5
omega = 2*np.pi/20

# Fluid mechanical/numerical parameters
rho = 1
u_square = 1.0
umax = 2 * u_square
c0 = 10 * umax
p0 = rho * c0 * c0

### ------------------------------------------------------
def calc_velocity(t):
    a, xc, k = 1.00093, 0.4701, 8.39404
    ui = a*exp(-exp(-k*(t-xc)))
    ai = a*434.224*exp(-k*t - 51.73*exp(-k*t))
    return ui, ai

from pysph.tools.geometry import get_4digit_naca_airfoil, rotate

### one airfoil
alpha = 10.0   # degrees
def solid_airfoil(dx, airfoil, chord, center, angle, Lt):
    """ Generates an airfoil geometry for fluid flow simulation.
    """
    if len(airfoil) == 4:
        data = np.load('airfoil5515.npz')
        # data = np.load('airfoil.npz')
        x, y = data['xf'], data['yf']
        # x, y = get_4digit_naca_airfoil(dx, airfoil, chord)
        z = np.zeros_like(x)
        x, y, z = G.rotate(x, y, z, angle=-alpha)
        # plt.scatter(x,y)
        # plt.show()
        h = dx
        one = np.ones_like(x)
        volume = dx * dx * one
        solid = get_particle_array(name='solid', x=x, y=y, y0=y, 
            m=volume * rho, rho=one * rho, h=h * one, V=1.0 / volume)
        solid.add_constant('ds_min', dx)

        solid.x += center[0]
        solid.y += center[1]
        return solid 
### ------------------------------------------------------


def check_time_outlet_bc(t, dt):
    if t < 6:
        return True
    return False


def check_time_solid_bc(t, dt):
    if t >= 6:
        return True
    return False


class ShepardInterpolateCharacteristics(Equation):
    def initialize(self, d_idx, d_J1, d_J2u, d_J3u, d_J2v, d_J3v):
        d_J1[d_idx] = 0.0
        d_J2u[d_idx] = 0.0
        d_J3u[d_idx] = 0.0

        d_J2v[d_idx] = 0.0
        d_J3v[d_idx] = 0.0

    def loop(self, d_idx, d_J1, d_J2u, s_J1, s_J2u, d_J3u, s_J3u, s_idx, s_J2v,
             s_J3v, d_J2v, d_J3v, WIJ):
        d_J1[d_idx] += s_J1[s_idx] * WIJ
        d_J2u[d_idx] += s_J2u[s_idx] * WIJ
        d_J3u[d_idx] += s_J3u[s_idx] * WIJ

        d_J2v[d_idx] += s_J2v[s_idx] * WIJ
        d_J3v[d_idx] += s_J3v[s_idx] * WIJ

    def post_loop(self, d_idx, d_J1, d_J2u, d_wij, d_avg_j2u, d_avg_j1, d_J3u,
                  d_avg_j3u, d_J2v, d_J3v, d_avg_j2v, d_avg_j3v):
        if d_wij[d_idx] > 1e-14:
            d_J1[d_idx] /= d_wij[d_idx]
            d_J2u[d_idx] /= d_wij[d_idx]
            d_J3u[d_idx] /= d_wij[d_idx]

            d_J2v[d_idx] /= d_wij[d_idx]
            d_J3v[d_idx] /= d_wij[d_idx]
        else:
            d_J1[d_idx] = d_avg_j1[0]
            d_J2u[d_idx] = d_avg_j2u[0]
            d_J3u[d_idx] = d_avg_j3u[0]

            d_J2v[d_idx] = d_avg_j2v[0]
            d_J3v[d_idx] = d_avg_j3v[0]

    def reduce(self, dst, t, dt):
        dst.avg_j1[0] = numpy.average(dst.J1[dst.wij > 0.0001])
        dst.avg_j2u[0] = numpy.average(dst.J2u[dst.wij > 0.0001])
        dst.avg_j3u[0] = numpy.average(dst.J3u[dst.wij > 0.0001])

        dst.avg_j2v[0] = numpy.average(dst.J2v[dst.wij > 0.0001])
        dst.avg_j3v[0] = numpy.average(dst.J3v[dst.wij > 0.0001])


class EvaluateCharacterisctics(Equation):
    def __init__(self, dest, sources, cref, rho_ref, uref, pref, vref):
        self.cref = cref
        self.rho_ref = rho_ref
        self.pref = pref
        self.uref = uref
        self.vref = vref
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_u, d_v, d_p, d_rho, d_J1, d_J2u, d_J3u, d_J2v,
                   d_J3v):
        a = self.cref
        rhoref = self.rho_ref
        uref = self.uref
        pref = self.pref

        d_J1[d_idx] = -a**2 * (d_rho[d_idx] - rhoref) + (d_p[d_idx] - pref)
        d_J2u[d_idx] = a*d_rho[d_idx]*(d_u[d_idx] - uref) + (d_p[d_idx] - pref)
        d_J3u[d_idx] = -a*d_rho[d_idx]*(d_u[d_idx] - uref) + (d_p[d_idx] - pref)

        d_J2v[d_idx] = a*d_rho[d_idx]*(d_v[d_idx] - uref) + (d_p[d_idx] - pref)
        d_J3v[d_idx] = -a*d_rho[d_idx]*(d_v[d_idx] - uref) + (d_p[d_idx] - pref)


class EvaluatePropertyfromCharacteristics(Equation):
    def __init__(self, dest, sources, cref, rho_ref, uref, vref, pref):
        self.c0 = cref
        self.rho0 = rho_ref
        self.p0 = pref
        self.u0 = uref
        self.v0 = uref
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_p, d_J1, d_J2u, d_J2v, d_J3u, d_J3v, d_rho,
                   d_u, d_xn, d_yn):
        # J3 is zero.
        j1 = d_J1[d_idx]
        nx = d_xn[d_idx]
        ny = d_yn[d_idx]

        # Characteristic in the downstream direction.
        if nx > 0.5 or ny > 0.5:
            # If outlet normal is (nx, ny) = (1, 0)
            if nx > 0.5:
                j2 = d_J2u[d_idx]
                vel_ref = self.u0
            # If outlet normal is (nx, ny) = (0, 1)
            else:
                j2 = d_J2v[d_idx]
                vel_ref = self.v0
            d_rho[d_idx] = self.rho0 + (-j1 + 0.5 * j2) / self.c0**2
            d_u[d_idx] = vel_ref + (j2) / (2 * d_rho[d_idx] * self.c0)
            d_p[d_idx] = self.p0 + 0.5 * (j2)
        # Characteristic in the upstream direction.
        else:
            # If outlet normal is (nx, ny) = (-1, 0)
            if nx < -0.5:
                j3 = d_J3u[d_idx]
                vel_ref = self.u0
            # If outlet normal is (nx, ny) = (0, -1)
            else:
                j3 = d_J3v[d_idx]
                vel_ref = self.v0
            d_rho[d_idx] = self.rho0 + 0.5 * j3 / self.c0**2
            d_u[d_idx] = vel_ref + j3 / (2 * d_rho[d_idx] * self.c0)
            d_p[d_idx] = self.p0 + 0.5 * (j3)


class MovingSquare(WindTunnel):
    def initialize(self):
        super().initialize()
        self.side = 1 * self.dc
        self.cxy = self.Lt/6.66666, 0.0
        # self.cxy = self.Lt/2.0, 0.0

    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(
            potential=False, sol_adapt=False, Lt=10, Wt=2.5, tf=8.0, dc=1, re=100,
            dx_min=0.0125, dx_max=0.04, cr=1.15
        )

    def consume_user_options(self):
        if self.options.n_damp is None:
            self.options.n_damp = 20
        self.Lt = self.options.Lt
        self.Wt = self.options.Wt
        self.dc = self.options.dc
        self.sol_adapt = self.options.sol_adapt
        dx_min = self.options.dx_min
        dx_max = self.options.dx_max
        cr = self.options.cr
        self.dx_min = dx_min
        self.dx_max = dx_max
        self.cr = cr
        re = self.options.re

        self.nu = nu = u_square * self.dc / re

        self.hdx = hdx = self.options.hdx
        self.nl = (int)(10.0*hdx)

        self.h = h = hdx * self.dx_min
        self.cfl = cfl = self.options.cfl_factor
        # This division by a factor of 2 is needed. The umax in the domain is
        # upto 2.8 and not 1.0. U free stream is mislabelled  as umax.
        dt_cfl = cfl * h / ((c0 + umax))
        dt_viscous = 0.125 * h**2 / nu

        self.dt = min(dt_cfl, dt_viscous)
        self.tf = 8.4
        self.vacondio = self.options.vacondio
        self.hybrid = self.options.hybrid
        self.wake = self.options.wake

        self.side = 1.0*self.dc
        self.chord = self.dc # * 2   
        self.center = self.cxy # (self.Lt/3.0, 0.0)

    def _create_fluid(self):
        dx = self.dx_max
        h0 = self.hdx*dx
        x, y = np.mgrid[dx / 2:self.Lt:dx, -self.Wt + dx/2:self.Wt:dx]
        x, y = (np.ravel(t) for t in (x, y))
        one = np.ones_like(x)
        volume = dx * dx * one
        m = volume * rho
        fluid = get_particle_array(
            name='fluid', m=m, x=x, y=y, h=h0, u=0.0, p=0.0, rho=rho,
            vmag=0.0
        )
        return fluid

    def _create_solid(self):
        dx = self.dx_min
        self.cxy = self.Lt/6.66666, 0.0
        x0, y0 = self.cxy
        side = self.side
        y0 -= side*0.5
        h0 = self.hdx*dx
        x, y = np.mgrid[x0-side/2:x0+side/2:dx, y0:y0+side:dx]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, y_0=y, x0=x, u=0.0, m=volume*rho, rho=rho,
            h=h0, ds=dx
        )
        solid.add_constant('ds_min', dx)
        return solid

    def _create_wall(self):
        dx = self.dx_max
        h0 = self.hdx*dx
        volume = dx*dx

        x0, y0 = np.mgrid[
            dx/2: self.Lt+self.nl*dx+self.nl*dx: dx, dx/2: self.nl*dx: dx
        ]
        x0 -= self.nl*dx
        y0 -= self.nl*dx+self.Wt
        x0 = np.ravel(x0)
        y0 = np.ravel(y0)
        xn0 = np.zeros_like(x0)
        yn0 = -1 * np.ones_like(x0)

        x1 = np.copy(x0)
        y1 = np.copy(y0)
        y1 += self.nl*dx+2*self.Wt
        x1 = np.ravel(x1)
        y1 = np.ravel(y1)
        xn1 = np.zeros_like(x1)
        yn1 = np.ones_like(x1)

        x2, y2 = np.mgrid[dx/2:self.nl*dx:dx, -self.Wt + dx/2:self.Wt:dx]
        x2, y2 = (np.ravel(t) for t in (x2, y2))
        x2 = x2 - self.nl * dx
        xn2 = -1 * np.ones_like(x2)
        yn2 = np.zeros_like(x2)

        x3, y3 = np.mgrid[dx/2:self.nl * dx:dx,  -self.Wt + dx/2:self.Wt:dx]
        x3, y3 = (np.ravel(t) for t in (x3, y3))
        x3 += self.Lt
        xn3 = np.ones_like(x3)
        yn3 = np.zeros_like(x3)

        x0 = np.concatenate([x0, x1, x2, x3])
        y0 = np.concatenate([y0, y1, y2, y3])
        xn = np.concatenate([xn0, xn1, xn2, xn3])
        yn = np.concatenate([yn0, yn1, yn2, yn3])
        volume = dx*dx
        wall = get_particle_array(
            name='wall', x=x0, y=y0, m=volume*rho, rho=rho, h=h0, xn=xn, yn=yn
        )
        return wall

    def create_equations(self):
        from pysph.sph.equation import Group
        from edac import EvaluateNumberDensity

        equations = self.scheme.get_equations()
        eq = []
        eq.append(
            Group(equations=[
                EvaluateCharacterisctics(
                    dest='fluid', sources=None, cref=c0, rho_ref=rho,
                    uref=0.0, vref=0.0, pref=0.0
                )
            ], condition=check_time_outlet_bc)
        )
        eq.append(
            Group(equations=[
                EvaluateNumberDensity(dest='wall', sources=['fluid']),
                ShepardInterpolateCharacteristics(dest='wall', sources=['fluid']),
            ], condition=check_time_outlet_bc)
        )
        eq.append(Group(equations=[
            EvaluatePropertyfromCharacteristics(
                dest='wall', sources=None, cref=c0, rho_ref=rho,
                uref=0.0, vref=0.0, pref=0.0
            )], condition=check_time_outlet_bc)
        )
        wall_solid_bc = equations[2].equations.pop()
        condition_outlet_bc = Group(
            equations=[wall_solid_bc],
            condition=check_time_solid_bc
        )
        equations.insert(3, condition_outlet_bc)
        equations = eq + equations
        return equations

    def create_particles(self):
        dx = self.dx_min
        fluid = self._create_fluid()
        # solid = self._create_solid()

        solid = solid_airfoil(dx=dx, airfoil='0025', chord=self.chord, 
                               center=self.center, angle=0.0, Lt = self.Lt)

        wall = self._create_wall()

        # import matplotlib.pyplot as plt
        # plt.scatter(wall.x, wall.y, s=2)
        # plt.scatter(fluid.x, fluid.y, s=2)
        # plt.scatter(solid.x, solid.y, s=2)
        # plt.show()
        
        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
        self.wake = wake

        particles = [fluid, solid, wall]
        boundary = [solid]
        self.shift_sources = list(particles)

        if wake is not None:
            particles.append(wake)
            boundary.append(wake)
        self.scheme.setup_properties(particles)

        setup_properties([fluid])
        fluid.add_property('vmag')

        bg_freq = 2

        self._update_bg = UpdateBackground(
            fluid, dim=2, boundary=boundary, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=5,
            bg_freq=bg_freq, sol_adapt=self.sol_adapt
        )
        sep = '-'*70
        msg = 'Generating background (BG) particles:'
        print(sep, '\n', msg)
        start = time()
        self._update_bg.set_initial_bg()
        msg = f'time took to generate the BG particles, {time()-start:.4f}s'
        print(msg, '\n', sep)
        logger.info('BG initialize:\n%s\n  %s\n%s', sep, msg, sep)
        self._update_bg.initialize_fluid(solids=[solid], vacondio=self.vacondio,
                                         hybrid=self.hybrid)
        bg_pa = self._update_bg.bg_pa
        G.remove_overlap_particles(bg_pa, solid, self.dx_min, dim=2)

        particles.append(bg_pa)

        props = ['u0', 'v0', 'w0', 'x0', 'y0', 'z0']
        for prop in props:
            solid.add_property(prop)

        props = ['J1', 'J2u', 'J3u', 'J2v', 'J3v']
        for prop in props:
            fluid.add_property(prop)
        # fluid.add_output_arrays(props)
        props = [ 'J1', 'avg_j1', 'J2u', 'avg_j2u', 'J3u', 'avg_j3u', 'J2v',
                 'avg_j2v', 'J3v', 'avg_j3v']
        for p in props:
            if p not in wall.properties:
                wall.add_property(p)
        solid.add_output_arrays(['au'])

        solid.add_property('y_0')
        solid.y_0[:] = solid.y  

        return particles

    def create_inlet_outlet(self, particle_arrays):
        return []

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None, cfl=None,
            inviscid_solids=['wall']
        )
        return s

    def configure_scheme(self):
        from pysph.sph.integrator_step import TwoStageRigidBodyStep
        scheme = self.scheme
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0,
            output_at_times=list(range(1, 7)),
            extra_steppers={'solid': TwoStageRigidBodyStep()}
        )

    # Airfoil oscillation
    def _motion_eqn(self, t):
        omega = 2*pi/20
        St = 0.1
        l0 = 0.75 * self.chord
        ly = l0*sin(omega*t)
        lx = St*l0*sin(omega*t)
        av = -l0 * omega * omega * sin(omega*t)
        # au = -St * l0 * omega * omega * sin(omega * t)         
        return ly, lx, av

    def square_motion(self, t):
        a = 2.8209512
        b = 0.525652151
        c = 0.14142151
        d = -2.55580905e-08
        au = a * np.exp(-(t - b) * (t - b) / (2.0 * c * c)) + d
        return au

    def pre_step(self, solver):
        t = solver.t
        for particle in self.particles:
            if particle.name == 'solid':
                au = self.square_motion(t)
                ly, lx, av = self._motion_eqn(t)
                particle.y[:] = particle.y_0[:] + ly 
                particle.au[:] = -au
                particle.av[:] = av

    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()

    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        for name in ['fluid']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        b = particle_arrays['solid'].scalar = 'm'
        for name in ['fluid', 'solid', 'wall']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')


if __name__ == '__main__':
    app = MovingSquare()
    app.run()
    app.post_process(app.info_filename)
