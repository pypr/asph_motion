"""A cube bouncing inside a box. (5 seconds)

This is used to test the rigid body equations.
"""

import os
import matplotlib.pyplot as plt
import numpy as np
from numpy import pi, cos, sin, exp, arctan2

from pysph.base.kernels import CubicSpline
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Group

from pysph.tools import geometry as G
from pysph.sph.integrator import Integrator
from pysph.sph.integrator_step import IntegratorStep

from pysph.solver.application import Application
from pysph.solver.solver import Solver


def solid_airfoil(dx, airfoil, chord, angle):
    """ Generates an airfoil geometry for fluid flow simulation.
    """
    # data = np.load('airfoil12.npz')     #../
    # data = np.load('airfoil.npz')
    # x, y = data['xs'], data['ys']
    data = np.load('ellipse_dxmin_01.npz')
    x, y = data['xf'], data['yf']
    # data = np.load('airfoil.npz')
    # data = np.load('airfoil15.npz')
    # x, y = data['xs'], data['ys']
    z = np.zeros_like(x)
    x, y, z = G.rotate(x, y, z, angle=angle)
    return x, y


class DummyIntegrator(Integrator):
    def one_timestep(self):
        pass


class AirfoilMotion(Application):
    def consume_user_options(self):
        self.dim = 3

        self.dt = 0.01
        self.tf = 2.0

        self.dx = 0.01
        self.rho = 1.0

        self.chord = 1.0

        self.cxy = 0.0, 0

        self.amp = 1.0
        self.T = 15.0
        self.omega = 2 * pi / self.T
        self.a = 2 * self.chord
        self.b = 2 * self.a

    def create_particles(self):
        rho = self.rho
        dx = self.dx
        h0 = dx
        m = dx * dx * rho

        airfoil = '5515'
        chord = self.chord

        # Initial angle at t = 0 sec made by the airfoil w.r.t the x-axis.
        angle = 0.0

        # Return the positions of the airfoil.
        x, y = solid_airfoil(dx, airfoil, chord, angle)

        # Shift the center of the airfoil to cxy, i.e., the domain center
        # then shift it to Chord/3.
        x += self.cxy[0] # + self.b + self.chord * (1/2 - 1/3)
        y += self.cxy[1] #- self.b

        # Airfoil center of mass position at time 0 sec.
        self.xcm = self.cxy[0]
        self.ycm = self.cxy[1] #- self.b

        self.ucm = 0.0
        self.vcm = 0.0

        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, xi=x, yi=y, u=0.0, m=volume*rho, rho=rho, h=h0
        )
        plt.scatter(solid.x, solid.y, s=2)
        plt.scatter(self.xcm, self.ycm, s=8)
        plt.axis('equal')
        plt.savefig(os.path.join(self.output_dir, 'airfoil_geom.png'))
        plt.show()
        return [solid]

    def create_solver(self):
        dim = self.dim
        dt = self.dt
        tf = self.tf

        kernel = CubicSpline(dim=dim)

        integrator = DummyIntegrator()

        solver = Solver(kernel=kernel, dim=dim, integrator=integrator, dt=dt,
                        tf=tf)
        solver.set_print_freq(1)
        return solver

    def create_equations(self):
        return []

    ### Simple motion, elliptic-circle
    def rotate(self, x, y, axis, rad):
        R0 = np.cos(rad)
        R1 = -np.sin(rad)
        R2 = np.sin(rad)
        R3 = np.cos(rad)
        xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
        ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
        xr, yr = xnew + axis[0], ynew + axis[1]
        return xr, yr

    def motion_esfahani(self, t):
        omega = self.omega
        height = self.a - self.a * np.cos(omega * t) 
        length = self.b * np.sin(omega * t)
        u = omega * self.b * cos(omega * t)
        v = omega * self.a * sin(omega * t)
        au = -omega * omega * self.b * sin(omega * t)
        av = omega * omega * self.a * cos(omega * t)
        return height, length, u, v, au, av

    # Pitching motion with an angle of attack between [-10, 10] 
    # about the center of the body.
    def pre_step2(self, solver):
        t = solver.t
        omega = self.omega

        for particle in self.particles:
            if particle.name == 'solid':
                # Rotation
                axis = [self.xcm, self.ycm]
                theta = (10.0 * pi / 180.0) * cos(omega * t + pi/2)
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr 
                particle.y[:] = yr 

                # u_rotation is angular_velocity \cross r
                angular_velocity = -(10.0 * pi / 180.0) * omega * sin(omega * t + pi/2.0)
                airfoil_x = particle.x - axis[0]
                airfoil_y = particle.y - axis[1]
                u_rotation = -airfoil_y * angular_velocity
                v_rotation = airfoil_x * angular_velocity

                particle.u[:] = u_rotation
                particle.v[:] = v_rotation

                # # Translation
                # height, length, u, v, au, av = self.motion_esfahani(t)
                # particle.x[:] = particle.x0 + length
                # particle.y[:] = particle.y0 + height

                # # Rotation
                # axis = [xcm + length, ycm + height]
                # theta = (10 * pi/180) * cos(omega * t + pi/2)
                # xr, yr = self.rotate(particle.x, particle.y, axis, theta)
                # particle.x[:] = xr
                # particle.y[:] = yr

                # # u_rotation is angular_velocity \cross r
                # angular_velocity = -10 * pi/180 * omega * sin(omega * t + pi/2)
                # airfoil_x = particle.x - axis[0]
                # airfoil_y = particle.y - axis[1]
                # u_rotation = airfoil_y * -angular_velocity
                # v_rotation = airfoil_x * angular_velocity

                # particle.u[:] = u + u_rotation
                # particle.v[:] = v + v_rotation
                # particle.au[:] = au
                # particle.av[:] = av

    def pre_step2(self, solver):
        t = solver.t
        omega = 2 * np.pi * 0.5 #self.omega
        for particle in self.particles:
            if particle.name == 'solid':
                # Translation - PurePlunging (Vert + Horizontal Motion)
                height, length, u, v, au, av = self.motion_esfahani(t)
                particle.x[:] = particle.xi + length
                particle.y[:] = particle.yi + height
                particle.u[:] = u 
                particle.v[:] = v 
                particle.au[:] = au
                particle.av[:] = av

                # Rotation2 #- pitch
                axis2 = [self.xcm, self.ycm]
                theta = (10.0 * pi / 180.0) * sin(omega * t + pi/2)
                xr2, yr2 = self.rotate(particle.xi, particle.yi, axis2, theta)
                particle.x[:] += xr2
                particle.y[:] += yr2

                # u_rotation is angular_velocity \cross r
                angular_velocity = -(10.0 * pi / 180.0) * omega * sin(omega * t + pi/2.0)
                airfoil_x = particle.x - axis2[0]
                airfoil_y = particle.y - axis2[1]
                u_rotation = -airfoil_y * angular_velocity
                v_rotation = airfoil_x * angular_velocity

                # particle.u[:] = u_rotation
                # particle.v[:] = v_rotation

                particle.u[:] += -omega * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                ) + u_rotation
                particle.v[:] +=  omega * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                ) + v_rotation

                particle.au[:] += -omega**2 * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )
                particle.av[:] += -omega**2 * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )

    def pre_step4(self, solver):     # pitch_plunge motion
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                omega = 2 * np.pi * 0.5

                # Rotation #- pitch
                axis = [self.cxy[0], self.cxy[1]]
                theta = omega * t
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                # # Rotation2 #- pitch
                # axis2 = [self.xcm, self.ycm]
                # theta = (2.48 * pi / 180.0) * cos(omega * t + pi/2)
                # xr2, yr2 = self.rotate(particle.xi, particle.yi, axis2, theta)
                # particle.x[:] += xr2
                # particle.y[:] += yr2

                particle.u[:] += -omega * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )
                particle.v[:] +=  omega * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )

                particle.au[:] += -omega**2 * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )
                particle.av[:] += -omega**2 * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )

    def square_motion(self, t):
        a = 2.8209512
        b = 0.525652151
        c = 0.14142151
        d = -2.55580905e-08

        au = a * np.exp(-(t - b) * (t - b) / (2.0 * c * c)) + d
        return au

    # Motion one: pitching-translating
    def pre_step(self, solver):
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                omega = 2.5 * self.omega  # 2 * np.pi * 0.5
                # Rotation
                axis = [self.xcm, self.ycm]
                theta  = -10 * np.pi / 180 * np.sin(omega * t)
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                # Translation of com
                # Acceleration due to translation of cm.
                au_trans = self.square_motion(t)
                av_trans = 0.0
                particle.au[:] = au_trans
                particle.av[:] = av_trans
                # Velocity due to translation of cm.
                ucm_old = self.ucm
                vcm_old = self.vcm
                self.ucm += dt * au_trans
                self.vcm += dt * av_trans

                particle.u[:] = 0.5 * (ucm_old + self.ucm)
                particle.v[:] = 0.5 * (vcm_old + self.vcm)
                self.xcm += 0.5 * dt * (ucm_old + self.ucm)
                self.ycm += 0.5 * dt * (vcm_old + self.vcm)
                particle.x[:] += 0.5 * dt * (ucm_old + self.ucm)
                particle.y[:] += 0.5 * dt * (vcm_old + self.vcm)
                particle.xi[:] += 0.5 * dt * (ucm_old + self.ucm)
                particle.yi[:] += 0.5 * dt * (vcm_old + self.vcm)

                # Velocity due to pitching of rigid body about cm.
                particle.u[:] += -omega * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )
                particle.v[:] +=  omega * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )
                # Acceleration due to translation of cm about self.cxy.
                particle.au[:] += -omega**2 * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )
                particle.av[:] += -omega**2 * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )




if __name__ == '__main__':
    app = AirfoilMotion()
    app.run()
