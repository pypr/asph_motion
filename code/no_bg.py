import logging
from time import time
from math import sqrt, log
import numpy
import numpy as np
from compyle.api import annotate, declare, wrap, profile, profile_ctx
from compyle.api import get_profile_info
from compyle.parallel import Elementwise
from compyle.low_level import cast
from pysph.base.kernels import QuinticSpline
from pysph.base.particle_array import ParticleArray
from pysph.sph.equation import Equation, Group
from pysph.tools.sph_evaluator import SPHEvaluator
from pysph.tools.interpolator import get_bounding_box
from pysph.solver.tools import Tool
from pysph.tools import geometry as G
from adapt import AdaptiveResolution

logger = logging.getLogger(__name__)


def setup_fluid_props(particle_arrays, ds_min, ds_max, cr, rho_ref):
    props = 'htmp ds m_min m_max shift_x shift_y shift_z vor_max'.split()
    int_props = 'n_nbrs closest_idx split fixed'.split()
    constants = dict(vor_max_all=0.0, ds_min=float(ds_min), ds_max=float(ds_max),
                     cr=float(cr), length=2.0, rho_ref=float(rho_ref))
    for pa in particle_arrays:
        for prop in props:
            if prop not in pa.properties:
                pa.add_property(prop)

        for prop in int_props:
            if prop not in pa.properties:
                pa.add_property(prop, type='int')

        for const, val in constants.items():
            if const not in pa.constants:
                pa.add_constant(const, val)

        pa.add_property('gradv', stride=9)
        pa.add_property('gradp', stride=3)
        pa.add_output_arrays(['ds', 'm_min', 'm_max', 'n_nbrs'])


class SmoothedVorticity(Equation):
    def initialize(self, d_idx, d_vor_max):
        d_vor_max[d_idx] = 0.0

    def loop(self, s_idx, d_idx, d_vor, s_vor, d_vor_max):
        d_vor_max[d_idx] = max(d_vor[d_idx], abs(s_vor[s_idx]))

    def reduce(self, dst, t, dt):
        dst.vor_max_all[0] = numpy.max(dst.vor)


class SolutionAdaptive(Equation):
    def __init__(self, dest, sources, cutoff, xmax, abs_cutoff):
        self.cutoff = cutoff
        self.xmax = xmax
        self.abs_cutoff = abs_cutoff
        super().__init__(dest, sources)

    def post_loop(self, d_idx, d_x, d_fixed, d_vor_max, d_ds, d_rho, d_m_min,
                  d_m_max, d_vor_max_all, d_ds_min):
        vm = d_vor_max_all[0]
        w = abs(d_vor_max[d_idx])
        ds_ref = d_ds[d_idx]
        cutoff = max(self.cutoff*vm, self.abs_cutoff)
        if (d_fixed[d_idx] & 1) == 0 and w > cutoff and d_x[d_idx] < self.xmax:
            s = d_ds_min[0]
            ds = min(s, ds_ref)
            d_ds[d_idx] = ds
            d_fixed[d_idx] |= 1
            # XXX: Fix for 3D
            mref = d_rho[d_idx]*ds*ds
            d_m_max[d_idx] = 1.05*mref
            d_m_min[d_idx] = 0.5*mref


class AdaptiveResolutionVacondioNoBG(AdaptiveResolution):
    def __init__(self, pa, dim, rho0, ds_min, ds_max, cr, boundary=None, bg=False,
                 do_mass_exchange=False, freq=5,
                 has_ghost=False, hybrid=True, nnps=None, sol_adapt=False,
                 abs_cutoff=0.01):
        self.hybrid = hybrid
        self.sol_adapt = sol_adapt
        self.abs_cutoff = abs_cutoff
        setup_fluid_props(
            [pa], ds_min=ds_min, ds_max=ds_max, cr=cr,
            rho_ref=rho0
        )
        super().__init__(pa, dim, rho0, boundary, bg, do_mass_exchange, freq,
                         has_ghost, nnps=nnps)
        # Each particle is split into these many particles
        if dim == 2:
            self.N_SPLIT = 7
        else:
            raise NotImplementedError(
                'Vacondio scheme not supported in 3D yet.'
            )
        if not bg:
            from adaptv import (copy_props2d_vacondio_6_beta_1,
                                copy_props2d_vacondio)
            from adapt import copy_props3d
            if dim == 2:
                if hybrid:
                    self.copy_props = Elementwise(copy_props2d_vacondio_6_beta_1,
                                                  backend=self.backend)
                else:
                    # If not hybrid use Vacondio's defaults.
                    self.copy_props = Elementwise(copy_props2d_vacondio,
                                                  backend=self.backend)
            elif dim == 3:
                self.copy_props = Elementwise(copy_props3d,
                                              backend=self.backend)
            else:
                raise NotImplementedError(
                    'Adaptive resolution not supported in 1D.'
                )

    def _post_split_update(self):
        if self.hybrid:
            self._add_remove_split_particles()
            self.sph_eval.update(update_domain=False)

    def solution_adaptive(self, t=0.0, dt=0.0):
        return self.sol_adapt > 0

    def _setup_evaluator(self):
        from adapt import (FindSplitMergeBase, SkipNearBoundary, MassExchange,
                           MarkBoundary, UpdateSpacing)
        from adaptv import FindSplitMergeV, PreMergeSetup, MergeIterative

        arrays = [self.pa]
        groups = []
        eqs = []
        name = self.name
        all_sources = [name]
        bounds = get_bounding_box([self.pa], tight=True)
        xmax = bounds[1]

        if self.boundary:
            boundary_names = [x.name for x in self.boundary]
            all_sources.extend(boundary_names)
            arrays.extend(self.boundary)

        eq = []
        eq.append(Group(
            equations=[MarkBoundary(dest=name, sources=boundary_names)]
        ))
        eq.append(Group(
            equations=[SmoothedVorticity(dest=name, sources=[name])],
            condition=self.solution_adaptive
        ))
        eq.append(Group(
            equations=[
                SolutionAdaptive(dest=name, sources=None,
                                    cutoff=self.sol_adapt, xmax=0.9*xmax,
                                    abs_cutoff=self.abs_cutoff)
            ],
            condition=self.solution_adaptive,
        ))
        eq.append(Group(
            equations=[UpdateSpacing(dest=name, sources=[name])],
        ))
        groups.extend(eq)

        eqs = []
        if self.hybrid:
            # Make hybrid work with Yang and Kong parameters.
            eqs.append(
                FindSplitMergeV(dest=self.name, sources=[self.name])
            )
        else:
            eqs.append(FindSplitMergeV(dest=self.name, sources=[self.name]))
        if self.boundary:
            eqs.append(SkipNearBoundary(dest=self.name, sources=boundary_names))
        groups.append(Group(equations=eqs, post=self._post_split_update))

        if self.do_mass_exchange:
            groups.append(Group(
                equations=[
                    PreMergeSetup(dest=name, sources=None),
                    MassExchange(dest=name, sources=[name])
                ]
            ))

        if self.hybrid:
            groups.append(
                Group(equations=[PreMergeSetup(dest=name, sources=[name])])
            )
            equations = [MergeIterative(dest=name, sources=[name])]
            groups.append(
                Group(equations=equations, iterate=True, max_iterations=3)
            )

        sph_eval = SPHEvaluator(
            arrays=arrays, equations=groups, dim=self.dim,
            nnps_factory=self._create_nnps
        )
        return sph_eval
