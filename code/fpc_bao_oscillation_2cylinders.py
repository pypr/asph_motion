"""
Flow past two cylinders.
"""
import os
import numpy as np
from numpy import pi, cos, sin, exp
from adapt import setup_properties
from fpc_auto import WindTunnel
from edac import AdaptiveEDACScheme

# Motion parameters
T = 10
omega = 2*np.pi/T  # omega=6.283 / T = 0.628
A = 0.25 # A = [0.25, 1.25] A/D = ??

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0

# Matchs using dxmin 0.025, dxmax 0.5
class FPCBao(WindTunnel):
    def initialize(self):
        super().initialize()
        self.remove_inner_layers = False
        self.gap = 2.5 * self.dc
        self.A = 0.5 * self.dc

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument(
            '--gap', action="store", type=float, dest="gap",
            default=1.5,
            help="Spacing gap between cylinders (multiplied to cylinder diameter)."
        )
        group.set_defaults(
            dc=1.0, re=100, tf=10, potential=False, dx_min=0.0125,
            Lt=75, Wt=30, dx_max=0.5, cr=1.12
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.gap = self.options.gap * self.dc
        self.cxy = 0.2667 * self.Lt, +0.5 * self.gap

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid', 'solid2'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def create_particles(self):
        fluid, wall, inlet, outlet = self._create_box()
        solid = self._create_solid()

        solid2 = self._create_solid()
        solid2.set_name('solid2')
        solid2.y[:] -= self.gap

        particles = [fluid, inlet, outlet, solid, solid2, wall]
        bg_sources = [solid, solid2]

        # Do not add wake, if present, to shift_sources.
        self.shift_sources = list(particles)

        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
            particles.append(wake)
            bg_sources.append(wake)
        self.wake = wake

        # Do not use clean=True here. The properties not used in EDAC equations
        # but used in the create_equations or adaptive will be erased.
        self.scheme.setup_properties(particles, clean=False)

        setup_properties([fluid, inlet, outlet])
        fluid.add_property('vmag')

        bg_pa = self._create_bg(fluid, bg_sources)
        particles.append(bg_pa)

        solid.add_property('y_0')
        solid.y_0[:] = solid.y
        solid2.add_property('y_0')
        solid2.y_0[:] = solid2.y

        return particles

    def post_process(self, info_fname):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt

        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        for solid_name in ['solid', 'solid2']:
            print(solid_name)
            filename = 'results_' + solid_name + '.npz'
            self.res = os.path.join(self.output_dir, filename)
            print(self.res)
            t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t(
                'fluid', solid_name
            )
            plt.plot(t, cd, label=r'$C_d$' + solid_name)
            plt.plot(t, cl, label=r'$C_l$' + solid_name)
            plt.plot(t, cd_sf, label=r'$C_{df}$' + solid_name)
            plt.plot(t, cl_sf, label=r'$C_{lf}$' + solid_name)
            plt.xlabel(r'$t$')
            plt.ylabel('cd-cl')
            plt.legend()
            plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()


    def pre_step(self, solver):               
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                # if t > 1.0:
                particle.y[:] = particle.y_0[:] + self.A * np.sin(omega * t)
                particle.v[:] = self.A * np.cos(omega*t) * omega 
                particle.av[:] = -self.A * np.sin(omega *t) * (omega)**2

            elif particle.name == 'solid2':
                # if t > 1.0:
                particle.y[:] = particle.y_0[:] - self.A * np.sin(omega * t)   
                particle.v[:] = -self.A * np.cos(omega * t) * omega 
                particle.av[:] = self.A * np.sin(omega * t) * (omega)**2


    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid', 'solid2']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')


if __name__ == '__main__':
    app = FPCBao()
    app.run()
    app.post_process(app.info_filename)
