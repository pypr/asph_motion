"""
Flow past two cylinders.
"""
import os
import numpy as np
from numpy import pi, cos, sin, exp, arctan2
from adapt import setup_properties
from fpc_auto import WindTunnel
from edac import AdaptiveEDACScheme

from pysph.tools import geometry as G
from pysph.base.utils import get_particle_array

from adapt import UpdateBackground, setup_properties
from fpc_auto import (
    WindTunnel, EvaluateCharacterisctics, ShepardInterpolateCharacteristics,
    EvaluatePropertyfromCharacteristics
)

# Motion parameters
# St = 0.1 	    # Strouhal Number (St=2fh0/Uinf)
k = 0.45            # Reduced frequency (k=pifc/Uinf)
T = (pi/k)          # One period (T=1.0/f)
omega = 2*np.pi/T   # Angular velocity (omega=2pi/T)

# Fluid mechanical/numerical parameters
rho = 1000
u_freestream = 1.0

##-------------------------------------------------------------
dx = 0.1
hdx = 1.2
h0 = dx * hdx

from pysph.tools.geometry import get_4digit_naca_airfoil, rotate

def solid_airfoil(dx, airfoil, chord, angle):
    """ Generates an airfoil geometry for fluid flow simulation.
    """
    angle = -6.0 # -90
    # data = np.load('airfoil5515.npz')
    data = np.load('airfoil_dxmin_004.npz') # Origin@c/3 N12
    x, y = data['xf'], data['yf']

    # import matplotlib.pyplot as plt 
    # plt.scatter(x, y, s=2)
    # plt.show()

    # # Sort the x-coordinated in ascending order
    # x_index = x.argsort()

    # # Remove the (x, y) coordinates of the last two points w.r.t x-position.
    # x = x[x_index[:-2]]
    # y = y[x_index[:-2]]

    # import matplotlib.pyplot as plt 
    # plt.scatter(x, y, s=2)
    # plt.show()
    # data = np.load('airfoil.npz')
    # data = np.load('airfoil15.npz')
    # x, y = data['xs'], data['ys']
    z = np.zeros_like(x)
    x, y, z = G.rotate(x, y, z, angle=angle)
    return x, y
##-------------------------------------------------------------

def compute_forces(fname, sname, nu):
    from edac import (
        MomentumEquationPressureGradient2, SummationDensityGather,
        SetWallVelocity, EvaluateNumberDensity, ComputeBeta,
        SolidWallPressureBC
    )

    all_sources = [fname, sname]
    equations = []

    g1 = []
    g1.extend([
        SummationDensityGather(dest=sname, sources=all_sources),
        ComputeBeta(dest=sname, sources=all_sources, dim=2),
        EvaluateNumberDensity(dest=sname, sources=[fname]),
        SetWallVelocity(dest=sname, sources=[fname]),
        SolidWallPressureBC(dest=sname, sources=[fname]),
    ])
    equations.append(Group(equations=g1, real=False))

    g2 = []
    g2.extend([
        MomentumEquationPressureGradient2(dest=sname, sources=[fname]),
        SolidWallNoSlipBCReverse(dest=sname, sources=[fname], nu=nu),
    ])
    equations.append(Group(equations=g2, real=True))
    return equations


class AirfoilMotion(WindTunnel):
    def initialize(self):
        super().initialize()
        self.remove_inner_layers = False
        self.chord = 1.0
        self.sol_adapt = 0.0

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument(
            '--omega', action="store", type=float, dest="omega",
            default=omega,
            help="Angular velocity of oscillation (2*pi*fe)."
        )
        group.add_argument(
            '--amp', action="store", type=float, dest="amp",
            default=1.0,
            help="Amplitude of cylinder oscillation."
        )
        group.add_argument(
            '--b', action="store", type=float, dest="b",
            default=0.0,
            help="The minor length of the ellipse."
        )
        motion_types = ['pitch', 'plunge', 'pitchplunge']
        group.add_argument(
            "--motion", action="store", type=str, dest="motion",
            default='plunge', choices=motion_types,
            help="Use three types of airfoil motion."
        )
        group.set_defaults(
            potential=False, dc=1.0, re=10000, dx_max=0.5, dx_min=0.004,
            Lt=10, Wt=3, pf=500, cr=1.12, tf=15.0
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.amp = self.options.amp
        self.omega = self.options.omega
        self.cxy = self.Lt/4.0, 0.0
        self.a = self.chord * 0.75 # * self.amp
        self.b = self.options.b
        self.motion = self.options.motion

        umax = (self.omega * np.sqrt(2 * (self.a + self.b)**2) * 2 + u_freestream)
        self.c0 = 10 * umax
        dt_cfl = self.cfl * self.h / (self.c0 + umax)
        self.dt = min(dt_cfl, self.dt)

        self.sol_adapt = self.options.sol_adapt

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        m = dx * dx * rho
        dia = self.dc
        airfoil = '0012'
        chord = self.chord
        angle = 0.0

        # Return the positions of the airfoil.
        x, y = solid_airfoil(dx, airfoil, chord, angle)
       
        # Shift the center of the airfoil to cxy, i.e., the domain center
        # then shift it to Chord/3.
        x += self.cxy[0] + self.b + self.chord * (1/2 - 1/3)
        y += self.cxy[1]

        # Airfoil center of mass position at time 0 sec.
        self.xcm = self.cxy[0]
        self.ycm = self.cxy[1]

        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, x0=x, y0=y, u=0.0, m=volume*rho, rho=rho,
            h=h0, ds=dx
        )
        solid.add_constant('ds_min', dx)
        # import matplotlib.pyplot as plt 
        # plt.scatter(solid.x, solid.y, s=2)
        # plt.show()
        return solid

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=None,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def configure_scheme(self):
        super().configure_scheme()
        self.scheme.configure(c0=self.c0)
        # times = np.linspace(0, 3, 24) * T
        # self.scheme.configure_solver(
        #     output_at_times=list(times)
        # )


    def create_particles(self):
        fluid, wall, inlet, outlet = self._create_box()
        solid = self._create_solid()

        particles = [fluid, inlet, outlet, solid, wall]
        bg_sources = [solid]

        # Do not add wake, if present, to shift_sources.
        self.shift_sources = list(particles)

        if not self.wake or self.sol_adapt:
            wake = None
        else:
            wake = self._create_pseudo_wake()
            particles.append(wake)
            bg_sources.append(wake)
        self.wake = wake

        # Do not use clean=True here. The properties not used in EDAC equations
        # but used in the create_equations or adaptive will be erased.
        self.scheme.setup_properties(particles, clean=False)

        setup_properties([fluid, inlet, outlet])
        fluid.add_property('vmag')

        bg_freq = 10
        bg_pa = self._create_bg(fluid, bg_sources, bg_freq)
        particles.append(bg_pa)

        return particles

    def post_process(self, info_fname):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt

        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        for solid_name in ['solid']:
            print(solid_name)
            filename = 'results_' + solid_name + '.npz'
            self.res = os.path.join(self.output_dir, filename)
            print(self.res)
            t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t(
                'fluid', solid_name
            )
            tau = t/T
            plt.plot(tau, cd, label=r'$C_d$' + solid_name)
            plt.plot(tau, cl, label=r'$C_l$' + solid_name)
            plt.plot(tau, cd_sf, label=r'$C_{df}$' + solid_name)
            plt.plot(tau, cl_sf, label=r'$C_{lf}$' + solid_name)
            plt.xlabel(r'$t/T$')
            plt.ylabel('cd-cl')
            plt.legend()
            plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    ### Simple motion, elliptic-circle
    def rotate(self, x, y, axis, rad):
        # rad = angle * np.pi / 180
        R0 = np.cos(rad)
        R1 = -np.sin(rad)
        R2 = np.sin(rad)
        R3 = np.cos(rad)
        xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
        ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
        xr, yr = xnew + axis[0], ynew + axis[1]
        return xr, yr

    def motion_esfahani(self, t):
        omega = self.omega
        height = self.a - self.a * np.cos(omega * t) 
        length = self.b * np.sin(omega * t)

        u = omega * self.b * cos(omega * t)
        v = omega * self.a * sin(omega * t)

        au = -omega * omega * self.b * sin(omega * t)
        av = omega * omega * self.a * cos(omega * t)
        return height, length, u, v, au, av

    # Airfoil Motion Types (plunge, pitch, pitch_plunge).
    def pure_plunge(self, solver):
        t = solver.t
        omega = self.omega
        for particle in self.particles:
            if particle.name == 'solid':
                # Translation - PurePlunging (Vert + Horizontal Motion)
                height, length, u, v, au, av = self.motion_esfahani(t)
                particle.x[:] = particle.x0 + length
                particle.y[:] = particle.y0 + height
                particle.u[:] = u 
                particle.v[:] = v 
                particle.au[:] = au
                particle.av[:] = av

    # Pitching motion with an angle of attack between [-10, 10] 
    # about the center of the body.
    def pure_pitch(self, solver):
        t = solver.t
        omega = self.omega
        for particle in self.particles:
            if particle.name == 'solid':
                # PurePitching - Rotating Motion abt. (xcm,ycm)
                axis = [self.xcm, self.ycm]
                theta_0 = (10.0 * pi / 180.0)
                theta = (theta_0) * cos(omega * t + pi/2)
                xr, yr = self.rotate(particle.x0, particle.y0, axis, theta)
                particle.x[:] = xr 
                particle.y[:] = yr 

                # u_rotation is angular_velocity \cross r
                angular_velocity = -(theta_0) * omega * sin(omega * t + pi/2.0)
                airfoil_x = particle.x - axis[0]
                airfoil_y = particle.y - axis[1]
                u_rotation = -airfoil_y * angular_velocity
                v_rotation = airfoil_x * angular_velocity

                # angular acceleration = d/dt of angular velocity
                angular_acc = -(theta_0) * omega * omega * cos(omega * t + pi/2.0)

                # acceleration = angular velocity \cross r
                au_rotation = -angular_acc * airfoil_y
                av_rotation = angular_acc * airfoil_x
                particle.u[:] = u_rotation
                particle.v[:] = v_rotation
                particle.au[:] = au_rotation
                particle.av[:] = av_rotation

    def pitch_plunge(self, solver):
        t = solver.t
        omega = self.omega
        for particle in self.particles:
            if particle.name == 'solid':
                # Translation
                height, length, u, v, au, av = self.motion_esfahani(t)
                particle.x[:] = particle.x0 + length
                particle.y[:] = particle.y0 + height

                # Rotation - pitch
                axis = [self.xcm + length, self.ycm + height]
                theta_0 = (2.48 * pi / 180.0)
                theta = (theta_0) * cos(omega * t + pi/2)
                xr, yr = self.rotate(particle.x, particle.y, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                # u_rotation is angular_velocity \cross r
                angular_velocity = -(theta_0) * omega * sin(omega * t + pi/2.0)
                airfoil_x = particle.x - axis[0]
                airfoil_y = particle.y - axis[1]
                u_rotation = -airfoil_y * angular_velocity
                v_rotation = airfoil_x * angular_velocity

                # angular acceleration = d/dt of angular velocity
                angular_acc = -(theta_0) * omega * omega * cos(omega * t + pi/2.0)

                # acceleration = angular velocity \cross r
                au_rotation = -angular_acc * airfoil_y
                av_rotation = angular_acc * airfoil_x
                particle.u[:] = u + u_rotation
                particle.v[:] = v + v_rotation
                particle.au[:] = au + au_rotation
                particle.av[:] = av + av_rotation


    # def pre_step(self, solver):
    #     if self.motion == 'plunge':
    #         self.pure_plunge(solver)
    #     elif self.motion == 'pitch':
    #         self.pure_pitch(solver)
    #     elif self.motion == 'pitchplunge':
    #         self.pitch_plunge(solver)


    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 3.0
        ''')


if __name__ == '__main__':
    app = AirfoilMotion()
    app.run()
    app.post_process(app.info_filename)
