"""
Flow past Ellipse.
"""
import os
import numpy as np
from numpy import pi, sin, cos

from pysph.base.utils import get_particle_array
from moving_square import MovingSquare


# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


def rotate(x, y, axis, deg):
    rad = deg * np.pi / 180
    R0 = np.cos(rad)
    R1 = -np.sin(rad)
    R2 = np.sin(rad)
    R3 = np.cos(rad)
    xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
    ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
    return xnew + axis[0], ynew + axis[1]


class EllipseMotion(MovingSquare):
    def initialize(self):
        super().initialize()
        self.diameter = 1 * self.dc
        self.remove_inner_layers = False
        self.chord = 1.0
        self.sol_adapt = 0.075
        self.omega = 2 * np.pi / 10

    def add_user_options(self, group):
        super().add_user_options(group)
        group.add_argument(
            '--a', action="store", type=float, dest="a",
            default=1.0,
            help="The major length of the ellipse."
        )
        group.add_argument(
            '--b', action="store", type=float, dest="b",
            default=0.5,
            help="The minor length of the ellipse."
        )
        group.set_defaults(
            potential=False, sol_adapt=False, dc=1, cr=1.12, pf=250, re=550, 
            #Lt=30, Wt=15, dx_min=0.01, dx_max=0.15, tf=10,   # rotation
            Lt=20, Wt=6, dx_min=0.01, dx_max=0.1, tf=10,   # translation
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.cxy = self.Lt/5, 0.0
        self.a = 2 * self.chord
        self.b = 2.5 * self.a

        # 1.5 is a small factor taking into account the
        # diameter of the ellipse.
        umax = self.omega * self.b * 1.5
        self.c0 = 10 * umax
        dt_cfl = self.cfl * self.h / (self.c0 + umax)
        self.dt = min(dt_cfl, self.dt)

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        data = np.load('ellipse_dxmin_01.npz')
        x, y = data['xf'], data['yf']

        x += self.cxy[0]
        y += self.cxy[1] #- self.b
        
        self.xcm = self.cxy[0]
        self.ycm = self.cxy[1] #- self.b
        self.ucm = 0.0
        self.vcm = 0.0

        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, m=volume*rho, rho=rho, h=h0, xi=x, yi=y,
        )
        solid.add_constant('ds_min', dx)

        import matplotlib.pyplot as plt
        plt.scatter(solid.x, solid.y, s=2)
        plt.scatter(self.xcm, self.ycm, s=8, c='r')
        plt.scatter(self.cxy[0], self.cxy[1], s=8, marker='x', c='k')
        plt.axis('equal')
        plt.savefig(os.path.join(self.output_dir, 'geom.png'))
        # plt.show()
        return solid

    def rotate(self, x, y, axis, rad):
        R0 = np.cos(rad)
        R1 = -np.sin(rad)
        R2 = np.sin(rad)
        R3 = np.cos(rad)
        xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
        ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
        xr, yr = xnew + axis[0], ynew + axis[1]
        return xr, yr

    def square_motion(self, t):
        a = 2.8209512
        b = 0.525652151
        c = 0.14142151
        d = -2.55580905e-08

        au = a * np.exp(-(t - b) * (t - b) / (2.0 * c * c)) + d
        return au

    # Motion one: pitching-translating
    def pre_step(self, solver):
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                omega = 4 * self.omega  # 2 * np.pi * 0.5
                # Rotation
                axis = [self.xcm, self.ycm]
                theta  = 10 * np.pi / 180 * np.sin(omega * t)
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                # Translation of com
                # Acceleration due to translation of cm.
                au_trans = self.square_motion(t)
                av_trans = 0.0
                particle.au[:] = au_trans
                particle.av[:] = av_trans
                # Velocity due to translation of cm.
                ucm_old = self.ucm
                vcm_old = self.vcm
                self.ucm += dt * au_trans
                self.vcm += dt * av_trans

                particle.u[:] = 0.5 * (ucm_old + self.ucm)
                particle.v[:] = 0.5 * (vcm_old + self.vcm)
                self.xcm += 0.5 * dt * (ucm_old + self.ucm)
                self.ycm += 0.5 * dt * (vcm_old + self.vcm)
                particle.x[:] += 0.5 * dt * (ucm_old + self.ucm)
                particle.y[:] += 0.5 * dt * (vcm_old + self.vcm)
                particle.xi[:] += 0.5 * dt * (ucm_old + self.ucm)
                particle.yi[:] += 0.5 * dt * (vcm_old + self.vcm)

                # Velocity due to pitching of rigid body about cm.
                particle.u[:] += -omega * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )
                particle.v[:] +=  omega * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )
                # Acceleration due to translation of cm about self.cxy.
                particle.au[:] += -omega**2 * (
                    cos(omega * t) * (particle.x - self.xcm) -
                    sin(omega * t) * (particle.y - self.ycm)
                )
                particle.av[:] += -omega**2 * (
                    sin(omega * t) * (particle.x - self.xcm) +
                    cos(omega * t) * (particle.y - self.ycm)
                )

    def motion_esfahani(self, t):
        omega = 2 * np.pi * 0.5     # self.omega
        height = self.a - self.a * np.cos(omega * t) 
        length = self.b * np.sin(omega * t)
        u = omega * self.b * cos(omega * t)
        v = omega * self.a * sin(omega * t)
        au = -omega * omega * self.b * sin(omega * t)
        av = omega * omega * self.a * cos(omega * t)
        return height, length, u, v, au, av

    # Motion Three: pitching-plunging - flapping MAV type motion
    # def pre_step(self, solver): 

    # Motion Three: Flapping motion, MAV type (elliptic-plunging)
    def pre_step2(self, solver):
        t = solver.t
        omega = self.omega  # 2 * np.pi * 0.1
        for particle in self.particles:
            if particle.name == 'solid':
                # Translation - PurePlunging (Vert + Horizontal Motion)
                height, length, u, v, au, av = self.motion_esfahani(t)
                particle.x[:] = particle.xi + length
                particle.y[:] = particle.yi + height
                particle.u[:] = u 
                particle.v[:] = v 
                particle.au[:] = au
                particle.av[:] = av


    # Motion Two: Rotating motion - vawt type
    def pre_step3(self, solver):     
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                omega = self.omega
                # Rotation #- pitch
                axis = [self.cxy[0], self.cxy[1]]
                theta = omega * t
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                particle.u[:] = -omega * (
                    sin(omega * t) * (particle.x - axis[0]) +
                    cos(omega * t) * (particle.y - axis[1])
                )
                particle.v[:] =  omega * (
                    cos(omega * t) * (particle.x - axis[0]) -
                    sin(omega * t) * (particle.y - axis[1])
                )
                particle.au[:] = -omega**2 * (
                    cos(omega * t) * (particle.x - axis[0]) -
                    sin(omega * t) * (particle.y - axis[1])
                )
                particle.av[:] = -omega**2 * (
                    sin(omega * t) * (particle.x - axis[0]) +
                    cos(omega * t) * (particle.y - axis[1])
                )

    # Double rotation problem.
    # Motion Two: pitching-rotating - vawt type motion
    def pre_step4(self, solver):     # pitch_plunge motion
        t = solver.t
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                omega = self.omega  # 2 * np.pi * 0.5
                # Translation
                height, length, u, v, au, av = self.motion_esfahani(t)
                particle.x[:] = particle.xi + length
                particle.y[:] = particle.yi + height

                # Rotation - pitch
                axis = [self.xcm + length, self.ycm + height]
                theta_0 = (2.48 * pi / 180.0)
                theta = (theta_0) * cos(omega * t + pi/2)
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                # u_rotation is angular_velocity \cross r
                angular_velocity = -(theta_0) * omega * sin(omega * t + pi/2.0)
                airfoil_x = particle.x - axis[0]
                airfoil_y = particle.y - axis[1]
                u_rotation = -airfoil_y * angular_velocity
                v_rotation = airfoil_x * angular_velocity
                # angular acceleration = d/dt of angular velocity
                angular_acc = -(theta_0) * omega * omega * cos(omega * t + pi/2.0)
                # acceleration = angular velocity \cross r
                au_rotation = -angular_acc * airfoil_y
                av_rotation = angular_acc * airfoil_x
                particle.u[:] = u + u_rotation
                particle.v[:] = v + v_rotation
                particle.au[:] = au + au_rotation
                particle.av[:] = av + av_rotation





if __name__ == '__main__':
    app = EllipseMotion()
    app.run()
    app.post_process(app.info_filename)
