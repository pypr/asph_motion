from pysph.base.kernels import QuinticSpline
from moving_square import MovingSquare


class Performance(MovingSquare):
    def create_nnps(self):
        if self._nnps is None:
            from pysph.base.nnps import OctreeNNPS as NNPS
            self._nnps = NNPS(dim=2.0, particles=self.particles,
                              radius_scale=3.0, cache=True)
        return self._nnps

    def create_particles(self):
        particles = super().create_particles()
        self._update_bg.adapt_fluid.freq = 5
        return particles

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl, c0=self.c0)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0,
            output_at_times=list(range(1, 7)),
        )

    def pre_step(self, solver):
        dt = solver.dt
        for particle in self.particles:
            if particle.name == 'solid':
                u = 1.0
                particle.u[:] = u
                particle.x[:] += u*dt

    def post_process(self, info_filename):
        pass


if __name__ == '__main__':
    app = Performance()
    app.run()
    app.post_process(app.info_filename)
