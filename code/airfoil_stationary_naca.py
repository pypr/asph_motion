"""
Flow past airfoil
"""
import os
import numpy as np
from numpy import pi, cos, sin, exp, math
from pysph.tools import geometry as G
from pysph.base.utils import get_particle_array


from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Equation, Group
from pysph.solver.application import Application
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme, SolidWallNoSlipBCReverse


from fpc_auto import WindTunnel


# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0 # 0.001 
u_freestream = 1.0 # 0.001 
c0 = 10 * umax
p0 = rho * c0 * c0

# Motion parameters
T = 100
A = 1.0 # A = [0.25, 1.5] A/D = ?? 
omega = 2*np.pi/T  # omega=6.283/T = 0.628/3

##-------------------------------------------------------------

dx = 0.1
hdx = 1.2
h0 = dx * hdx
alpha = 90.0  # degrees # NACA5515 Huang19 model

from pysph.tools.geometry import get_4digit_naca_airfoil, rotate

# # two airfoils
# def solid_airfoil(dx, airfoil, chord, angle): 
#     data = np.load('airfoil15.npz')
#     x0, y0 = data['xs'], data['ys']
#     z0 = np.zeros_like(x0)
#     angle = -10
#     chord = 2
#     number_of_airfoils = 2
#     path_radius = 5
#     Lt = 36.0
#     center = [Lt/2, 12.0]
#     x, y, z = [], [], []
#     for i in range(number_of_airfoils):
#         aoa = -angle - 360*i / number_of_airfoils
#         px, py, pz = G.rotate(x0, y0, z0, angle=aoa)
#         theta = 2*np.pi*i / number_of_airfoils + 0.5*np.pi
#         px -= path_radius * np.cos(theta)
#         py += path_radius * np.sin(theta)
#         # x = np.append(px)
#         # y = np.append(py)
#         # z = np.append(pz)
#         x = np.concatenate([x, px])
#         y = np.concatenate([y, py])
#         z = np.concatenate([z, pz])

#         x = x + Lt / 2.0  # x_airfoil - 0.5
#         y = y # + w_tunnel * 0.25

#         h = dx
#         one = np.ones_like(x)
#         volume = dx * dx * one
#         solidp = get_particle_array(name='solidp', x=x, x0=x, y=y, y0=y, m=volume * rho,
#                                    rho=one * rho, h=h * one, V=1.0 / volume)
#         solidp.add_constant('ds_min', dx)
#         solidp.x += center[0]
#         solidp.y += center[1]
#         return solidp


def solid_airfoil(dx, airfoil, chord, angle):
    """ Generates an airfoil geometry for fluid flow simulation.
    """
    # data = np.load('airfoil12.npz')

    dir_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(dir_path, 'airfoil5515.npz')
    data = np.load(path)
    x, y = data['xf'], data['yf']

    # x, y = get_4digit_naca_airfoil(dx, airfoil, chord)
    z = np.zeros_like(x)
    angle = -5
    x, y, z = G.rotate(x, y, z, angle=angle)
    return x, y
##-------------------------------------------------------------

# Identify the boundary by computing the divergence of the
# position vector. When the value is lower than dim - 0.5 then
# it is identified as a surface point.
class IdentifyBoundary(Equation):
    def initialize(self, d_idx, d_div_r): 
        d_div_r[d_idx] = 0.0
        
    def loop(self, d_idx, d_div_r, XIJ, DWIJ, s_m, s_rho, s_idx): 
        Vj = s_m[s_idx]/s_rho[s_idx] 
        xijdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2] 
        d_div_r[d_idx] -= Vj * xijdotdwij 


class Airfoil(WindTunnel):
    def initialize(self):
        super().initialize()
        # self.chord = 2.0
        self.chord = self.dc * 2   
        self.center = (self.Lt/2.0, 0.0)
 
    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(
            dc=1.0, re=420, tf=15, potential=False, dx_min=0.0125,
            Lt=10, Wt=3, dx_max=0.2, cr=1.15
        )

    def consume_user_options(self):
        super().consume_user_options()

        self.chord = self.dc * 2   
        self.cxy = (self.Lt/2.0, 0)

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        m = dx * dx * rho
        dia = self.dc
        airfoil = '5515'
        chord = self.chord
        angle = -5 #alpha
        # Return the positions of the airfoil.
        x, y = solid_airfoil(dx, airfoil, chord, angle)
        # solidp = solid_airfoil(dx=dx, airfoil='0015', chord=chord, angle=-10.0)
        # x, y = solidp.x, solidp.y
        # Shift the center of the airfoil to cxy.
        x += self.cxy[0]
        y += self.cxy[1]
        # Create the solid particle array.
        solid = get_particle_array(
            name='solid', x=x, y=y, y0=y, m=m, rho=rho, h=h0, x0=x, ds=dx
        )
        solid.add_constant('ds_min', dx)
        return solid


    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')

        # t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t(['fluid'], ['solid'])
        # self._plot_coeffs(t, cd, cl, cd_sf, cl_sf)
        
        # Compute cp vs t
        self.compute_cp()
        #return t, cd, xc


    # cxy = (L/2, 0.375 * chord)
    def _motion_eqn(self, t):
        omega = 2*pi/40
        S = 0.75 # factor
        l0 = 0.75 * self.chord

        ly = l0*sin(omega*t)
        lx = S*l0*cos(omega*t) - S*l0
        
        av = -l0 * omega * omega * sin(omega*t)
        au = -S * l0 * omega * omega * cos(omega * t) 
                
        theta_i = 15.0
        theta_0 = 2.48
        theta = theta_i + theta_0 * sin(omega*t)
        dlydt = l0*omega*cos(omega*t)
        dlxdt = S*l0*omega*cos(omega*t)
        # aoa = theta - math.atan(dhdt/u_freestream)
        aoa = theta - math.atan(dlydt/(u_freestream+dlxdt))

        return ly, lx, au, av

    # def pre_step(self, solver):               
    #     t = solver.t
    #     dt = solver.dt
    #     for particle in self.particles:
    #         if particle.name == 'solid':
    #             ly, lx, av, au = self._motion_eqn(t)
    #             S = 0.5               
    #             l0 = 0.75 * self.chord
    #             particle.x[:] = particle.x0 + lx # - S*l0
    #             particle.y[:] = particle.y0 + ly
    #             # particle.u[:] = l0 * omega * cos(omega*t)
    #             # particle.v[:] = St * l0 * omega * cos(omega * t)
    #             particle.av[:] = av #-St * l0 * omega * omega * sin(omega * t)
    #             particle.au[:] = au #-l0 * omega * omega * sin(omega*t)
                

    # def pre_step(self, solver):     # Rotate
    #     t = solver.t
    #     dt = solver.dt
    #     for pa in self.particles:
    #         if pa.name == 'solid':
    #             angle = t
    #             x,y,z = G.rotate(x=pa.x, y=pa.y,z=pa.z, angle=-angle)
    #             pa.x[:] = x[:] 
    #             pa.y[:] = y[:] 


    def compute_cp(self):
        import os
        import matplotlib.pyplot as plt
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.solver.utils import load

        res = os.path.join(self.output_dir, 'x_vs_cp.npz')
        data = load(self.output_files[-1])
        solid = data['arrays']['solid']
        solid.add_property('div_r')
        sph_eval = SPHEvaluator(
            arrays=[solid],
            equations=[Group(equations=[
                IdentifyBoundary(
                    dest=solid.name, sources=[solid.name]
                )
            ])], dim=2, kernel=QuinticSpline(dim=2)
        )
        sph_eval.evaluate()
        dim = 2
        surface = (solid.div_r < dim - 0.625)

        plt.scatter(solid.x, solid.y, c=surface)
        plt.axes().set_aspect('equal')
        plt.show()
        surface_pts = solid.x[surface] / self.chord
        surface_cp = solid.p[surface] / (0.5 * rho * u_freestream**2)
        plt.scatter(surface_pts, surface_cp)
        # plt.plot(surface_pts, surface_cp)
        np.savez(res, x=surface_pts, cp=surface_cp)
        fig = os.path.join(self.output_dir, "x_vs_cp.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'vor'
            b.range = '-1, 1'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 4.0
        ''')


if __name__ == '__main__':
    app = Airfoil()
    app.run()
    app.post_process(app.info_filename)

