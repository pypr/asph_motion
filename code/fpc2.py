"""
Flow past cylinder
"""
import os
import numpy as np
from numpy import pi, cos, sin, exp

from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme, SolidWallNoSlipBCReverse

from fpc_auto import WindTunnel

# Motion parameters
T = 10
omega = 2*np.pi/T  # omega=6.283 / T = 0.628
# A = 0.5 # A = [0.25, 1.25] A/D = ??

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


class MovingCylinder(WindTunnel):
    def initialize(self):
        super().initialize()
        self.s = 2.5 * self.dc
        self.A = 0.25 * self.dc
        self.sol_adapt = True #False

    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(potential=False, sol_adapt=False)
        group.add_argument(
            "--gsf", action="store", type=float, dest="gsf_factor",
            default=1.5,
            help="gap specing factor, spacing bw two cyls / cyl dia."
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.hdx = hdx = self.options.hdx
        self.nl = (int)(6.0*hdx)
        self.tf = 10.0
        self.gsf = self.options.gsf_factor
        self.s = self.gsf * self.dc
        print('dt = ', self.dt)

    def _create_solid(self):
        solid = super()._create_solid()
        solid.y += self.s/2.0
        return solid

    def _create_solid2(self):
        solid2 = super()._create_solid()
        solid2.set_name('solid2')
        solid2.y -= self.s/2.0
        return solid2

    def _create_pseudo_wake(self):
        dx = self.dx_min*self.cr**2
        x0, y0 = self.cxy
        l = 4*self.dc
        w = 4*self.dc
        y0 -= w*0.5
        h0 = self.hdx*dx
        x, y = np.mgrid[x0:x0+l:dx, y0:y0+w:dx]
        volume = dx*dx
        wake = get_particle_array(
            name='wake', x=x, y=y,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume)
        wake.add_constant('ds_min', dx)
        return wake

    def create_particles(self):
        dx = self.dx_min
        fluid = self._create_fluid()
        solid = self._create_solid()
        solid2 = self._create_solid2()
        inlet = self._create_inlet()
        outlet = self._create_outlet()        
        wall = self._create_wall()

        import matplotlib.pyplot as plt
        plt.scatter(wall.x, wall.y, s=2)
        plt.scatter(fluid.x, fluid.y, s=2)
        plt.scatter(solid.x, solid.y, s=2)
        plt.scatter(solid2.x, solid2.y, s=2)
        plt.scatter(inlet.x, inlet.y, s=2)
        plt.scatter(outlet.x, outlet.y, s=2)
        plt.show()

        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        # wake = self._create_pseudo_wake()
        wake = None
        self.wake = wake

        particles = [fluid, inlet, outlet, solid, solid2, wall]
        boundary = [solid, solid2]
        self.shift_sources = list(particles)

        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        if wake is not None:
            particles.append(wake)
            boundary.append(wake)
        self.scheme.setup_properties(particles)

        setup_properties([fluid, inlet, outlet])
        fluid.add_property('vmag')

        self._set_wall_normal(wall)

        bg_freq = 10 if self.sol_adapt else 500
        self._update_bg = UpdateBackground(
            fluid, dim=2, boundary=boundary, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=5,
            bg_freq=bg_freq, sol_adapt=self.sol_adapt
        )
        self._update_bg.set_initial_bg()
        self._update_bg.initialize_fluid(solids=[solid, solid2], hybrid=True)
        bg_pa = self._update_bg.bg_pa
        particles.append(bg_pa)

        fluid.uag[:] = 1.0
        fluid.uta[:] = 1.0
        inlet.pta[:] = 500
        outlet.uta[:] = 1.0

        solid.add_property('y_0')
        solid.y_0[:] = solid.y
        solid2.add_property('y_0')
        solid2.y_0[:] = solid2.y
        return particles

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid', 'solid2'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')
        t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf = self._compute_force_vs_t()
        self._plot_coeffs(t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf)
        return t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf

    def _compute_force_vs_t(self):
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.sph.equation import Group
        from edac import (
            MomentumEquationPressureGradient2, SummationDensityGather,
            SetWallVelocity, EvaluateNumberDensity
        )
        from edac import ComputeBeta

        data = load(self.output_files[0])
        solid = data['arrays']['solid']
        solid2 = data['arrays']['solid2']
        fluid = data['arrays']['fluid']

        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf',]
        for p in prop:
            solid.add_property(p)
            solid2.add_property(p)
            fluid.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.
        equations = [
            Group(equations=[
                SummationDensityGather(
                    dest='solid', sources=['fluid', 'solid', 'solid2']),
                ComputeBeta(dest='solid', sources=['fluid', 'solid', 'solid2'], dim=2),
                ComputeBeta(dest='solid2', sources=['fluid', 'solid', 'solid2'], dim=2),
                EvaluateNumberDensity(dest='solid', sources=['fluid']),
                EvaluateNumberDensity(dest='solid2', sources=['fluid']),
                SetWallVelocity(dest='solid', sources=['fluid']),
                SetWallVelocity(dest='solid2', sources=['fluid']), 
            ], real=False),
            Group(equations=[
                    MomentumEquationPressureGradient2(dest='solid', sources=['fluid']),
                    SolidWallNoSlipBCReverse(dest='solid', sources=['fluid'], nu=self.nu),
                    MomentumEquationPressureGradient2(dest='solid2', sources=['fluid']),
                    SolidWallNoSlipBCReverse(dest='solid2', sources=['fluid'], nu=self.nu),
            ], real=True),
        ]
        sph_eval = SPHEvaluator(
            arrays=[solid, solid2, fluid], equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cd2, cl2, cl_sf, cd_sf, cl2_sf, cd2_sf = [], [], [], [], [], [], [], [], []
        import gc
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx_max}, Cr: {self.cr:.4f}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        files_total = len(self.output_files[at_file:])
        i = 0
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays['fluid']
            solid = arrays['solid']
            solid2 = arrays['solid2']
            for p in prop:
                solid.add_property(p)
                solid2.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            solid2.add_property('beta')
            solid2.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            solid2.rho[:] = 1000
            sph_eval.update_particle_arrays([solid, solid2, fluid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            fx2 = solid2.m*solid2.au
            fy2 = solid2.m*solid2.av

            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            auf2 = solid2.m*solid2.auf
            avf2 = solid2.m*solid2.avf

            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))

            cd2.append(np.sum(fx2)/(0.5 * rho * umax**2 * self.dc))
            cl2.append(np.sum(fy2)/(0.5 * rho * umax**2 * self.dc))
            cd2_sf.append(np.sum(auf2)/(0.5 * rho * umax**2 * self.dc))
            cl2_sf.append(np.sum(avf2)/(0.5 * rho * umax**2 * self.dc))

            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf = list(map(np.asarray, (t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cd2=cd2, cl2=cl2, cl_sf=cl_sf, cd_sf=cd_sf, cl2_sf=cl2_sf, cd2_sf=cd2_sf)
        return t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf


    def _plot_coeffs(self, t, cd, cl, cd2, cl2, cd_sf, cl_sf, cd2_sf, cl2_sf):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_{df}$')
        plt.plot(t, cd2, label=r'$C_{d2}$')
        plt.plot(t, cl2, label=r'$C_{l2}$')
        plt.plot(t, cd2_sf, label=r'$C_{d2f}$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()


    # def pre_step(self, solver):               
    #     t = solver.t
    #     dt = solver.dt
    #     for particle in self.particles:
    #         if particle.name == 'solid':
    #             if t > 1.0:
    #                 particle.y[:] = particle.y_0[:] + self.A * np.sin(omega * (t-1.0))
    #                 particle.v[:] = self.A * np.cos(omega*(t-1.0)) * omega 
    #                 particle.av[:] = -self.A * np.sin(omega *(t-1.0)) * (omega)**2

    #         elif particle.name == 'solid2':
    #             if t > 1.0:
    #                 particle.y[:] = particle.y_0[:] - self.A * np.sin(omega * (t-1.0))   
    #                 particle.v[:] = -self.A * np.cos(omega*(t-1.0)) * omega 
    #                 particle.av[:] = self.A * np.sin(omega *(t-1.0)) * (omega)**2


    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()

    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['solid', 'solid2']:
            b = particle_arrays[name]
            b.scalar = 'm'
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid', 'solid2']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')


if __name__ == '__main__':
    app = MovingCylinder()
    app.run()
    app.post_process(app.info_filename)
