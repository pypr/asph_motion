"""
Flow past S-shaped object.
"""
import os
import numpy as np
from numpy import pi, sin, cos

from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from moving_square import MovingSquare


# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
u_freestream = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


def rotate(x, y, axis, deg):
    rad = deg * np.pi / 180
    R0 = np.cos(rad)
    R1 = -np.sin(rad)
    R2 = np.sin(rad)
    R3 = np.cos(rad)
    xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
    ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
    return xnew + axis[0], ynew + axis[1]


def semicircle(ri, ro, dx, shift=True, ends=True, up=True):
    if shift:
        r = np.arange(ri+dx/2, ro, dx)
    else:
        r = np.arange(ri, ro, dx)
    x, y = np.array([]), np.array([])
    for i in r:
        spacing = dx
        theta = np.linspace(0, pi, int(pi*i/spacing))
        if not ends:
            theta = theta[1:-1]
        x = np.append(x, i * cos(theta))
        sign = 1 if up else -1
        y = np.append(y, sign * i * sin(theta))
    return x, y


def c_shape(d, dx, shift=True):
    r = d/2
    x, y = semicircle(r/2, r, dx, ends=False)
    x1, y1 = semicircle(0, r/4, dx, shift=shift, up=False)
    x = np.hstack((x, x1-0.75*r))
    y = np.hstack((y, y1))
    return x, y


def s_shape(d, dx, shift=True):
    x, y = c_shape(d, dx, shift)
    x1, y1 = c_shape(d, dx, shift)
    xr, yr = rotate(x1, y1, [0, 0], 180)
    x = np.hstack([x, xr + 0.75*d])
    y = np.hstack([y, yr + dx])
    return x, y


class SShape(MovingSquare):
    def initialize(self):
        super().initialize()
        self.diameter = 1 * self.dc
        self.remove_inner_layers = False
        # Using omega 2 * pi/ 5 will give a max velocity of
        # (omega * max(x_solid  - x_cm))^2 = 1.09.
        self.omega = 2 * np.pi / 10

    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(
            potential=False, sol_adapt=False, Lt=30, Wt=15, tf=10, dc=1,
            re=2000, cr=1.12, dx_min=0.01, dx_max=0.2, solution_adapt=0.025,
            pfreq=250
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.tf = self.options.tf
        self.cxy = self.Lt / 2, 0.0

    def _create_solid(self):
        dx = self.dx_min
        h0 = self.hdx*dx
        dia = self.diameter
        x, y = s_shape(dia, dx, shift=True)

        x += self.cxy[0] - 0.5 * (dia - dia/4)
        y += self.cxy[1]

        self.xcm = self.cxy[0]
        self.ycm = self.cxy[1]

        self.ucm = 0.0
        self.vcm = 0.0

        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y, m=volume*rho, rho=rho, h=h0, xi=x, yi=y
        )
        solid.add_constant('ds_min', dx)

        import matplotlib.pyplot as plt
        plt.scatter(solid.x, solid.y, s=2)
        plt.scatter(self.xcm, self.ycm, s=8)
        plt.scatter(self.cxy[0], self.cxy[1], s=8)
        plt.axis('equal')
        plt.savefig(os.path.join(self.output_dir, 'geom.png'))
        plt.show()
        return solid

    def rotate(self, x, y, axis, rad):
        R0 = np.cos(rad)
        R1 = -np.sin(rad)
        R2 = np.sin(rad)
        R3 = np.cos(rad)
        xnew = R0 * (x - axis[0]) + R1 * (y - axis[1])
        ynew = R2 * (x - axis[0]) + R3 * (y - axis[1])
        xr, yr = xnew + axis[0], ynew + axis[1]
        return xr, yr

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 250
        kernel = QuinticSpline(dim=2)
        scheme.configure(h=self.h, nu=self.nu, cfl=self.cfl, c0=self.c0)

        scheme.configure_solver(
            kernel=kernel, tf=self.tf, dt=self.dt, pfreq=pfreq, n_damp=0,
            output_at_times=list(range(1, 10)),
        )

    def pre_step(self, solver):
        t = solver.t
        for particle in self.particles:
            if particle.name == 'solid':
                omega = self.omega

                # Rotation
                axis = [self.xcm, self.ycm]
                theta  = omega * t
                xr, yr = self.rotate(particle.xi, particle.yi, axis, theta)
                particle.x[:] = xr
                particle.y[:] = yr

                # Velocity due to pitching of rigid body about cm.
                particle.u[:] = -omega * (
                    sin(omega * t) * (particle.xi - axis[0]) +
                    cos(omega * t) * (particle.yi - axis[1])
                )
                particle.v[:] =  omega * (
                    cos(omega * t) * (particle.xi - axis[0]) -
                    sin(omega * t) * (particle.yi - axis[1])
                )

                # Acceleration due to translation of cm about self.cxy.
                particle.au[:] = -omega**2 * (
                    cos(omega * t) * (particle.xi - axis[0]) -
                    sin(omega * t) * (particle.yi - axis[1])
                )
                particle.av[:] = -omega**2 * (
                    sin(omega * t) * (particle.xi - axis[0]) +
                    cos(omega * t) * (particle.yi - axis[1])
                )


if __name__ == '__main__':
    app = SShape()
    app.run()
