"""
Flow past cylinder
"""
import os
import numpy as np

from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument

from adapt import UpdateBackground, setup_properties
from edac import AdaptiveEDACScheme
from flow_past_cylinder import SolidWallNoSlipBCReverse, ResetInletVelocity

from pysph.tools.geometry import get_4digit_naca_airfoil, rotate

from pysph.sph.equation import Equation, Group
from fpc_auto import WindTunnel, potential_flow

# Motion parameters
T = 10
A = 1.0 # A = [0.3, 1.5] A/D = ?? 
omega = 2*np.pi/T  # omega=6.283/T = 0.628/3

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


##-------------------------------------------------------------
uinf = 1.0

alpha = 10.0   # 10  # degrees
dx = 0.1
hdx = 1.2
h0 = dx * hdx

Lt = 60.0  # length of tunnel

from pysph.sph.wc.edac import EDACScheme
from pysph.tools.geometry import get_4digit_naca_airfoil, rotate

# # # one airfoil
# def solid_airfoil(dx, airfoil, chord, center, angle, Lt):
#     """ Generates an airfoil geometry for fluid flow simulation.
#     """
#     if len(airfoil) == 4:
#         data = np.load('airfoil.npz')
#         x, y = data['xf'], data['yf']
#         # x, y = get_4digit_naca_airfoil(dx, airfoil, chord)
#         z = np.zeros_like(x)
#         # if abs(angle) > 1e-3:
#         x, y, z = G.rotate(x, y, z, angle=-alpha)
#         # x = x + Lt/4.0 # l_tunnel / 3.0  # x_airfoil - 0.5
#         # y = y + w_tunnel * 0.5
#         # plt.scatter(x,y)
#         # plt.show()
#         h = dx
#         one = np.ones_like(x)
#         volume = dx * dx * one
#         solid = get_particle_array(name='solid', x=x, y=y, y0=y, 
#             m=volume * rho, rho=one * rho, h=h * one, V=1.0 / volume)
#         solid.add_constant('ds_min', dx)
#         solid.x += center[0]
#         solid.y += center[1]
#         return solid 

# two airfoils
def solid_airfoil(dx, airfoil, chord, center, angle, 
                  number_of_airfoils=2, path_radius=Lt*0.2):
    """ Generates a geometry which can be used for simulations of
       flow over an airfoil.
    """
    if len(airfoil) == 4:
        x0, y0 = get_4digit_naca_airfoil(dx, airfoil, chord)
        x0 -= chord/2
        z0 = np.zeros_like(x0)

        x, y, z = [], [], []
        for i in range(number_of_airfoils):
            aoa = -angle - 360*i / number_of_airfoils
            tmpx, tmpy, tmpz = G.rotate(x0, y0, z0, angle=aoa)
            theta = 2*np.pi*i / number_of_airfoils + 0.5*np.pi
            tmpx += path_radius * -np.cos(theta)
            tmpy += path_radius * np.sin(theta)
            x = np.concatenate([x, tmpx])
            y = np.concatenate([y, tmpy])
            z = np.concatenate([z, tmpz])

        x = x + Lt / 2.0  # x_airfoil - 0.5
        y = y # + w_tunnel * 0.25

        h = dx
        one = np.ones_like(x)
        volume = dx * dx * one
        solid = get_particle_array(name='solid', x=x, y=y, y0=y, m=volume * rho,
                                   rho=one * rho, h=h * one, V=1.0 / volume, ds=dx)
        solid.add_constant('ds_min', dx)
        solid.x += center[0]
        solid.y += center[1]
        return solid

##-------------------------------------------------------------

class MovingAirfoil(WindTunnel):
    def initialize(self):
        super().initialize()
        self.s = 2.5 * self.dc 
        self.A = 0.25 * self.dc 
        self.sol_adapt = True #False

        self.center = (30.0, 0.0)
        # self.chord = self.dc 
        self.chord = 2.0
        
        
    def add_user_options(self, group):
        super().add_user_options(group)
        group.set_defaults(potential=False, sol_adapt=False)
        group.add_argument(
            "--gsf", action="store", type=float, dest="gsf_factor",
            default=1.5,
            help="gap specing factor, spacing bw two cyls / cyl dia."
        )

    def consume_user_options(self):
        super().consume_user_options()
        self.hdx = hdx = self.options.hdx
        self.nl = (int)(10.0*hdx)
        self.tf = 10.0
        self.gsf = self.options.gsf_factor
        self.s = self.gsf * self.dc

    def _create_pseudo_wake(self):
        dx = self.dx_min*self.cr**2
        dc = self.dc
        x0, y0 = self.cxy
        l = 4*self.dc
        w = 4*self.dc
        y0 -= w*0.5
        h0 = self.hdx*dx
        x, y = np.mgrid[x0:x0+l:dx, y0:y0+w:dx]
        volume = dx*dx
        wake = get_particle_array(
            name='wake', x=x, y=y,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume)
        wake.add_constant('ds_min', dx)
        return wake

    def create_particles(self):
        dx = self.dx_min
        fluid = self._create_fluid()

        # solid = solid_airfoil(dx=dx, airfoil='0025', chord=self.chord, 
        #                        center=self.center, angle=20.0, Lt = self.Lt) 
        solid = solid_airfoil(dx=dx, airfoil='0025', chord=self.chord, center=self.center,
                  angle=0.0, number_of_airfoils=2, path_radius=self.Lt*0.2)

        outlet = self._create_outlet()
        inlet = self._create_inlet()
        wall = self._create_wall()
        
        ghost_inlet = self.iom.create_ghost(inlet, inlet=True)
        ghost_outlet = self.iom.create_ghost(outlet, inlet=False)

        # wake = self._create_pseudo_wake()
        wake = None
        self.wake = wake

        particles = [fluid, inlet, outlet, solid, wall]
        boundary = [solid]
        self.shift_sources = list(particles)

        if ghost_inlet:
            particles.append(ghost_inlet)
        if ghost_outlet:
            particles.append(ghost_outlet)

        if wake is not None:
            particles.append(wake)
            boundary.append(wake)
        self.scheme.setup_properties(particles)

        setup_properties([fluid, inlet, outlet])
        fluid.add_property('vmag')

        self._set_wall_normal(wall)

        bg_freq = 10 if self.sol_adapt else 500
        self._update_bg = UpdateBackground(
            fluid, dim=2, boundary=boundary, ds_min=self.dx_min,
            ds_max=self.dx_max, cr=self.cr, rho_ref=rho, freq=5,
            bg_freq=bg_freq, sol_adapt=self.sol_adapt
        )
        self._update_bg.set_initial_bg()
        self._update_bg.initialize_fluid(solids=[solid])
        bg_pa = self._update_bg.bg_pa
        particles.append(bg_pa)

        if self.options.potential:
            u, v, p = potential_flow(inlet.x, inlet.y, umax, self.cxy, self.dc)
            inlet.u[:] = u
            inlet.v[:] = v
            inlet.p[:] = p
            u, v, p = potential_flow(fluid.x, fluid.y, umax, self.cxy, self.dc)
            fluid.u[:] = u
            fluid.v[:] = v
            fluid.p[:] = p
            fluid.uhat[:] = u
            fluid.vhat[:] = v
            fluid.vmag[:] = np.sqrt(u**2 + v**2)
        fluid.uag[:] = 1.0
        fluid.uta[:] = 1.0
        inlet.pta[:] = 500
        outlet.uta[:] = 1.0

        solid.add_property('x_0')
        solid.x_0[:] = solid.x
        # solid.add_property('y_0')
        # solid.y_0[:] = solid.y  
        fluid.add_output_arrays(['vor'])
        return particles

    def create_scheme(self):
        nu = None
        s = AdaptiveEDACScheme(
            ['fluid'], ['solid'], dim=2, rho0=rho, c0=c0,
            nu=nu, h=None, inlet_outlet_manager=None,
            inviscid_solids=['wall'], cfl=None
        )
        return s

    
    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return
        self.res = os.path.join(self.output_dir, 'results.npz')
        t, cd, cl, cd_sf, cl_sf = self._compute_force_vs_t()
        self._plot_coeffs(t, cd, cl, cd_sf, cl_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _compute_force_vs_t(self):
        from pysph.solver.utils import iter_output, load
        from pysph.tools.sph_evaluator import SPHEvaluator
        from pysph.sph.equation import Group
        from edac import (
            MomentumEquationPressureGradient2, SummationDensityGather,
            SetWallVelocity, EvaluateNumberDensity
        )
        from vacondio import ComputeBeta

        data = load(self.output_files[0])
        solid = data['arrays']['solid']
        fluid = data['arrays']['fluid']

        prop = ['awhat', 'auhat', 'avhat', 'wg', 'vg', 'ug', 'V', 'uf', 'vf',
                'wf', 'wij', 'vmag', 'pavg', 'nnbr', 'auf', 'avf', 'awf']
        for p in prop:
            solid.add_property(p)
            fluid.add_property(p)

        # We find the force of the solid on the fluid and the opposite of that
        # is the force on the solid. Note that the assumption is that the solid
        # is far from the inlet and outlet so those are ignored.
        equations = [
            Group(equations=[
                SummationDensityGather(
                    dest='solid', sources=['fluid', 'solid']),
                ComputeBeta(dest='solid', sources=['fluid', 'solid'], dim=2),
                EvaluateNumberDensity(dest='solid', sources=['fluid']),
                SetWallVelocity(dest='solid', sources=['fluid']),
            ], real=False),
            Group(equations=[
                    MomentumEquationPressureGradient2(dest='solid', sources=['fluid']),
                    SolidWallNoSlipBCReverse(dest='solid', sources=['fluid'], nu=self.nu),
            ], real=True),
        ]
        sph_eval = SPHEvaluator(
            arrays=[solid, fluid], equations=equations, dim=2,
            kernel=QuinticSpline(dim=2)
        )
        t, cd, cl, cl_sf, cd_sf = [], [], [], [], []
        import gc
        msg = f"diameter: {self.dc}, dx_min: {self.dx_min}, "
        msg += f"dx_max: {self.dx_max}, Cr: {self.cr:.4f}, nu: {self.nu:.4f}"
        print(msg)

        # Don't use the zeroth file as au is zero which results in nan's.
        at_file = 1
        files_total = len(self.output_files[at_file:])
        i = 0
        for sd, arrays in iter_output(self.output_files[at_file:]):
            fluid = arrays['fluid']
            solid = arrays['solid']
            for p in prop:
                solid.add_property(p)
                fluid.add_property(p)
            solid.add_property('beta')
            solid.beta[:] = 1.0
            t.append(sd['t'])
            fluid.rho[:] = 1000
            solid.rho[:] = 1000
            sph_eval.update_particle_arrays([solid, fluid])
            sph_eval.evaluate()
            fx = solid.m*solid.au
            fy = solid.m*solid.av
            auf = solid.m*solid.auf
            avf = solid.m*solid.avf
            cd.append(np.sum(fx)/(0.5 * rho * umax**2 * self.dc))
            cl.append(np.sum(fy)/(0.5 * rho * umax**2 * self.dc))
            cd_sf.append(np.sum(auf)/(0.5 * rho * umax**2 * self.dc))
            cl_sf.append(np.sum(avf)/(0.5 * rho * umax**2 * self.dc))
            msg = f"iters: {i}/{files_total}, "
            msg += f"t: {t[-1]:.4f}, Cd: {cd[-1]:.4f}, Cl: {cl[-1]:.4f}"
            msg += f" Clf: {cl_sf[-1]:.4f}, Cdf: {cd_sf[-1]:.4f}"
            print(msg)
            gc.collect()
            i += 1
        t, cd, cl, cd_sf, cl_sf = list(map(np.asarray, (t, cd, cl, cd_sf, cl_sf)))
        np.savez(self.res, t=t, cd=cd, cl=cl, cl_sf=cl_sf, cd_sf=cd_sf)
        return t, cd, cl, cd_sf, cl_sf

    def _plot_coeffs(self, t, cd, cl, cd_sf, cl_sf):
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot as plt
        plt.figure()
        plt.plot(t, cd, label=r'$C_d$')
        plt.plot(t, cl, label=r'$C_l$')
        plt.plot(t, cd_sf, label=r'$C_{df}$')
        plt.plot(t, cl_sf, label=r'$C_{lf}$')
        plt.xlabel(r'$t$')
        plt.ylabel('cd/cl')
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_dir, "force_vs_t.png")
        plt.savefig(fig, dpi=300)
        plt.close()


    def pre_step(self, solver):               
        t = solver.t
        dt = solver.dt
        # for particle in self.particles:
        #     if particle.name == 'solid':
        #         if t > 1.0:
        #             particle.y[:] = particle.y_0[:] + A * np.sin(omega * (t-1.0))
        #             particle.v[:] = A * np.cos(omega*(t-1.0)) * omega 
        #             particle.av[:] = -A * np.sin(omega *(t-1.0)) * (omega)**2
        for particle in self.particles: # OFV in x direction
            if particle.name == 'solid':
                if t > 1.0:
                    particle.x[:] = particle.x_0[:] + A * np.sin(omega * (t-1.0))
                    particle.u[:] = A * np.cos(omega*(t-1.0)) * omega 
                    particle.au[:] = -A * np.sin(omega *(t-1.0)) * (omega)**2


    def post_step(self, solver):
        freq = 500
        if solver.count % freq == 0:
            self.nnps.update()
            for i, pa in enumerate(self.particles):
                if pa.name == 'fluid':
                    self.nnps.spatially_order_particles(i)
            self.nnps.update()


    def customize_output(self):
        self._mayavi_config('''
        particle_arrays['bg'].visible = False
        if 'wake' in particle_arrays:
            particle_arrays['wake'].visible = False
        if 'ghost_inlet' in particle_arrays:
            particle_arrays['ghost_inlet'].visible = False
        for name in ['fluid', 'inlet', 'outlet']:
            b = particle_arrays[name]
            b.scalar = 'p'
            b.range = '-1000, 1000'
            b.plot.module_manager.scalar_lut_manager.lut_mode = 'seismic'
        for name in ['fluid', 'solid']:
            b = particle_arrays[name]
            b.point_size = 2.0
        ''')



if __name__ == '__main__':
    app = MovingAirfoil()
    app.run()
    app.post_process(app.info_filename)
